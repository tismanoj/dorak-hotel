<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends AdminController {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language','form_field_values'));
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
		$this->load->library('Datatables');
		$this->output->set_meta('description','Dorak Users');
		$this->output->set_meta('title','Dorak Users');
		$this->output->set_title('Dorak Users');
		$this->load->model('users/users_model');
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}	
		
	}
	
function userListingAjax(){
// this function is for generation  json data for data tables ajax call	
	$this->datatables->select('
							dorak_users.id,
							dorak_users.first_name,
							dorak_users.last_name,
							companies.company_name,	
							departments.title as department,
							positions.title as position,
							role.title as role,
							dorak_users.email,
							dorak_users.phone,										
							dorak_users.active,
							access_levels.description
							',False)		
		->add_column('Actions','$1','get_action_lnk(dorak_users.id,'.$this->accessLabelId.')')
		->edit_column('dorak_users.active','$1','get_status_lnk(dorak_users.active,dorak_users.id)')     
		->unset_column('dorak_users.id')
		->join('role', 'dorak_users.role = role.id','left')
		->join('companies', 'dorak_users.company = companies.id','left')
		->join('positions', 'positions.id = dorak_users.position','left')
		->join('departments', 'departments.id = dorak_users.department','left')
		->join('user_authorization', 'user_authorization.user_id = dorak_users.id','left')
		->join('access_levels', 'access_levels.id = user_authorization.access_level_id','left')
		->group_by('dorak_users.id');
	
	$this->datatables->from('dorak_users');
	echo $this->datatables->generate();
    exit();		
	}

	// delete the user
	function delete_user($id = NULL)
	{
			if(!checkAccess($this->accessLabelId,'users','delete'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	
	
		$id = (int) $id;
		if($id > 1)
		{
				// do we have the right userlevel?
					$this->ion_auth->delete_user($id);
					
					$sdata['message'] ='User Deleted';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
					$this->session->set_userdata($flashdata); 
	
		}
		else{
			$sdata['message'] ='Top lebel user  can not be deleted';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
					$this->session->set_userdata($flashdata); 
			
		}
			// redirect them back to the auth page
			redirect('users/users_list', 'refresh');
	}

	// redirect if needed, otherwise display the user list
	function index()
	{	
	$this->output->set_meta('title','User Dasboard');
	$this->output->set_title('User Dasboard');
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}		
		else
		{
			// get user details
			$data['user_profile'] = $this->ion_auth->user()->row();	
			$data['group_details'] = $this->ion_auth->get_users_groups($data['user_profile']->id)->row();
			
			$this->_render_page('users/index', $data);
		}
	}
	
	// redirect if needed, otherwise display the user list
	function users_list()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else
		{
			// check if user have access for this page
		if(!checkAccess($this->accessLabelId,'users','view'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	
			
			$positions=getPositions();// from form data helper
			$positionOptions =array(''  => 'Select');	
			if($positions && count($positions)>0)
			{
			foreach($positions as $position)
			{
				$positionOptions[$position->id]=$position->title;
			}
			}
			
			$departments=getDepartements();// from form data helper
			$departmentOptions =array(''  => 'Select');	
			if($departments && count($departments)>0)
			{
			foreach($departments as $department)
			{
				$departmentOptions[$department->id]=$department->title;
			}
			}
			
			$roles=getRoles();// from form data helper
			$roleOptions =array(''  => 'Select');	
			if($roles && count($roles)>0)
			{
			foreach($roles as $role)
			{
				$roleOptions[$role->id]=$role->title;
			}
			}
			$companies=getCompanies();
			$companyOptions =array(''  => 'Select');	
			if($companies && count($companies)>0)
			{
			foreach($companies as $company)
			{
				$companyOptions[$company->id]=$company->company_name;
			}
			}	
		$tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $data['identity_column'] = $identity_column;
            // display the create user form
            // set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $data['first_name'] = array(
                'name'  => 'first_name',
				'tabindex'=>'1',
				'data-validation'=>'required,custom,length',
				'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
				'data-validation-length'=>"min3",
				'maxlength'     => '100',
				'size'          => '100',
				'data-validation-help'=>"Should  be minimum  3  characters ",										
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $data['last_name'] = array(
                'name'  => 'last_name',
				'tabindex'=>'2',
				'data-validation'=>'custom,length',
				'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",
				'data-validation-length'=>"min3",
				'data-validation-optional'=>'true',	
				'maxlength'     => '100',
				'size'          => '100',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $data['identity'] = array(
                'name'  => 'identity',
				'tabindex'=>'12',
				'data-validation'=>'custom,required,length',
				'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
				'data-validation-length'=>"min5",
				'data-validation-help'=>"Should  be minimum  5  characters ",
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $data['email'] = array(
                'name'  => 'email',
				'tabindex'=>'7',
				'data-validation'=>'required,email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
          
            $data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
				'data-validation'=>'required,phone_number,length',	
				'data-validation-length'=>"min10",
				'maxlength'     => '20',
				'tabindex'=>'8',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $data['password'] = array(
                'name'  => 'password_confirmation',
				'tabindex'=>'9',
				'data-validation'=>'required,strength,length',	
				'data-validation-strength'=>"2"	,			
				'data-validation-length'=>"min6",
				'maxlength'     => '20',
				'size'          => '20',
                'id'    => 'password_confirmation',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirmation'),
            );
            $data['password_confirm'] = array(
                'name'  => 'password',
				'data-validation-help'=>"Should be same as  Password",
				'data-validation'=>"required,confirmation",
				'tabindex'=>'10',
				'maxlength'     => '20',
				'size'          => '20',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
			
			$data['companies']=$companyOptions;
			$data['positions']=$positionOptions;
			$data['departments']=$departmentOptions;
			$data['roles']=$roleOptions;
			$data['posted_position']=	$this->input->post('position');						
			$data['posted_department']=	$this->input->post('department');					
			$data['posted_role']=	$this->input->post('role');
			$data['posted_company']=	$this->input->post('company');			
			
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('users/list', $data);
			//$this->_render_page('users/create_user', $data);
		}
	}

	function user_roles()
	{
		$data['user_roles'] = $this->ion_auth->groups()->result();
		$this->_render_page('users/roles', $data);	
	}

	// activate the user
	function activate($id, $code=false)
	{
		
		if(!checkAccess($this->accessLabelId,'users','add'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	
		
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
				$sdata['message'] = $this->ion_auth->messages();
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
				$this->session->set_userdata($flashdata); 
			redirect('users/users_list', 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
			$sdata['message'] = $this->ion_auth->errors();
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
				$this->session->set_userdata($flashdata); 
			redirect("users/forgot_password", 'refresh');
		}
	}

	// deactivate the user
	function deactivate($id = NULL)
	{
		
			if(!checkAccess($this->accessLabelId,'users','delete'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	
	
			$id = (int) $id;		
			if ($this->ion_auth->logged_in())
				{
					$this->ion_auth->deactivate($id);
					
					$sdata['message'] ='User deactivated';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
					$this->session->set_userdata($flashdata); 
				}

			// redirect them back to the auth page
			redirect('users/users_list', 'refresh');
	}

	// create a new user
	function create_user()
    {
        $data['title'] = "Create User";

        if (!$this->ion_auth->logged_in())
        {
            redirect('auth', 'refresh');
        }
		// check if user have access for this page
		if(!checkAccess($this->accessLabelId,'users','add'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
				$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);	
			redirect('users', 'refresh');	
			}

			$positions=getPositions();// from form data helper
			$positionOptions =array(''  => 'Select');	
			if($positions && count($positions)>0)
			{
			foreach($positions as $position)
			{
				$positionOptions[$position->id]=$position->title;
			}
			}
			
			$departments=getDepartements();// from form data helper
			$departmentOptions =array(''  => 'Select');	
			if($departments && count($departments)>0)
			{
			foreach($departments as $department)
			{
				$departmentOptions[$department->id]=$department->title;
			}
			}
			
			$roles=getRoles();// from form data helper
			$roleOptions =array(''  => 'Select');	
			if($roles && count($roles)>0)
			{
			foreach($roles as $role)
			{
				$roleOptions[$role->id]=$role->title;
			}
			}
			
			$companies=getCompanies();
			$companyOptions =array(''  => 'Select');	
			if($companies && count($companies)>0)
			{
			foreach($companies as $company)
			{
				$companyOptions[$company->id]=$company->company_name;
			}
			}
		
        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $data['identity_column'] = $identity_column;

        // validate form input
      	$this->form_validation->set_rules('first_name','First name', 'required');
		 if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
		$this->form_validation->set_rules('phone','Phone', 'required');
		$this->form_validation->set_rules('company','Company', 'required');   if($identity_column!=='email')
         $this->form_validation->set_rules('password_confirmation','Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password]');
        $this->form_validation->set_rules('password','Confirm Password', 'required');

        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password_confirmation');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => $this->input->post('company'),
                'phone'      => $this->input->post('phone'),
				'position'      => $this->input->post('position'),
				'department'      => $this->input->post('department'),
				'role'      => $this->input->post('role'),
            );
        }
		$access_level_id=getRoleAccessLevelId($this->input->post('role'));
		$groupAray=array($access_level_id);		
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data,$groupAray))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
           // $this->session->set_flashdata('message', $this->ion_auth->messages());
           
				$sdata['message'] ='New user created';
					$flashdata = array(
							'flashdata'  => $this->ion_auth->messages(),
							'message_type'     => 'sucess'
							);				
					$this->session->set_userdata($flashdata); 

		   redirect("users/users_list", 'refresh');
        }
        else
        {
			/*
            // display the create user form
            // set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $data['first_name'] = array(
                'name'  => 'first_name',
				'tabindex'=>'1',
				'data-validation'=>'required,custom,length',
				'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
				'data-validation-length'=>"3-50",
				'data-validation-help'=>"Should  be minimum  3  characters ",										
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $data['last_name'] = array(
                'name'  => 'last_name',
				'tabindex'=>'2',
				'data-validation'=>'custom,length',
				'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",
				'data-validation-length'=>"3-50",
				'data-validation-optional'=>'true',	
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $data['identity'] = array(
                'name'  => 'identity',
				'tabindex'=>'12',
				'data-validation'=>'custom,required,length',
				'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
				'data-validation-length'=>"min5",
				'data-validation-help'=>"Should  be minimum  5  characters ",
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $data['email'] = array(
                'name'  => 'email',
				'tabindex'=>'7',
				'data-validation'=>'required,email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
          
            $data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
				'data-validation'=>'required,phone_number,length',	
				'data-validation-length'=>"min10",
				'maxlength'     => '20',
				'tabindex'=>'8',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $data['password'] = array(
                'name'  => 'password_confirmation',
				'tabindex'=>'9',
				'data-validation'=>'required,strength,length',	
				'data-validation-strength'=>"2"	,			
				'data-validation-length'=>"min6",
				'maxlength'     => '20',
				'size'          => '20',
                'id'    => 'password_confirmation',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirmation'),
            );
            $data['password_confirm'] = array(
                'name'  => 'password',
				'data-validation-help'=>"Should be same as  Password",
				'data-validation'=>"required,confirmation",
				'tabindex'=>'10',
                'id'    => 'password',
                'type'  => 'password',
				'maxlength'     => '20',
				'size'          => '20',
                'value' => $this->form_validation->set_value('password'),
            );
			$data['companies']=$companyOptions;
			$data['positions']=$positionOptions;
			$data['departments']=$departmentOptions;
			$data['roles']=$roleOptions;
			$data['posted_position']=	$this->input->post('position');						
			$data['posted_department']=	$this->input->post('department');					
			$data['posted_role']=	$this->input->post('role');
			$data['posted_company']=	$this->input->post('company');			
            //$this->_render_page('users/create_user', $data);
			*/
			$sdata['message'] ='User not created this email id already exists';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
					$this->session->set_userdata($flashdata); 
			redirect("users/users_list", 'refresh');
        }
    }

	// edit a user
	function edit_user($id)
	{
		$data['title'] = "Edit User";
		
			if(!checkAccess($this->accessLabelId,'users','edit'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	

		$tables = $this->config->item('tables','ion_auth');
		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name','First name', 'required');
		$this->form_validation->set_rules('phone','Phone', 'required');
		$this->form_validation->set_rules('company','Company', 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password_confirmation'))
			{
				$this->form_validation->set_rules('password_confirmation','Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password]');
				$this->form_validation->set_rules('password','Confirm Password', 'required');
			}
			
			$newemail    = strtolower($this->input->post('email'));
			$oldemail    = strtolower($this->input->post('oldemail'));
			if($newemail!=$oldemail)
			{
			$identity_column = $this->config->item('identity','ion_auth');
			$data['identity_column'] = $identity_column;

			// validate form input
			if($identity_column!=='email')
			{
				  $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
			}
			else
			{
				$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
			}
			}
			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
					'position'      => $this->input->post('position'),
					'department'      => $this->input->post('department'),
					'role'      => $this->input->post('role'),
				);
				if($newemail!=$oldemail)
				{
					$data['email']  = $newemail;
				}

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}
				// Only allow updating groups if user is admin
				
					$groupData=getRoleAccessLevelId($this->input->post('role'));				
					//Update the groups user belongs to
					//$groupData = $this->input->post('groups');
					// get  $this->input->post('role')group id  from role table
					if ($groupData && !empty($groupData)) {
						$this->ion_auth->remove_from_group('', $id);
						//foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($groupData, $id);
						//}

					}
				

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
					$sdata['message'] =$this->ion_auth->messages();
					$flashdata = array(
							'flashdata'  => $this->ion_auth->messages(),
							'message_type'     => 'sucess'
							);				
					$this->session->set_userdata($flashdata);
					redirect('users/users_list', 'refresh');
				
			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				   
					$flashdata = array(
							'flashdata'  => $this->ion_auth->errors(),
							'message_type'     => 'error'
							);				
					$this->session->set_userdata($flashdata);				 
					redirect('users/users_list', 'refresh');				

			    }

			}
			else{
				
					$flashdata = array(
							'flashdata'  => 'Data validation error',
							'message_type'     => 'error'
							);				
					$this->session->set_userdata($flashdata);
			}
		}
		
		$positions=getPositions();// from form data helper
			$positionOptions =array(''  => 'Select');	
			if($positions && count($positions)>0)
			{
			foreach($positions as $position)
			{
				$positionOptions[$position->id]=$position->title;
			}
			}
			
			$departments=getDepartements();// from form data helper
			$departmentOptions =array(''  => 'Select');	
			if($departments && count($departments)>0)
			{
			foreach($departments as $department)
			{
				$departmentOptions[$department->id]=$department->title;
			}
			}
			
			$roles=getRoles();// from form data helper
			$roleOptions =array(''  => 'Select');	
			if($roles && count($roles)>0)
			{
			foreach($roles as $role)
			{
				$roleOptions[$role->id]=$role->title;
			}
			}
			
			$companies=getCompanies();
			$companyOptions =array(''  => 'Select');	
			if($companies && count($companies)>0)
			{
			foreach($companies as $company)
			{
				$companyOptions[$company->id]=$company->company_name;
			}
			}
		
		

		// display the edit user form
		$data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$data['user'] = $user;
		$data['groups'] = $groups;
		$data['currentGroups'] = $currentGroups;

		$data['first_name'] = array(
			'name'  => 'first_name',
			'data-validation'=>'required,custom,length',
			'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
			'data-validation-length'=>"min3",
			'maxlength'     => '100',
			'size'          => '100',
			'data-validation-help'=>"Should  be minimum  3  characters ",										
             'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'data-validation'=>'custom,length',
			'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",
			'data-validation-length'=>"min3",
			'data-validation-optional'=>'true',	
			'maxlength'     => '80',
			'size'          => '80',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		 $data['email'] = array(
                'name'  => 'email',
				'tabindex'=>'7',
				'data-validation'=>'required,email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email', $user->email),
            );
			
			 $data['oldemail'] = array(
                'name'  => 'oldemail',
				'tabindex'=>'7',
				'data-validation'=>'required,email',
                'id'    => 'oldemail',
                'type'  => 'hidden',
                'value' => $this->form_validation->set_value('email', $user->email),
            );
		
		$data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'data-validation'=>'required,phone_number,length',	
			'data-validation-length'=>"min10",
			'maxlength'     => '20',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$data['password'] = array(
				'name'  => 'password_confirmation',
				'tabindex'=>'9',
				'data-validation'=>'strength',	
				'data-validation-strength'=>"2"	,
				'data-validation-optional'=>'true',	
				'maxlength'     => '20',
				'size'          => '20',				
                'id'    => 'password_confirmation',
			   'type' => 'password'
		);
		$data['password_confirm'] = array(
			'name'  => 'password',
			'data-validation-optional'=>'true',	
			'data-validation-help'=>"Should be same as  Password",
			'data-validation'=>"confirmation",
			'maxlength'     => '20',
			'size'          => '20',
			'type' => 'password'
		);
		
		$data['companies']=$companyOptions;
		$data['positions']=$positionOptions;
		$data['departments']=$departmentOptions;
		$data['roles']=$roleOptions;		
		$data['posted_position']=$user->position;						
		$data['posted_department']=	$user->department;					
		$data['posted_role']=$user->role;
		$data['posted_company']=$user->company;
		$this->_render_page('users/edit_user', $data);
	}

	// create a new group
	function create_role()
	{
		$data['title'] = $this->lang->line('create_group_title');
		if(!checkAccess($this->accessLabelId,'users','add'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				$sdata['message'] =$this->ion_auth->messages();
					$flashdata = array(
							'flashdata'  => $this->ion_auth->messages(),
							'message_type'     => 'sucess'
							);				
					$this->session->set_userdata($flashdata);
				redirect("users/user_roles", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('users/create_role', $data);
		}
	}

	// edit a group
	function edit_role($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('users', 'refresh');
		}

		$data['title'] = $this->lang->line('edit_group_title');

			if(!checkAccess($this->accessLabelId,'users','edit'))	
			{
			$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
			}	

		$group = $this->ion_auth->group($id)->row();
		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');
		$group_update=false;
		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_name']);
				
				//$acl_controllers=$this->input->post('acl_controller');
			$acl_controller_methods=$this->input->post('acl_method');
			if($acl_controller_methods && count($acl_controller_methods)>0)
				{
					//$this->ion_auth->removeRoleAclInfo($id);
				foreach($acl_controller_methods as $controllerIDkey=>$methodsId)
				{
					$this->controllerID=$controllerIDkey;
					if(count($methodsId)>0)
					{
					foreach($methodsId as $methodId) {
						$contactData[] = array(
								'access_level_id' => $id,
								'acl_sys_controller_id' => $this->controllerID,								
								'acl_sys_method_id' => $methodId								
							);							
						}
					}
				}
				
				$this->ion_auth->addRoleAclInfo($contactData,$id);
				}
				else
				{
				$this->ion_auth->removeRoleAclInfo($id);
				}			
				if($group_update)
				{
					//$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
					$sdata['message'] ='Access level is updated';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
					$this->session->set_userdata($flashdata);
				
				}
				else
				{
					//$this->session->set_flashdata('message', $this->ion_auth->errors());
				
					$flashdata = array(
							'flashdata'  => $this->ion_auth->errors(),
							'message_type'     => 'error'
							);				
					$this->session->set_userdata($flashdata);
				}
				redirect("users/user_roles", 'refresh');
			}
		}

	
		// pass the user to the view
		$data['group'] = $group;
		$data['acl_controllers']  =$this->ion_auth->acl_controllers();
		$data['ACLResource'] = $this->ion_auth->roleAssignedACLResource($id);
		
		//$data['acl_controller_methods']  =$this->ion_auth->acl_controller_methods();

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : 'readonly';

		$data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('users/edit_role', $data);
	}


	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

}
