<?php 
/**
  * @author tis
  * @author MS
  * @description this controller  import hotel  information using PHPExcel library
  */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Import extends AdminController {
	function __construct()
	{
		parent::__construct();
		
		$this->_init();
	}
	
private function _init()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect none logged user the login page
			redirect('auth/login', 'refresh');
		}	
		if(!checkAccess($this->accessLabelId,'hotels','add'))	
		{
		$sdata['message'] =$this->accessDenidMessage;					
			$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
			$this->session->set_userdata($flashdata);
			redirect('users', 'refresh');	
		}
	
		$this->load->helper(array('form','html','import'));
		$this->load->library('form_validation');		
		$this->output->set_meta('description','Hotel information');
		$this->output->set_meta('title','Hotel information');
		$this->output->set_title('Hotel informations');
		$this->tmpFileDir=$this->config->item('upload_temp_dir');
		$this->load->model('hotels/import_model');
	}
	
public function index()
	{
		$this->data = array(		
			'importAction' => site_url('hotels/import/process'), 			
			);	
		$this->load->view('import',$this->data);
	}
	
	/* Import pricing */

	/**
  * @method string importPriceing()
  * @method import hotel room details and pricing for markets, on the basis of hotel code	
  */
   public function importPriceing()
    {
		$data=array('message'=>'');
		$user = $this->ion_auth->user()->row();		
         if ($this->form_validation->run($this) != FALSE) {	
            $sdata['message'] ='Please select excel file to import';
			$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
			$this->session->set_userdata($flashdata); 
			redirect('/hotels');	
        } else {
			$arr_data=array();			
            // config upload
            $config['upload_path'] = $this->tmpFileDir;
            $config['allowed_types'] = 'xlsx|csv|xls|xls';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('hotels_pricingfile')) {			
				$sdata['message'] = $this->upload->display_errors();
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
				$this->session->set_userdata($flashdata); 
				redirect('/hotels');	
            } else {
                $upload_data = $this->upload->data();
				$hotels_pricingfile = $upload_data['full_path'];
				chmod($hotels_pricingfile,0777);               
				$this->load->library('excel');
			//read file from path
				$objPHPExcel = PHPExcel_IOFactory::load($hotels_pricingfile);
			try {
				$fileType = PHPExcel_IOFactory::identify($hotels_pricingfile);
				$objReader = PHPExcel_IOFactory::createReader($fileType);
				$objPHPExcel = $objReader->load($hotels_pricingfile);
				$sheets = [];
				foreach ($objPHPExcel->getAllSheets() as $sheet) {
					$sheets[$sheet->getTitle()] = $sheet->toArray();
				}
			 } catch (Exception $e) {
				 
				 log_message('importPriceing : ',$e->getMessage());
			 }
			$pricingRecordsArray=array();	
			$header=array();
			$srh = array("  "," ", "_(",")","(");
			$repl = array("_","_","_","","_");
			$objWorksheet = $objPHPExcel->getActiveSheet();
	// format  each row  with column as key of array 
	foreach ($objWorksheet->getRowIterator() as $row) {
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                       //    even if a cell value is not set.
                                                       // By default, only cells that have a value 
                                                       //    set will be iterated.
	foreach ($cellIterator as $cell) {
			$rowNumber=$cell->getRow(); 
			$cell_value=$cell->getValue();
			$column=$cell->getColumn();
			if ($rowNumber == 1) {
			$header[$rowNumber][$column] = str_replace($srh, $repl,strtolower(trim($cell_value)));	
			}
			else if ($rowNumber > 1){
						$pricingRecordsArray[$rowNumber][$header[1][$column]] = $cell_value;
				}					
			}   
			}
			if(count($pricingRecordsArray)>0){
			foreach($pricingRecordsArray as $pricingRec)
			{				
			$hotelInfoArray=array();// array to  insert hotel  information
			if(isset($pricingRec['hotel_code']) && $pricingRec['hotel_code']!="")
			{							
			// check if hotel details exists
			 $hotelId=$this->import_model->getHotelDetailsBycode($pricingRec['hotel_code']);
			if($hotelId)
			{
			$hid=$hotelId;
			// hotel pricing section	
			if(isset($pricingRec['room_type']) && $pricingRec['room_type']!='' && isset($pricingRec['currency']) && $pricingRec['currency']!='' && isset($pricingRec['inventory']) && $pricingRec['inventory']!='')
			{
			$room_type	=$this->getRoomTypeId($pricingRec['room_type']);
			$max_adult	='';
			$complimentary ='';
			$max_child ='';
			$period_from ='';
			$period_to ='';
			$complimentary ='';
			$inclusions ='';			
			if(isset($pricingRec['max_adult']) && $pricingRec['max_adult']!=""){
				$max_adult	=$pricingRec['max_adult'];
				}
			if(isset($pricingRec['complimentary']) && $pricingRec['complimentary']!=""){
				$complimentary =$pricingRec['complimentary'];
				}					
			if(isset($pricingRec['max_child']) && $pricingRec['max_child']!=""){
				$max_child =$pricingRec['max_child'];
				}
			if(isset($pricingRec['inclusions']) && $pricingRec['inclusions']!=""){
				$inclusions =$pricingRec['inclusions'];
				}
			if(isset($pricingRec['period_from']) && $pricingRec['period_from']!=""){
					$period_from	=formatDateToMysql($pricingRec['period_from']);
					}	
			if(isset($pricingRec['period_to']) && $pricingRec['period_to']!=""){
					$period_to	=formatDateToMysql($pricingRec['period_to']);
					}
	// check  if record already   exists  for  room type and  period 
	// then update the dada
		$hotel_pricing_id=$this->import_model->getHotalRoomsPricingData($hid,$room_type,$pricingRec['currency'],$period_from,$period_to);
		if(!$hotel_pricing_id){
		// if not exists  then add
				$hotelRoomTypesDetailsData= array(
						'hotel_id' => $hid,
						'room_type' => $room_type,
						'inclusions' => $inclusions,
						'curency_code' => $pricingRec['currency'],
						'max_adult' => $max_adult,
						'max_child' => $max_child,
						'inventory' => $pricingRec['inventory'],
						'period_from' => $period_from,
						'period_to' => $period_to,										
				);
			// add  data in database table  and get  id
			$pricing_id=$this->import_model->importHotalRoomsPricingData($hotelRoomTypesDetailsData);
		}
		else{
		$pricing_id=$hotel_pricing_id;
		// edit  pricing details
		$hotelRoomTypesDetailsData= array(						
						'inclusions' => $inclusions,						
						'max_adult' => $max_adult,
						'max_child' => $max_child,
						'inventory' => $pricingRec['inventory'],									
				);
		$this->import_model->editHotalRoomsPricingData($hid,$pricing_id,$hotelRoomTypesDetailsData);
		}
			if($pricing_id){
				$data['message'] = 'Room Type Detail information imported. <br>';
			}
			$roomComplementaryArray=explode(',',$complimentary);//coma seprated
			if(count($roomComplementaryArray)>0){
				$hotelRoomsPricingComplimentaryData=array();
				foreach($roomComplementaryArray as $roomComplementary)
				{
				if(trim($roomComplementary)!=""){
				$rcmpl_service_id=$this->getCmplServiceId(trim($roomComplementary));
				$hotelRoomsPricingComplimentaryData[] = array(
											'pricing_id' => $pricing_id,
											'cmpl_service_id' => $rcmpl_service_id																	
											);
					}
				}
				if(!empty($hotelRoomsPricingComplimentaryData)&& count($hotelRoomsPricingComplimentaryData)>0){	
					$this->import_model->deleteRoomPricingComplimentary($pricing_id);				
					$this->import_model->addHotalRoomPricingComplimentary($hotelRoomsPricingComplimentaryData);
					}	
			 }
			}			
			if($pricing_id && $pricing_id >0 && isset($pricingRec['market']) && $pricingRec['market']!=""){
					$market_id=$this->getMarketId($pricingRec['market']);
					$double_price='';
					$triple_price='';
					$quad_price='';
					$breakfast_price='';
					$half_board_price='';
					$price_all_inclusive='';
					$extra_adult_price='';
					$extra_child_price='';
					$extra_bed_price='';
				if(isset($pricingRec['double_price']) && $pricingRec['double_price']!=""){
				$double_price =$pricingRec['double_price'];
				}
				if(isset($pricingRec['triple_price']) && $pricingRec['triple_price']!=""){
				$triple_price =$pricingRec['triple_price'];
				}
				if(isset($pricingRec['quad_price']) && $pricingRec['quad_price']!=""){
				$quad_price =$pricingRec['quad_price'];
				}
				if(isset($pricingRec['breakfast_price']) && $pricingRec['breakfast_price']!=""){
				$breakfast_price =$pricingRec['breakfast_price'];
				}
				if(isset($pricingRec['half_board_price']) && $pricingRec['half_board_price']!=""){
				$half_board_price =$pricingRec['half_board_price'];
				}
				if(isset($pricingRec['price_all_inclusive']) && $pricingRec['price_all_inclusive']!=""){
				$price_all_inclusive =$pricingRec['price_all_inclusive'];
				}
				if(isset($pricingRec['extra_adult_price']) && $pricingRec['extra_adult_price']!=""){
				$extra_adult_price =$pricingRec['extra_adult_price'];
				}
				if(isset($pricingRec['extra_child_price']) && $pricingRec['extra_child_price']!=""){
				$extra_child_price =$pricingRec['extra_child_price'];
				}
				if(isset($pricingRec['extra_bed_price']) && $pricingRec['extra_bed_price']!=""){
				$extra_bed_price =$pricingRec['extra_bed_price'];
				}
				$hotelRoomTypesRateData= array(
						'pricing_id' => $pricing_id,
						'market_id' => $market_id,
						'double_price' => $double_price,
						'triple_price' => $triple_price,
						'quad_price' => $quad_price,
						'breakfast_price' => $breakfast_price,
						'half_board_price' => $half_board_price,
						'all_incusive_adult_price' => $price_all_inclusive,
						'extra_adult_price' => $extra_adult_price,
						'extra_child_price' => $extra_child_price,
						'extra_bed_price' => $extra_bed_price,																							
				);				
				$this->import_model->updateHotelRroomsPricingDetails($hotelRoomTypesRateData,$pricing_id,$market_id);
				}
				// hotel pricing section end
				$sdata['message'] ='Hotel pricing imported';
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
				$this->session->set_userdata($flashdata); 
				}
				else{
					// hotel not exists;
				 $sdata['message'] =$pricingRec['hotel_code'].' Not Exists.';
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
				$this->session->set_userdata($flashdata); 
				}
			}				
			}
			}			
			}
	}
		
	if(file_exists($hotels_pricingfile))
						{
						try { 
						unlink($hotels_pricingfile);
						} 
						catch (Exception $e) {
						 log_message('pricing file delete: ', $e->getMessage());										
						}						
						}
					redirect('/hotels');
	}
	## Import pricing  end ####	
	
	## import hotel details ####		
/**
  * @method string process()
  * @method import hotel all tabs information together or individually as  added in excel file
  */
   public function process()
    {		
		$data=array('message'=>'');
		$user = $this->ion_auth->user()->row();		
           if ($this->form_validation->run($this) != FALSE) {	
            $sdata['message'] ='Please select excel file to import';
			$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
			$this->session->set_userdata($flashdata); 
			redirect('/hotels');	
        } else {
			$arr_data=array();			
            // config upload
            $config['upload_path'] = $this->tmpFileDir;
            $config['allowed_types'] = 'xlsx|csv|xls|xls';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('hotels_file')) {			
				$sdata['message'] = $this->upload->display_errors();
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
				$this->session->set_userdata($flashdata); 
				redirect('/hotels');	
            } else {
                $upload_data = $this->upload->data();
				$hotels_file = $upload_data['full_path'];				 
					try { 
						chmod($hotels_file,0777); 
						} 
						catch (Exception $e) {
						 log_message('file permession: ', $e->getMessage());										
						}	
			$this->load->library('excel');
			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($hotels_file);
			try {
				$fileType = PHPExcel_IOFactory::identify($hotels_file);
				$objReader = PHPExcel_IOFactory::createReader($fileType);
				$objPHPExcel = $objReader->load($hotels_file);
				$sheets = [];
				foreach ($objPHPExcel->getAllSheets() as $sheet) {
					$sheets[$sheet->getTitle()] = $sheet->toArray();
				}
			 } catch (Exception $e) {
				 die($e->getMessage());
			 }
	$arrangedRecordsArray=array();	
	$header=array();
	$srh = array("  "," ", "_(",")","(");
	$repl = array("_","_","_","","_");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	// format  each row  with column as key of array 
	foreach ($objWorksheet->getRowIterator() as $row) {
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                       //    even if a cell value is not set.
                                                       // By default, only cells that have a value 
                                                       //    set will be iterated.
	foreach ($cellIterator as $cell) {
			$rowNumber=$cell->getRow(); 
			$cell_value=$cell->getValue();
			$column=$cell->getColumn();
			if ($rowNumber == 2) {
			$header[$rowNumber][$column] = str_replace($srh, $repl,strtolower(trim($cell_value)));	
			}
			else if ($rowNumber > 2){
						$arrangedRecordsArray[$rowNumber][$header[2][$column]] = $cell_value;
			}					
		}   
		}
		$contactData=array();	
		$nearestDistanceData=array();
		$hotelFacilityData=array();
		$roomFacilityComplimentaryData=array();
		$bankingDetailsData=array();
		$contractInfoData=array();
		$contractComplimentryRoomData=array();
		$contractComplimentryRoomExData=array();
		$contractRenovationData=array();
		$contractPaymentPlanData=array();
		$contractCancellationInfoData=array();
		$hotelRoomTypesDetailsData=array();
		$hotelRoomTypesRateData=array();
		$cmpl_room_id=false;
		$hotel_room_pricing_id=false;
		$hid=false;
		$exhid=false;
		$hotelCode=false;
		$ishotelInfoAdded=false;
		$hotelCounter=-1;// counter  counting extra 1 so insilize by -1
		foreach($arrangedRecordsArray as $rec)
		{					
			$hotelInfoArray=array();// array to  insert hotel  information
			if(isset($rec['hotel_name']) && $rec['hotel_name']!="" && $rec['city']!="" 	&& $rec['post_code']!="" && $rec['address']!=""	)
			{							
			// check if hotel details already exists
			 $hotelId=$this->import_model->getHotelDetails($rec['hotel_name'],$rec['city'],$rec['post_code']);
			if($hotelId)
				{
					$hid=$hotelId;		
					$hotelCounter++;			
				}
			else{
				// add hotel details in db
					$hotelInfoArray['hotel_name']=$rec['hotel_name'];
					if(isset($rec['hotel_code']) && $rec['hotel_code']!="")
					$hotelInfoArray['hotel_code']=$rec['hotel_code'];
					if(isset($rec['hotel_name']) && $rec['hotel_name']!="")		
					$hotelInfoArray['hotel_address']=$rec['hotel_name'];			
					if(isset($rec['hotel_chain']) and $rec['hotel_chain']!="")
					$hotelInfoArray['chain_id']=$this->hotelChainId($rec['hotel_chain']);
					if(isset($rec['purpose']) and $rec['purpose']!="")
					$hotelInfoArray['purpose']=$this->hotelPurposeId($rec['purpose']);
					if(isset($rec['country_code']) and $rec['country_code']!="")
					$hotelInfoArray['country_code']=$this->chkCountryCode($rec['country_code']);
					if(isset($rec['city']) and $rec['city']!="")		
					$hotelInfoArray['city']=$this->chkCountryCity($rec['city'],$rec['country_code']);
					if(isset($rec['district']) and $rec['district']!="")				
					$hotelInfoArray['district']=$this->chkCityDistrict($rec['district'],$rec['country_code'],$rec['city']);
					if(isset($rec['post_code']) and $rec['post_code']!="")		
					$hotelInfoArray['post_code']=$rec['post_code'];
					if(isset($rec['currency_code']) and $rec['currency_code']!="")	
					$hotelInfoArray['currency']=$rec['currency_code'];
					if(isset($rec['star_rating']) and $rec['star_rating']!="")
					$hotelInfoArray['star_rating']=$rec['star_rating'];
					if(isset($rec['dorak_rating']) and $rec['dorak_rating']!="")
					$hotelInfoArray['dorak_rating']=$rec['dorak_rating'];
					//$hotelInfoArray['commisioned']=$rec['hotel_name'];
					if(isset($rec['status']) and $rec['status']!="")
					$hotelInfoArray['status']=getHotelStatusId($rec['status']);// using helper
					$hotelInfoArray['created_byuser']=$user->id;
					// add hotel details
					if(!empty($hotelInfoArray) && count($hotelInfoArray)>0)
					{
					if($hid=$this->import_model->insertDetails($hotelInfoArray))
					{
					// if hotel code not provided the  or empty the add code for the hotel
					if(!isset($rec['hotel_code']) || $rec['hotel_code']=="")
					$hotelCode=$this->generateHotelcode($hid,$hotelInfoArray);
					}
					$hotelCounter++;
					$ishotelInfoAdded=true;
					}					
					}		
			 }
			elseif(isset($rec['hotel_code']) && $rec['hotel_code']!="")
			{
			 $hid=$this->import_model->getHotelDetailsBycode($rec['hotel_code']);
			}
			// prepare  other  details of hotel to add
			if(!$hid && $hid!="")
			{
			$data['message'] .= 'Hotels required  details are missing! Please Download sample file! and add content accordingly<br>';
				$flashdata = array(
						   'flashdata'  => $data['message'],
							'message_type'     => 'notice'
							);				
					$this->session->set_userdata($flashdata);
			redirect('/hotels');
			exit();			
			}
			if($hid && $hid!="")
			{
			$age_group='';	
			$ishotelInfoAdded=true;
			if(isset($rec['age_group']) && $rec['age_group']!="")
					{
					$age_group	=$this->getAgegroupId($rec['age_group']);					
				 if($age_group!="")
					{
					$uchData = array('child_age_group_id' => $age_group);
					$this->import_model->updateHotelDetails($hid,$uchData);
					}		
					}			
				#add hotel property types
			if(isset($rec['property_type']) and $rec['property_type']!="")	
			{					
			$hotelPropertyTypeArray=explode(',',$rec['property_type']);//coma seprated
			if(count($hotelPropertyTypeArray)>0){
				$hotelPropertyTypeData=array();
				foreach($hotelPropertyTypeArray as $hotelPropertyType)
				{
				if(trim($hotelPropertyType)!="")
				{
				$propertyType_id=$this->getPropertyType($hotelPropertyType);
				$hotelPropertyTypeData[] = array(
											'hotel_id' => $hid,
											'property_type_id' => $propertyType_id																	
											);
					}
				}
				if(!empty($hotelPropertyTypeData)&& count($hotelPropertyTypeData)>0)
					{										
					$this->import_model->addHotalPropertyTypes($hotelPropertyTypeData);
					}	
			 }
			}
			 #property type end
				
			// check if  contact details 
			if(isset($rec['designation']) && $rec['name']!="" && $rec['email']!="" && $rec['phone']!="" ){
				 if(isset($rec['extension']) && $rec['extension']!=""){
					$contactextension	=$rec['extension'];
					}
					else{
						$contactextension='';
					}
				$contactData[] = array(
						'hotel_id' => $hid,
						'position' => $this->contactPositionId($rec['designation']),
						'name' => $rec['name'],
						'email' => $rec['email'],
						'phone' => $rec['phone'],
						'extension' => $contactextension
					);	
			}
			if(isset($rec['city_name']) && isset($rec['city_distance']) && $rec['city_distance']!="" && $rec['city_name']!="" ){		
				$nearestDistanceData[] = array(
						'hotel_id' => $hid,
						'distance_from' => $rec['city_name'],
						'distance' => $rec['city_distance'],
						'distance_type' => 1 // 1 for city
					);		
			}			
			if(isset($rec['airport_name']) && isset($rec['airport_distance']) && $rec['airport_name']!="" && $rec['airport_distance']!="" ){		
				$nearestDistanceData[] = array(
						'hotel_id' => $hid,
						'distance_from' => $rec['airport_name'],
						'distance' => $rec['airport_distance'],
						'distance_type' => 2 // 2 for airport
					);		
			}	
		if(isset($rec['room_facility'])  && $rec['room_facility']!="" ){		
				$roomFacilityComplimentaryData[] = array(
						'hotel_id' => $hid,
						'cmpl_service_id' => $this->getCmplServiceId($rec['room_facility'])
					);		
			}
		if(isset($rec['hotel_facility'])  && $rec['hotel_facility']!="" ){		
				$hotelFacilityData[] = array(
						'hotel_id' => $hid,
						'facility_id' => $this->getFacilityId($rec['hotel_facility'])
					);		
			}	
		if(isset($rec['account_number'])  && isset($rec['account_name'])  && isset($rec['bank_name']) && isset($rec['bank_address']) && $rec['account_number']!="" && $rec['account_name']!=""&& $rec['bank_name']!="" && $rec['bank_address']!=""){
			$iban_code='';
			$ifsc_code='';
			$branch_name='';
			$branch_code='';
			$swift_code='';				
			if(isset($rec['iban_code']) && $rec['iban_code']!=""){
					$iban_code	=$rec['iban_code'];
					}
			if(isset($rec['branch_name']) && $rec['branch_name']!=""){
					$branch_name	=$rec['branch_name'];
					}
			if(isset($rec['bank_branch_code']) && $rec['bank_branch_code']!=""){
					$branch_code	=$rec['bank_branch_code'];
					}
			if(isset($rec['ifsc_code']) && $rec['ifsc_code']!=""){
					$ifsc_code	=$rec['ifsc_code'];
					}
			if(isset($rec['swift_code']) && $rec['swift_code']!=""){
					$swift_code	=$rec['swift_code'];
					}					
				$bankingDetailsData[] = array(
						'hotel_id' => $hid,
						'iban_code' => $iban_code,
						'account_number' => $rec['account_number'],
						'account_name' => $rec['account_name'],
						'bank_name' => $rec['bank_name'],
						'bank_address' => $rec['bank_address'],
						'branch_name' => $branch_name,
						'branch_code' => $branch_code,								
						'swift_code' => $swift_code,
						'bank_ifsc_code' => $ifsc_code	
					);		
				}
				// contract  imformation section
	if(isset($rec['contract_start_date'])  && isset($rec['contract_end_date'])  && isset($rec['contract_signed_by']) && $rec['contract_start_date']!="" && $rec['contract_end_date']!=""&& $rec['contract_signed_by']!=""){
			$contract_start_date=formatDateToMysql($rec['contract_start_date']); //use helper
			$contract_end_date=formatDateToMysql($rec['contract_end_date']); //use helper
			$contract_file_name='';			
			if(isset($rec['contract_file_name']) && $rec['contract_file_name']!=""){
					$contract_file_name	=$rec['contract_file_name'];
					}	
				$contractInfoData[] = array(
						'hotel_id' => $hid,
						'start_date'=>$contract_start_date,
						'end_date'=>$contract_end_date,
						'signed_by'=>$rec['contract_signed_by'],										
						'contract_file'=>$contract_file_name											
					);	
				}
	// contract  imformation section end
		// complimentary section	
		if(isset($rec['room_night']) && $rec['room_night']!=""  && isset($rec['period_from_date']) && $rec['period_from_date']!=""  && isset($rec['period_to_date']) && $rec['period_to_date']!=""){
			$compliRoomNight='';
			$compliPeriodFromDate='';
			$compliPeriodToDate	='';
			$compliUpgradable ='0';// 0 mfor no		 
				if(isset($rec['room_night']) && $rec['room_night']!=""){
					$compliRoomNight	=$rec['room_night'];
					}
				if(isset($rec['period_from_date']) && $rec['period_from_date']!=""){
					$compliPeriodFromDate =formatDateToMysql($rec['period_from_date']);
					}
				if(isset($rec['period_to_date']) && $rec['period_to_date']!=""){
					$compliPeriodToDate	=formatDateToMysql($rec['period_to_date']);
					}	
				if(isset($rec['upgradable']) && $rec['upgradable']!=""){
					if(strtolower($rec['upgradable'])=='yes')
					$compliUpgradable=1;
					else
					$compliUpgradable=0;
					}
				$contractComplimentryRoomData= array(
						'hotel_id' => $hid,
						'room_night'=>$compliRoomNight,						
						'start_date'=>$compliPeriodFromDate,
						'end_date'=>$compliPeriodToDate,
						'upgrade'=>$compliUpgradable,
				);
				// remove old ComlimentaryRoom details 
				$this->import_model->deleteHotalComlimentaryRoom($hid);
				// add  data in database table  and get  id
				$cmpl_room_id=$this->import_model->importHotalComlimentaryRoom($contractComplimentryRoomData);
			if($cmpl_room_id && $rec['hotel_name']!=""){
				$data['message'] .= $rec['hotel_name'].' : Complimentary Room information imported. <br>';			
				}			
			}			
			if($cmpl_room_id && $cmpl_room_id >0 && isset($rec['excluded_from_date']) && $rec['excluded_from_date']!="" && isset($rec['excluded_to_date']) && $rec['excluded_to_date']!=""){
				$excludedFromDate =formatDateToMysql($rec['excluded_from_date']);
				$excludedToDate	=formatDateToMysql($rec['excluded_to_date']);		
				$contractComplimentryRoomExData[] = array(
						'cmpl_room_id' => $cmpl_room_id,
						'exclude_date_from' => $excludedFromDate,
						'excluded_date_to' => $excludedToDate													
				);
				}
				// complimentary section end
			// renovation section
			if(isset($rec['renovation_from'])  && isset($rec['renovation_to'])  && isset($rec['renovation_type']) && isset($rec['areas_effected']) && $rec['renovation_from']!="" && $rec['renovation_to']!="" && $rec['renovation_type']!="" && $rec['areas_effected']!=""){
			$renovation_from=formatDateToMysql($rec['renovation_from']); //use helper
			$renovation_to=formatDateToMysql($rec['renovation_to']); //use helper
				$contractRenovationData[] = array(
						'hotel_id' => $hid,
						'date_from'=>$renovation_from,
						'date_to'=>$renovation_to,
						'renovation_type' => $rec['renovation_type'],
						'area_effected' => $rec['areas_effected']											
					);	
			}			
			// renovation section end
			// payment plan section
			if(isset($rec['plan'])  && isset($rec['payment_plan_value'])   && $rec['plan']!="" && $rec['payment_plan_value']!=""){
				$contractPaymentPlanData[] = array(
						'hotel_id' => $hid,
						'payment_option_id' => $this->getPaymentOpionId($rec['plan']),
						'payment_value' => $rec['payment_plan_value'],											
					);	
			}				
			// payment plan section end
			if(isset($rec['low_season_cancel_before'])  && isset($rec['low_season_cancel_before_value'])   && $rec['low_season_cancel_before']!="" && $rec['low_season_cancel_before_value']!=""){
				$contractCancellationInfoData[] = array(
						'hotel_id' => $hid,
						'cancelled_before' => $this->getCancelBeforeOpionId($rec['low_season_cancel_before']),
						'payment_request' => $rec['low_season_cancel_before_value'],
						'seasion' => 'low'											
					);
			$this->import_model->deleteHotalCancellation($hid,'low');						
			}
			
			if(isset($rec['high_season_cancel_before'])  && isset($rec['high_season_cancel_before_value'])   && $rec['high_season_cancel_before']!="" && $rec['high_season_cancel_before_value']!=""){
				$contractCancellationInfoData[] = array(
						'hotel_id' => $hid,
						'cancelled_before' => $this->getCancelBeforeOpionId($rec['high_season_cancel_before']),
						'payment_request' => $rec['high_season_cancel_before_value'],
						'seasion' => 'high'											
					);	
			$this->import_model->deleteHotalCancellation($hid,'high');						
			}
		// hotel pricing section	
		if(isset($rec['room_type']) && $rec['room_type']!='' && isset($rec['currency']) && $rec['currency']!='' && isset($rec['inventory']) && $rec['inventory']!=''){
			$room_type	=$this->getRoomTypeId($rec['room_type']);
			$max_adult	='';
			$complimentary ='';
			$max_child ='';
			$period_from ='';
			$period_to ='';
			$complimentary ='';
			$inclusions ='';			
			if(isset($rec['max_adult']) && $rec['max_adult']!=""){
				$max_adult	=$rec['max_adult'];
				}
			if(isset($rec['complimentary']) && $rec['complimentary']!=""){
				$complimentary =$rec['complimentary'];
				}					
			if(isset($rec['max_child']) && $rec['max_child']!=""){
				$max_child =$rec['max_child'];
				}
			if(isset($rec['inclusions']) && $rec['inclusions']!=""){
				$inclusions =$rec['inclusions'];
				}
			if(isset($rec['period_from']) && $rec['period_from']!=""){
					$period_from	=formatDateToMysql($rec['period_from']);
					}	
			if(isset($rec['period_to']) && $rec['period_to']!=""){
					$period_to	=formatDateToMysql($rec['period_to']);
					}	
				$hotelRoomTypesDetailsData= array(
						'hotel_id' => $hid,
						'room_type' => $room_type,
						'inclusions' => $inclusions,
						'curency_code' => $rec['currency'],
						'max_adult' => $max_adult,
						'max_child' => $max_child,
						'inventory' => $rec['inventory'],
						'period_from' => $period_from,
						'period_to' => $period_to,										
				);
			// add  data in database table  and get  id
			$hotel_room_pricing_id=$this->import_model->importHotalRoomsPricingData($hotelRoomTypesDetailsData);
			if($hotel_room_pricing_id && $rec['hotel_name']!=""){
				$data['message'] .= $rec['hotel_name'].' : Room Type Detail information imported. <br>';
			
			}
			$roomComplementaryArray=explode(',',$complimentary);//coma seprated
			if(count($roomComplementaryArray)>0){
				$hotelRoomsPricingComplimentaryData=array();
				foreach($roomComplementaryArray as $roomComplementary)
				{
				if(trim($roomComplementary)!="")
				{
				$rcmpl_service_id=$this->getCmplServiceId(trim($roomComplementary));
				$hotelRoomsPricingComplimentaryData[] = array(
											'pricing_id' => $hotel_room_pricing_id,
											'cmpl_service_id' => $rcmpl_service_id																	
											);
					}
				}
				if(!empty($hotelRoomsPricingComplimentaryData)&& count($hotelRoomsPricingComplimentaryData)>0)
					{	
					$this->import_model->deleteRoomPricingComplimentary($hotel_room_pricing_id);				
					$this->import_model->addHotalRoomPricingComplimentary($hotelRoomsPricingComplimentaryData);
					}	
			 }
			}			
			if($hotel_room_pricing_id && $hotel_room_pricing_id >0 && isset($rec['market']) && $rec['market']!="")
				{
					$market_id=$this->getMarketId($rec['market']);
					$double_price='';
					$triple_price='';
					$quad_price='';
					$breakfast_price='';
					$half_board_price='';
					$price_all_inclusive='';
					$extra_adult_price='';
					$extra_child_price='';
					$extra_bed_price='';
				if(isset($rec['double_price']) && $rec['double_price']!="")
				{
				$double_price =$rec['double_price'];
				}
				if(isset($rec['triple_price']) && $rec['triple_price']!="")
				{
				$triple_price =$rec['triple_price'];
				}
				if(isset($rec['quad_price']) && $rec['quad_price']!="")
				{
				$quad_price =$rec['quad_price'];
				}
				if(isset($rec['breakfast_price']) && $rec['breakfast_price']!="")
				{
				$breakfast_price =$rec['breakfast_price'];
				}
				if(isset($rec['half_board_price']) && $rec['half_board_price']!="")
				{
				$half_board_price =$rec['half_board_price'];
				}
				if(isset($rec['price_all_inclusive']) && $rec['price_all_inclusive']!="")
				{
				$price_all_inclusive =$rec['price_all_inclusive'];
				}
				if(isset($rec['extra_adult_price']) && $rec['extra_adult_price']!="")
				{
				$extra_adult_price =$rec['extra_adult_price'];
				}
				if(isset($rec['extra_child_price']) && $rec['extra_child_price']!="")
				{
				$extra_child_price =$rec['extra_child_price'];
				}
				if(isset($rec['extra_bed_price']) && $rec['extra_bed_price']!="")
				{
				$extra_bed_price =$rec['extra_bed_price'];
				}
					$hotelRoomTypesRateData[] = array(
						'pricing_id' => $hotel_room_pricing_id,
						'market_id' => $market_id,
						'double_price' => $double_price,
						'triple_price' => $triple_price,
						'quad_price' => $quad_price,
						'breakfast_price' => $breakfast_price,
						'half_board_price' => $half_board_price,
						'all_incusive_adult_price' => $price_all_inclusive,
						'extra_adult_price' => $extra_adult_price,
						'extra_child_price' => $extra_child_price,
						'extra_bed_price' => $extra_bed_price,																							
				);
				}
				// hotel pricing section end
			}
			else{
				
				if(isset($rec['hotel_name']) && isset($rec['city']))
				{
				$data['message'] .= 'Hotels '.$rec['hotel_name'].' in city '.$rec['city']. ' required  details are missing!<br>';
				}
				else{
					$data['message'] .= 'Hotels  required  details are missing or improper format!<br>';
				}
				$flashdata = array(
						   'flashdata'  => $data['message'],
							'message_type'     => 'notice'
							);				
					$this->session->set_userdata($flashdata);  
			}		
		$exhid=$hid;			
		}		
			// for new hotel record the  hotel details are changed  so we have to insert the  already  added in array value in database
			if(!empty($contactData) && count($contactData)>0 ){
			// delete old record from database table and add new one
			$this->import_model->deleteHotalContactInfo($hid);			
			$this->import_model->importHotalContactInfo($contactData);
			}
			if(!empty($nearestDistanceData) && count($nearestDistanceData)>0 ){
			// delete old record from database table and add new one
			$this->import_model->deleteHotalDistanceFrom($hid);				
			$this->import_model->importHotalDistanceFrom($nearestDistanceData);
			}
			if(!empty($hotelFacilityData) && count($hotelFacilityData)>0 ){
			$this->import_model->deleteHotalFacilities($hid);					
			$this->import_model->importHotalFacilities($hotelFacilityData);
			}
			if(!empty($roomFacilityComplimentaryData) && count($roomFacilityComplimentaryData)>0 ){	
			$this->import_model->deleteComplimentaryServices($hid);		
			$this->import_model->importComplimentaryServices($roomFacilityComplimentaryData);
			}
			if(!empty($bankingDetailsData) && count($bankingDetailsData)>0 ){
			$this->import_model->deleteHotalBankingInfo($hid);				
			$this->import_model->importHotalBankingInfo($bankingDetailsData);			
			}	
			if(!empty($contractInfoData) && count($contractInfoData)>0 ){			
			$this->import_model->importHotalContract($contractInfoData);			
			}
			if(!empty($contractComplimentryRoomExData) && count($contractComplimentryRoomExData)>0 ){			
				$this->import_model->importHotalComlimentaryRoomExcludedDate($contractComplimentryRoomExData);	
			}			
			if(!empty($contractRenovationData) && count($contractRenovationData)>0 ){	
				$this->import_model->deleteHotalRenovationSchedule($hid);		
				$this->import_model->importHotalRenovationSchedule($contractRenovationData);				
			}			
			if(!empty($contractPaymentPlanData) && count($contractPaymentPlanData)>0 ){	
				$this->import_model->deleteHotalPaymentShedules($hid);		
				$this->import_model->importHotalPaymentShedules($contractPaymentPlanData);			
			}
			if(!empty($contractCancellationInfoData) && count($contractCancellationInfoData)>0 ){			
				$this->import_model->importHotalCancellation($contractCancellationInfoData);
			}			
			if(!empty($hotelRoomTypesRateData) && count($hotelRoomTypesRateData)>0 ){				
				$this->import_model->importHotelRroomsPricingDetails($hotelRoomTypesRateData);				
			}
			if($ishotelInfoAdded){
					$hotelCounter++;
					$data['message'] ='Totel '.$hotelCounter .' Hotels Information Imported. <br>';			
					$flashdata = array(
						   'flashdata'  => $data['message'],
							'message_type'     => 'notice'
							);				
					$this->session->set_userdata($flashdata);
			}
			else{
					$hotelCounter++;
					$data['message'] ='No  Hotel  Information Imported. <br>';			
					$flashdata = array(
						   'flashdata'  => $data['message'],
							'message_type'     => 'notice'
							);				
					$this->session->set_userdata($flashdata);
					}
				if(file_exists($hotels_file)){
						try { 
						unlink($hotels_file);
						} 
						catch (Exception $e) {
						 log_message('hotel details import: ', $e->getMessage());										
						}						
			}
			}
			redirect('/hotels');
}
}
## other functions ####

/**
  * @method string hotelPurposeId()
  * @method use to add if purpose not exists in database and return id / get purpose id  if already in database
  * @return purpose Id.	
  */
private function hotelPurposeId($purpose_title){
		$purpose_title=removeExtraspace($purpose_title);
		if($pid=$this->import_model->getPurpose($purpose_title)){
						$purposeId=$pid;						
					}
					else{
						# add data in database 
						$hotel_purpose = array('title'=>$purpose_title);						
						$purposeId=$this->import_model->addPurpose($hotel_purpose);						
					}
					return  $purposeId;
}
/**
  * @method string hotelChainId()
  * @method use to add if chain not exists in database and return id / get id  if already in database
  * @return Chain Id.	
  */
private function hotelChainId($chain_title){
	$chain_title=removeExtraspace($chain_title);
	/* check if already in database if not then add */
		if($hcid=$this->import_model->getHotelChain($chain_title))
			{
			$chainId=$hcid;						
			}
			else
			{
			/* add data in database  */
			$hotel_chain = array('title'=>$chain_title);						
			$chainId=$this->import_model->addHotelChain($hotel_chain);						
			}
	return  $chainId;
}

/**
  * @method string contactPositionId()
  * @method use to add if contact Position not exists in database and return id / get id  if already in database
  * @return contact Position Id.	
  */
private function contactPositionId($title){
	$title=removeExtraspace($title);
	/* check if already in database if not then add */
		if($hcid=$this->import_model->getPositions($title))
			{
			$cId=$hcid;						
			}
			else
			{
			/* add data in database  */
			$position_det = array('title'=>$title);						
			$cId=$this->import_model->addPositions($position_det);						
			}
	return  $cId;
}

/**
  * @method string getCmplServiceId()
  * @method use to add if Cmplimentary Service not exists in database and return id / get id  if already in database
  * @return   Service Id.	
  */
private function getCmplServiceId($title){
	$title=removeExtraspace($title);
	/* check if already in database if not then add */
		if($hcSid=$this->import_model->getComplimentaryService($title))
			{
			$csId=$hcSid;						
			}
			else
			{
			/* add data in database  */
			$service_name_det = array('service_name'=>$title);						
			$csId=$this->import_model->addComplimentaryService($service_name_det);						
			}
	return  $csId;
}

/**
  * @method string getCmplServiceId()
  * @method use to add if Facility not exists in database and return id / get id  if already in database
  * @return   Facility Id.	
  */
private function getFacilityId($title){
	$title=removeExtraspace($title);
	/* check if already in database if not then add */
		if($hfid=$this->import_model->getFacility($title))
			{
			$fId=$hfid;						
			}
			else
			{
			/* add data in database  */
			$facility_det = array('facility_title'=>$title);						
			$fId=$this->import_model->addFacility($facility_det);						
			}
	return  $fId;
}

/**
  * @method string getAgegroupId()
  * @method use to add if Age group not exists in database and return id / get id  if already in database
  * @return   group Id.	
  */
private function getAgegroupId($agegroup){
	$agegroup=removeExtraspace($agegroup);
	/* check if already in database if not then add */
		if($afid=$this->import_model->getAgeGroup($agegroup))
			{
			$fId=$afid;						
			}
			else
			{
			/* add data in database  */
			$agegroup_det = array('age_range'=>$agegroup);						
			$fId=$this->import_model->addAgeGroup($agegroup_det);						
			}
	return  $fId;
}

/**
  * @method string getRoomTypeId()
  * @method use to add if room type not exists in database and return id / get id  if already in database
  * @return   Room Type Id.	
  */
private function getRoomTypeId($getRoomType){
	$getRoomType=removeExtraspace($getRoomType);
	/* check if already in database if not then add */
		if($afid=$this->import_model->getRoomTypeId($getRoomType))
			{
			$fId=$afid;						
			}
			else
			{
			/* add data in database  */
			$getRoomType_det = array('title'=>$getRoomType);						
			$fId=$this->import_model->addRoomType($getRoomType_det);						
			}
	return  $fId;
	
}
/**
  * @method string getPaymentOpionId()
  * @method use to add if payment opion not exists in database and return id / get id  if already in database
  * @return  Payment Opion Id.	
  */
private function getPaymentOpionId($patymentplan){
	$patymentplan=removeExtraspace($patymentplan);
	/* check if already in database if not then add */
		if($afid=$this->import_model->getPaymentPlan($patymentplan))
			{
			$fId=$afid;						
			}
			else
			{
			/* add data in database  */
			$patymentplan_det = array('option_title'=>$patymentplan);						
			$fId=$this->import_model->addPaymentPlan($patymentplan_det);						
			}
	return  $fId;
}

/**
  * @method string getCancelBeforeOpionId()
  * @method use to add if cancel before opion not exists in database and return id / get id  if already in database
  * @return  cancel before opion Id.	
  */
  
private function getCancelBeforeOpionId($optitle)
{
	$optitle=removeExtraspace($optitle);
	/* check if already in database if not then add */
		if($afid=$this->import_model->getCancelBefore($optitle))
			{
			$fId=$afid;						
			}
			else
			{
			/* add data in database  */
			$optitle_det = array('time_period'=>$optitle);						
			$fId=$this->import_model->addCancelBefore($optitle_det);						
			}
	return  $fId;
}

/**
  * @method string getMarketId()
  * @method use to add if Market not exists in database and return id / get id  if already in database
  * @return  Market Id .	
  */
private function getMarketId($mtitle)
{
	$mtitle=removeExtraspace($mtitle);
	/* check if already in database if not then add */
		if($afid=$this->import_model->getMarket($mtitle))
			{
			$fId=$afid;						
			}
			else
			{
			/* add data in database  */
			$m_det = array('title'=>$mtitle,'status'=>1);						
			$fId=$this->import_model->addMarket($m_det);						
			}
	return  $fId;
}

/**
  * @method string getPropertyType()
  * @method use to add if property type not exists in database and return id / get id  if already in database
  * @return  Property Type ID.	
  */
private function getPropertyType($ptitle)
{
	$mtitle=removeExtraspace($ptitle);
	/* check if already in database if not then add */
		if($afid=$this->import_model->getPropertyTypeId($ptitle))
			{
			$fId=$afid;						
			}
			else
			{
			/* add data in database  */
			$m_det = array('title'=>$mtitle,'status'=>1);						
			$fId=$this->import_model->addPropertyType($m_det);						
			}
	return  $fId;
}


/**
  * @method string chkCountryCity()
  * @method use to add if country city not exists in database and return id / get id  if already in database
  * @return  city  code.	
  */
private function chkCountryCity($city,$country_code='')
{
if(!empty($country_code) && !empty($city))
{
$country_code=removeExtraspace($country_code);
$city=removeExtraspace($city);	
$country_id=$this->import_model->getCountryId($country_code);
/* check if already in database if not then add */
		if(!$this->import_model->getCityId($city,$country_code))
			{			
			/* add data in database  */
			$city_det = array('country_id'=>$country_id,'country_code'=>$country_code,'city_code'=>$city,'city_name'=>$city);						
			$this->import_model->addCity($city_det);						
			}
	 return $city;
}
 return;
}
/**
  * @method string chkCityDistrict()
  * @method use to add if country district not exists in database and return district / get district if already in database
  * @return  district.	
  */
private function chkCityDistrict($dname,$country_code='',$city='')
{
if(!empty($dname) && !empty($country_code) && !empty($city)){
$dname=removeExtraspace($dname);
$city=removeExtraspace($city);	
$country_code=removeExtraspace($country_code);	
$country_id=$this->import_model->getCountryId($country_code);
$cityid=$this->import_model->getCityId($city,$country_code);
/* check if already in database if not then add */
		if(!$this->import_model->getDistrictId($dname,$cityid))
			{
			/* add data in database  */
			$districtDet = array('country_id'=>$country_id,'city_id'=>$cityid,'city_name'=>$city,'district_name'=>$dname);						
			$this->import_model->addDistrict($districtDet);						
			}
	 return $dname;
}
}
/**
  * @method string chkCountryCode()
  * @method use to add if country code not exists in database and return code / get code  if already in database
  * @return  city  code.	
  */
private function chkCountryCode($countrycode){
	$countrycode=removeExtraspace($countrycode);
	/* check if already in database if not then add */
		if($dcode=$this->import_model->getCountryCode($countrycode))
			{
			$countrycode=$dcode;						
			}
			else
			{
			/* add data in database  */
			$country_det = array('country_code'=>$countrycode,'country_name'=>$countrycode);						
			$this->import_model->addCountry($country_det);						
			}
	 return $countrycode;
}
/**
  * @method string generateHotelcode()
  * @method use to generate hotel code
  * @return   code.	
  */
private function generateHotelcode($inserted_hotel_id,$hotel_data){
					$hCodePrefix=$this->config->item('hotel_code_prefix'); 
					$first4CharOfName=substr($hotel_data['hotel_name'], 0, 4);	
					$first3CharOfCity=substr($hotel_data['city'], 0, 3);	
					$hotelCountryCode=$hotel_data['country_code'];
					$hotelCityCode=	getCityCode($hotel_data['city']);// using helper				
					$hcode=$hCodePrefix.$inserted_hotel_id."-".$first4CharOfName;					
					if($hotelCityCode && $hotelCityCode!=""){
					$hcode.="-".$hotelCityCode;
					}
					else{
					 $hcode.="-".$first3CharOfCity;
					}					
					if($hotelCountryCode && $hotelCountryCode!="")
					$hcode.="-".$hotelCountryCode;
					$uhData = array(
					'hotel_code' => $hcode);
					$this->import_model->updateHotelDetails($inserted_hotel_id,$uhData);	
		return $hcode;			
}
}