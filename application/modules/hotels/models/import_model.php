<?php
class Import_model extends AdminModel {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function addCountry($data)
    {
        if($this->db->insert('countries', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
    function getCountryCode($c_code)
    {
		$this->db->select('country_code');
		$this->db->where("country_code", $c_code); 
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->country_code;
		}
		else{
			return false;
		}
	}
	
	function getCountryId($c_code)
    {
		$this->db->select('id');
		$this->db->where("country_code", $c_code); 
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
function addCity($data)
    {
        if($this->db->insert('country_cities', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
	
function getCityId($cityName,$country_code='')
    {
		$this->db->select('id');
		$this->db->where("city_name", $cityName); 
		if($country_code!="")
		$this->db->where("country_code", $country_code); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}

	function getDistrictId($district_name,$cityId)
    {
		$this->db->select('id');		
		$this->db->where("city_id", $cityId); 
		$this->db->where("district_name", $district_name); 		
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
function addDistrict($data)
    {
        if($this->db->insert('city_districts', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
	
 function getHotelDetailsBycode($hotelcode)
    {
        $this->db->select('hotel_id');
        $this->db->where('hotel_code', $hotelcode);
        $query = $this->db->get('hotels');        
        if ($query->num_rows() > 0)
		{
		return $query->row()->hotel_id; 
		}
		else{
			return false;
		}
		
    }	
	
	
function updateHotelDetails($hid,$data)
    {
	$this->db->where('hotel_id', $hid);
	$this->db->update('hotels', $data); 
	}

 function getHotelDetails($hname,$hcity,$postcode)
    {
        $this->db->select('hotel_id');
        $this->db->where('hotel_name', $hname);
		$this->db->where('city', $hcity);
		$this->db->where('post_code', $postcode);
        $query = $this->db->get('hotels');        
        if ($query->num_rows() > 0)
		{
		return $query->row()->hotel_id; 
		}
		else{
			return false;
		}
		
    }
// save hotel data into database

    function insertDetails($data)
    {
        if($this->db->insert('hotels', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
	
	function addAgeGroup($data)
	{
	if($this->db->insert('child_age_ranges', $data))
        {
			return $this->db->insert_id();
		}
		return false;	
	}
	
	 function getAgeGroup($title)
    {
		$this->db->select('id');
		$this->db->where("age_range", $title); 
        $query = $this->db->get('child_age_ranges');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
	
    function addPurpose($data)
    {
        if($this->db->insert('hotel_purpose', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
    function getPurpose($title)
    {
		$this->db->select('id');
		$this->db->where("title", $title); 
        $query = $this->db->get('hotel_purpose');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	

function getPropertyTypeId($title)
    {
		$this->db->select('id');
		$this->db->where("title", $title); 
        $query = $this->db->get('property_types');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
	
function addPropertyType($data)
    {
        if($this->db->insert('property_types', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
function getPaymentPlan($title)
    {
		$this->db->select('id');
		$this->db->where("option_title", $title); 
        $query = $this->db->get('payment_options');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
function addPaymentPlan($data)
    {
        if($this->db->insert('payment_options', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
		
function getCancelBefore($title)
    {
		$this->db->select('id');
		$this->db->where("time_period", $title); 
        $query = $this->db->get('cancel_time_option');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
function addCancelBefore($data)
    {
        if($this->db->insert('cancel_time_option', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }	
	
	
function deleteHotalPaymentShedules($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_payment_shedules'); 
		}
		return;
    }
	
    
function importHotalPaymentShedules($data)
	{
	 if($this->db->insert_batch('hotel_payment_shedules', $data))
        {
			return true;
		}
		return false;	
	}
	
function addHotelChain($data)
    {
        if($this->db->insert('hotel_chain_list', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotelChain($title)
    {
		$this->db->select('chain_id');
		$this->db->where("title", $title); 
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->chain_id;
		}
		else{
			return false;
		}
	}
	

 function addPositions($data)
    {
        if($this->db->insert('positions', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getPositions($title)
    {
		$this->db->select('id');
		$this->db->where("title", $title); 
        $query = $this->db->get('positions');
        if ($query->num_rows() > 0)
		{
        return $query->row()->id;
		}
		else{
			return false;
		}
	}
	
    
    /* hotel contact section */
	
 function deleteHotalContactInfo($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_contact_details'); 
		}
		return;
    }
    
    
  function importHotalContactInfo($data)
    {
        if($this->db->insert_batch('hotel_contact_details', $data))
        {
			return true;
		}
		return false;
    }
    
	
/*  hotel contact section */

    /* import room facilities services section */
	
	function addComplimentaryService($data)
    {
        if($this->db->insert('complimentary_services', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getComplimentaryService($title)
    {
		$this->db->select('cmpl_service_id');
		$this->db->where("service_name", $title); 
        $query = $this->db->get('complimentary_services');
        if ($query->num_rows() > 0)
		{
        return $query->row()->cmpl_service_id;
		}
		else{
			return false;
		}
	}
	
	
 function deleteComplimentaryServices($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_complimentary_services'); 
		}
		return;
    }	
    
  function importComplimentaryServices($data)
    {
        if($this->db->insert_batch('hotel_complimentary_services', $data))
        {
			return true;
		}
		return false;
    }
    
  
  /* hotel contarct section */
    
  function importHotalContract($data)
    {
        if($this->db->insert_batch('hotel_contract_info', $data))
        {
			return true;
		}
		return false;
    }
    

    
	
/*  hotel contarct section */

/*  hotel Comlimentary  room section */

 function deleteHotalComlimentaryRoom($hid)
    {
		if($hid!="")
		{
			$sql='DELETE hotel_complimentary_room,hotel_cmplimntry_room_excluded_dates
					FROM hotel_complimentary_room
					INNER JOIN hotel_cmplimntry_room_excluded_dates ON hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id
					WHERE  hotel_complimentary_room.cmpl_room_id=hotel_cmplimntry_room_excluded_dates.cmpl_room_id  AND  hotel_complimentary_room.hotel_id ='.$hid;
			$this->db->query($sql);
		}
		return;
    }	
	 
  function importHotalComlimentaryRoom($data)
    {
        if($this->db->insert('hotel_complimentary_room', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
	
  function importHotalComlimentaryRoomExcludedDate($data)
    {
        if($this->db->insert_batch('hotel_cmplimntry_room_excluded_dates', $data))
        {
			return true;
		}
		return false;
    }
    

/*  hotel Comlimentary  room  section end */

  function addHotalPropertyTypes($data)
    {
        if($this->db->insert_batch('hotel_property_types', $data))
        {
			return true;
		}
		return false;
    } 



/*  hotel bank details section */

 function deleteHotalBankingInfo($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_bank_accounts'); 
		}
		return;
    }	
	
    
  function importHotalBankingInfo($data)
    {
        if($this->db->insert_batch('hotel_bank_accounts', $data))
        {
			return true;
		}
		return false;
    }   
    
	
/* hotel bank details section */

/*  hotel Cancellation details section */

function deleteHotalCancellation($hid,$seasion)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->where('seasion', $seasion);
			$this->db->delete('hotel_cancellation'); 
		}
		return;
    }
    
  function importHotalCancellation($data)
    {
        if($this->db->insert_batch('hotel_cancellation', $data))
        {
			return true;
		}
		return false;
    }
    

/*  hotel Cancellation details section */   

/*  hotel Renovation schedule details section */

function deleteHotalRenovationSchedule($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_renovation_schedule'); 
		}
		return;
    }

    
  function importHotalRenovationSchedule($data)
    {
        if($this->db->insert_batch('hotel_renovation_schedule', $data))
        {
			return true;
		}
		return false;
    }
    
    

/*  hotel Renovation schedule details section */


/*  hotel Distance  from section */


 function deleteHotalDistanceFrom($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_distance'); 
		}
		return;
    }
    
  function importHotalDistanceFrom($data)
    {
        if($this->db->insert_batch('hotel_distance', $data))
        {
			return true;
		}
		return false;
    }
    
/* hotel Facility section */

function addFacility($data)
    {
        if($this->db->insert('facilities', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getFacility($title)
    {
		$this->db->select('facility_id');
		$this->db->where("facility_title", $title); 
        $query = $this->db->get('facilities');
        if ($query->num_rows() > 0)
		{
        return $query->row()->facility_id;
		}
		else{
			return false;
		}
	}
	
	
 function deleteHotalFacilities($hid)
    {
		if($hid!="")
		{
			$this->db->where('hotel_id', $hid);
			$this->db->delete('hotel_facilities'); 
		}
		return;
    }	
	
    
  function importHotalFacilities($data)
    {
        if($this->db->insert_batch('hotel_facilities', $data))
        {
			return true;
		}
		return false;
    }
    
/*  hotel Facility  section end */

/*  hotel pricings  section */

function  getHotalRoomsPricingData($hotel_id,$room_type,$currency,$period_from,$period_to)
{
	$this->db->select('pricing_id');
	$this->db->where("hotel_id", $hotel_id);
	$this->db->where("room_type", $room_type);
	$this->db->where("curency_code", $currency);
	$this->db->where("period_from", $period_from);
	$this->db->where("period_to", $period_to);	
    $query = $this->db->get('hotel_rooms_pricing');
       if ($query->num_rows() > 0)
		{
        return $query->row()->pricing_id;
		}
		else{
			return false;
		}	
	
}
function editHotalRoomsPricingData($hid,$pricing_id,$data)
    {
		$this->db->where('pricing_id',$pricing_id);
		$this->db->where("hotel_id", $hid);
		$this->db->update('hotel_rooms_pricing', $data); 
	}
function importHotalRoomsPricingData($data)
    {
        if($this->db->insert('hotel_rooms_pricing', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
function addHotalRoomPricingComplimentary($data)
	{
	 if($this->db->insert_batch('hotel_rooms_pricing_complimentary', $data))
        {
			return true;
		}
		return false;	
	}

 function deleteRoomPricingComplimentary($pricingId)
    {
		if($pricingId!="")
		{
			$this->db->where('pricing_id', $pricingId);
			$this->db->delete('hotel_rooms_pricing_complimentary'); 
		}
		return;
    }		
	
function importHotelRroomsPricingDetails($data)
	{
	 if($this->db->insert_batch('hotel_rooms_pricing_details', $data))
        {
			return true;
		}
		return false;	
	}
	
function updateHotelRroomsPricingDetails($data,$pricing_id='',$market_id=null) {
	if($pricing_id!="")	{
		$this->db->where('pricing_id',$pricing_id);
		$this->db->where('market_id',$market_id);
		$q = $this->db->get('hotel_rooms_pricing_details'); 
		if($q->num_rows() > 0 ) { 
			$this->db->where('pricing_id',$pricing_id); 
			$this->db->where('market_id',$market_id);
			$this->db->update('hotel_rooms_pricing_details',$data); 
			return $pricing_id;
		} else { 
			$this->db->insert('hotel_rooms_pricing_details',$data);
			return $this->db->insert_id();
		}	
	} else { 
		$this->db->insert('hotel_rooms_pricing_details',$data);
		return $this->db->insert_id();
	}
		return;
	}
	
/*  hotel pricings  section  end*/


function addRoomType($data)
    {
        if($this->db->insert('hotel_room_types', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getRoomTypeId($title)
    {
		$this->db->select('type_id');
		$this->db->where("title", $title); 
        $query = $this->db->get('hotel_room_types');
        if ($query->num_rows() > 0)
		{
        return $query->row()->type_id;
		}
		else{
			return false;
		}
	}
	
function addMarket($data)
    {
        if($this->db->insert('markets', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getMarket($title)
    {
		$this->db->select('id');
		$this->db->where("title", $title); 
        $query = $this->db->get('markets');
        if ($query->num_rows() > 0)
		{
        return $query->row()->id;
		}
		else{
			return false;
		}
	}
		
	
	


}
