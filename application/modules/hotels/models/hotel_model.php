<?php
class Hotel_model extends AdminModel {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	
 function getHotelsDetails($hotelId)
    {
		$this->db->select('hotels.hotel_id,
							hotels.hotel_name,
							hotels.hotel_code,
							hotels.country_code,
							hotels.city,
							hotels.hotel_address,
							hotels.post_code,
							hotels.district,
							hotels.dorak_rating,
							hotels.star_rating,
							hotels.purpose,
							hotels.currency,
							hotels.chain_id,							
							hotels.commisioned,
							hotels.child_age_group_id,
							hotels.status');
		$this->db->from('hotels');
		$this->db->where('hotels.hotel_id',$hotelId);
        $query = $this->db->get();
        return $query->row();
    }	
  
 function getHotelsList($search_term=null,$limit=10,$offset=0)
    {
		$this->db->select('hotels.hotel_id,
							hotels.hotel_name,
							hotels.hotel_code,
							countries.country_name,
							hotels.city,
							hotels.dorak_rating,
							hotels.star_rating,
							hotel_purpose.title as purpose,
							hotels.last_updated,
							cn.title as chain,
							hs.title as status,
							pt.title as property_types');
		$this->db->from('hotels');
		$this->db->join('hotel_status hs', 'hotels.status = hs.id');
		$this->db->join('countries', 'hotels.country_code = countries.country_code');
		$this->db->join('hotel_chain_list cn', 'cn.chain_id = hotels.chain_id','left');
		$this->db->join('hotel_purpose', 'hotel_purpose.id = hotels.purpose','left');
		$this->db->join('hotel_property_types  hpt', 'hotels.hotel_id = hpt.hotel_id','left');
		$this->db->join('property_types  pt', 'pt.id = hpt.property_type_id','left');
		if($search_term!="" and count($search_term)>0)
		{
		if($search_term['searchByHotelName']!="")
		$this->db->like('hotels.hotel_name', $search_term['searchByHotelName']);
		if($search_term['searchByHotelCode']!="")
		$this->db->like('hotels.hotel_code', $search_term['searchByHotelCode']);
		if($search_term['searchByPurpose']!="")
		$this->db->where('hotels.purpose', $search_term['searchByPurpose']);
		if($search_term['searchByStarRating']!="")
		$this->db->where('hotels.star_rating', $search_term['searchByStarRating']);	
		if($search_term['searchByStatus']!="")
		$this->db->where('hotels.status', $search_term['searchByStatus']);
		if($search_term['searchByChainId']!="")
		$this->db->where('cn.chain_id', $search_term['searchByChainId']);
		if($search_term['searchByPropertyType']!="")
		$this->db->where('pt.id', $search_term['searchByPropertyType']);		
		}
		$this->db->order_by("hotels.hotel_name", "asc"); 
		$this->db->group_by('hotels.hotel_id');
		$this->db->limit($limit,$offset);
		
        $query = $this->db->get();
        return $query->result();
    }
	
function deleteHotels($hids)
{
$this->db->where_in('hotel_id', $hids);
if($this->db->delete('hotels'))
{
$this->deleteHotelBankDetails($hids);// delete bank details mass
$this->deleteHotelPropertyTypes($hids);
$this->deleteHotelFacilities($hids);
$this->deleteHotelDistance($hids);
$this->deleteHotelContactDetails($hids);
$this->deleteHotelContractInfo($hids);
$this->deleteHotelRenovationSchedule($hids);
$this->deleteHotelComplimentaryRoomExcludedDates($hids);
$this->deleteHotelComplimentaryRoom($hids);
$this->deleteHotelComplimentaryServices($hids);
$this->deleteHotelRoomsPricingComplimentary($hids);
$this->deleteHotelRoomsPricing($hids);
}
return true;
}

function deleteHotel($hid)
{
$hotelId=(int)$hid;
$this->db->where('hotel_id',$hotelId);
if($this->db->delete('hotels'))
{
$this->deleteHotelBankDetails($hotelId);
$this->deleteHotelPropertyTypes($hotelId);
$this->deleteHotelFacilities($hotelId);
$this->deleteHotelDistance($hotelId);
$this->deleteHotelContactDetails($hotelId);
$this->deleteHotelContractInfo($hotelId);
$this->deleteHotelRenovationSchedule($hotelId);

$this->deleteHotelComplimentaryRoomExcludedDates($hotelId);
$this->deleteHotelComplimentaryRoom($hotelId);
$this->deleteHotelComplimentaryServices($hotelId);
$this->deleteHotelRoomsPricingComplimentary($hotelId);
$this->deleteHotelRoomsPricing($hotelId);
}
return true;
}


function deleteHotelBankDetails($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);
}
else{
$this->db->where('hotel_id',(int)$hids);	
}
return $this->db->delete('hotel_bank_accounts');
}


function deleteHotelFacilities($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);
}
else{
$this->db->where('hotel_id',(int)$hids);	
}
return $this->db->delete('hotel_facilities');
}



function deleteHotelPropertyTypes($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_property_types');
}

//hotel_distance
function deleteHotelDistance($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_distance');
}



//del hotel_contact_details

function deleteHotelContactDetails($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_contact_details');
}

// hotel_contract_info

function deleteHotelContractInfo($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_contract_info');
}

// hotel_renovation_schedule

function deleteHotelRenovationSchedule($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_renovation_schedule');
}

// hotel_complimentary_services

function deleteHotelComplimentaryServices($hids)
{
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_complimentary_services');
}

// hotel_complimentary_room
function deleteHotelComplimentaryRoom($hids)
{
	
if(is_array($hids))
{
$this->db->where_in('hotel_id',$hids);	
}
else{
$this->db->where('hotel_id',(int)$hids);
}
return $this->db->delete('hotel_complimentary_room');

}

//hotel_cmplimntry_room_excluded_dates

function deleteHotelComplimentaryRoomExcludedDates($hids)
{
if(is_array($hids))
{
$hids=implode($hids,',');	
}
$sql='DELETE hotel_cmplimntry_room_excluded_dates
FROM hotel_cmplimntry_room_excluded_dates 
INNER JOIN hotel_complimentary_room ON hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id
';
$sql.=' WHERE hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id  AND hotel_complimentary_room.hotel_id IN ('.$hids.')';
$this->db->query($sql);
return true;
}

//hotel_rooms_pricing_complimentary

function deleteHotelRoomsPricingComplimentary($hids)
{
if(is_array($hids))
{
$hidsD=implode($hids,',');	
}
else{
	$hidsD=$hids;
}
$sql='DELETE hotel_rooms_pricing_complimentary
FROM hotel_rooms_pricing_complimentary
INNER JOIN hotel_rooms_pricing ON hotel_rooms_pricing.pricing_id = hotel_rooms_pricing_complimentary.pricing_id
';
$sql.='WHERE  hotel_rooms_pricing_complimentary.pricing_id=hotel_rooms_pricing.pricing_id  AND   hotel_rooms_pricing.hotel_id IN ('.$hidsD.')';
$this->db->query($sql);
return true;
}


//hotel_rooms_pricing

function deleteHotelRoomsPricing($hids)
{
if(is_array($hids))
{
$hidsD=implode($hids,',');	
}
else{
	$hidsD=$hids;
}
$sql='DELETE hotel_rooms_pricing,hotel_rooms_pricing_details
FROM hotel_rooms_pricing
INNER JOIN hotel_rooms_pricing_details ON hotel_rooms_pricing.pricing_id = hotel_rooms_pricing_details.pricing_id
';
$sql.='WHERE  hotel_rooms_pricing_details.pricing_id=hotel_rooms_pricing.pricing_id  AND  hotel_rooms_pricing.hotel_id IN ('.$hidsD.')';
$this->db->query($sql);
return true;
}



function updateHotelDetails($hid,$data)
    {
	$this->db->where('hotel_id', $hid);
	$this->db->update('hotels', $data); 
	}

 function getHotelDetails($hname,$hcity,$postcode)
    {
        $this->db->select('hotel_id');
        $this->db->where('hotel_name', $hname);
		$this->db->where('city', $hcity);
		$this->db->where('post_code', $postcode);
        $query = $this->db->get('hotels');        
        if ($query->num_rows() > 0)
		{
		return $query->result(); 
		}
		else{
			return false;
		}
		
    }
// save hotel data into database

    function insertDetails($data)
    {
        if($this->db->insert('hotels', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
    function addPurpose($data)
    {
        if($this->db->insert('hotel_purpose', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
    function getPurpose($title)
    {
		$this->db->select('id');
		$this->db->where("title", $title); 
        $query = $this->db->get('hotel_purpose');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
	function getPurposeList()
    {
		$this->db->select('id,title'); 
        $query = $this->db->get('hotel_purpose');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
	function getPropertyTypeList()
    {
		$this->db->select('id,title'); 
        $query = $this->db->get('property_types');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}

function getHotelPropertyType($title)
    {
		$this->db->select('id');
		$this->db->where("title", $title); 
        $query = $this->db->get('property_types');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
	}
	
	
function addHotelPropertyType($data)
    {
        if($this->db->insert('property_types', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
function addHotalPaymentShedules($data)
	{
	 if($this->db->insert_batch('hotel_payment_shedules', $data))
        {
			return true;
		}
		return false;	
	}
	
function getHotalPaymentShedules($hotel_id)
	{
		$this->db->select('id,payment_option_id,payment_value');
		$this->db->where("hotel_id", $hotel_id); 
		$this->db->order_by("payment_value", "desc"); 
		$query = $this->db->get('hotel_payment_shedules');
		if ($query->num_rows() > 0)
		{
			return $query->result(); 
			}
		else{
			return false;
		}	
	}

function deleteHotalPaymentShedules($arrayIds,$hotelId)
    {
	if(!empty($arrayIds) && count($arrayIds)>0)
	{
	$this->db->where('hotel_id', $hotelId);
	$this->db->where_not_in('id', $arrayIds);
	$this->db->delete('hotel_payment_shedules');
	}
	return true;
	}	
function updateHotalPaymentShedules($data,$id='')
    {
	if($id!="")
	{
	$this->db->where('id',$id);
	$q = $this->db->get('hotel_payment_shedules'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('id',$id); 
	$this->db->update('hotel_payment_shedules',$data); 
	return $id;
	}	
	}
	else { 
	$this->db->insert('hotel_payment_shedules',$data);
	return  $this->db->insert_id();		
	}			
	return;
	}
	
function addHotelChain($data)
    {
        if($this->db->insert('hotel_chain_list', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotelChain($title)
    {
		$this->db->select('chain_id');
		$this->db->where("title", $title); 
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->chain_id;
		}
		else{
			return false;
		}
	}
	
function getHotelChainList()
    {
		$this->db->select('chain_id,title'); 
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
   
    /* hotel contact section */
    
  function addHotalContactInfo($data)
    {
        if($this->db->insert_batch('hotel_contact_details', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalContactInfo($hotel_id)
    {
		$this->db->select('contact_id,position,name,email,phone,extension'); 
		$this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_contact_details');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/*  hotel contact section */

/* hotel complimentary services section */
 
  function addHotalComplimentaryServices($data)
    {
        if($this->db->insert_batch('hotel_complimentary_services', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalComplimentaryServices($hotel_id)
    {
		$this->db->select('complimentary_services.cmpl_service_id,complimentary_services.service_name'); 
		$this->db->from('complimentary_services');
		$this->db->join('hotel_complimentary_services', 
						'complimentary_services.cmpl_service_id = hotel_complimentary_services.cmpl_service_id');
		$this->db->where("hotel_complimentary_services.hotel_id", $hotel_id);
		$query = $this->db->get();
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/*  hotel complimentary services section */
  
  /* hotel contarct section */
    
  function addHotalContract($data)
    {
        if($this->db->insert('hotel_contract_info', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotalContract($hotel_id)
    {
		$this->db->select('id,start_date,end_date,signed_by,contract_file'); 
		$this->db->where("hotel_id", $hotel_id);
		$this->db->order_by("start_date", "desc"); 
        $query = $this->db->get('hotel_contract_info');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/*  hotel contarct section */

/*  hotel Comlimentary  room section */
    
  function addHotalComlimentaryRoom($data)
    {
        if($this->db->insert('hotel_complimentary_room', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotalComlimentaryRoom($hotel_id)
    {
		$this->db->select('cmpl_room_id,room_night,room_type,start_date,end_date,upgrade'); 
		$this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_complimentary_room');
        if ($query->num_rows() > 0)
		{
        return $query->row(); 
		}
		else{
			return false;
		}
	}
	
  function updateHotalComlimentaryRoom($data,$hotel_id,$cmpl_room_id='')
    {	
	if($cmpl_room_id!="")
	{
	$this->db->where("hotel_id", $hotel_id);
	$this->db->where('cmpl_room_id',$cmpl_room_id);
	$q = $this->db->get('hotel_complimentary_room'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('cmpl_room_id',$cmpl_room_id); 
	$this->db->update('hotel_complimentary_room',$data); 
	return $cmpl_room_id;
	}
	else { 
	$this->db->insert('hotel_complimentary_room',$data);
	return  $this->db->insert_id();		
	}		
	}
	else { 
	$this->db->insert('hotel_complimentary_room',$data);
	return  $this->db->insert_id();		
	}			
	return;
	}	
	
  function addHotalComlimentaryRoomExcludedDate($data)
    {
        if($this->db->insert_batch('hotel_cmplimntry_room_excluded_dates', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalComlimentaryRoomExcludedDate($hotel_cmpl_room_id)
    {
		$this->db->select('id,exclude_date_from,excluded_date_to'); 
		$this->db->where("cmpl_room_id", $hotel_cmpl_room_id);
        $query = $this->db->get('hotel_cmplimntry_room_excluded_dates');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
	function getHotalComlimentaryRoomExcludedDateByHotel($hotel_id=null)
    {
		$this->db->select('id,exclude_date_from,excluded_date_to'); 
		$this->db->from('hotel_cmplimntry_room_excluded_dates');
		$this->db->join('hotel_complimentary_room', 
						'hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id');
		$this->db->where("hotel_complimentary_room.hotel_id", $hotel_id);
		$this->db->order_by("hotel_cmplimntry_room_excluded_dates.exclude_date_from", "desc"); 	   
	   $query = $this->db->get();
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
function deleteHotalComlimentaryRoomExcludedDate($arrayIds,$cmpl_room_id)
    {
	if(!empty($arrayIds) && count($arrayIds)>0)
	{
	$this->db->where('cmpl_room_id', $cmpl_room_id);
	$this->db->where_not_in('id', $arrayIds);
	$this->db->delete('hotel_cmplimntry_room_excluded_dates');
	}
	return true;
	}	
function updateHotalComlimentaryRoomExcludedDate($data,$id='')
    {
	if($id!="")
	{
	$this->db->where('id',$id);
	$q = $this->db->get('hotel_cmplimntry_room_excluded_dates'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('id',$id); 
	$this->db->update('hotel_cmplimntry_room_excluded_dates',$data); 
	return $id;
	}	
	}
	else { 
	$this->db->insert('hotel_cmplimntry_room_excluded_dates',$data);
	return  $this->db->insert_id();		
	}			
	return;
	}

/*  hotel Comlimentary  room  section end */

  function addHotalPropertyTypes($data)
    {
        if($this->db->insert_batch('hotel_property_types', $data))
        {
			return true;
		}
		return false;
    } 

/*  hotel bank details section */
    
  function addHotalBankingInfo($data)
    {
        if($this->db->insert_batch('hotel_bank_accounts', $data))
        {
			return true;
		}
		return false;
    }   
    
  function getHotalBankInfo($hotel_id)
    {
		$this->db->select('id,account_number,account_name,bank_name,bank_address,bank_ifsc_code'); 
		$this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_bank_accounts');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/* hotel bank details section */

/*  hotel Confrence details section */
    
  function addHotalConfrence($data)
    {
        if($this->db->insert_batch('hotel_confrences', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalConfrence($hotel_id)
    {
		$this->db->select('id,confrence_name,area,celling_height,classroom,theatre,banquet,reception,conference,ushape,hshape'); 
		$this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_confrences');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/*  hotel Confrence details section */

/*  hotel Cancellation details section */
    
  function addHotalCancellation($data)
    {
        if($this->db->insert_batch('hotel_cancellation', $data))
        {
			return true;
		}
		return false;
    }
 
  function getHotalCancellation($hotel_id)
    {
		$this->db->select('id,seasion,cancelled_before,payment_request'); 
		$this->db->where("hotel_id", $hotel_id);     
		$this->db->order_by("payment_request", "desc");
		 $query = $this->db->get('hotel_cancellation');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
function deleteHotalCancellation($arrayIds,$hotelId)
    {
	if(!empty($arrayIds) && count($arrayIds)>0)
	{
	$this->db->where('hotel_id', $hotelId);
	$this->db->where_not_in('id', $arrayIds);
	$this->db->delete('hotel_cancellation');
	}
	return true;
	}	
function updateHotalCancellation($data,$id='')
    {
	if($id!="")
	{
	$this->db->where('id',$id);
	$q = $this->db->get('hotel_cancellation'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('id',$id); 
	$this->db->update('hotel_cancellation',$data); 
	return $id;
	}	
	}
	else { 
	$this->db->insert('hotel_cancellation',$data);
	return  $this->db->insert_id();		
	}			
	return;
	}

/*  hotel Cancellation details section */   

/*  hotel Renovation schedule details section */
    
  function addHotalRenovationSchedule($data)
    {
        if($this->db->insert_batch('hotel_renovation_schedule', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalRenovationSchedule($hotel_id)
    {
		$this->db->select('rnv_id,date_from,date_to,renovation_type,area_effected'); 
		$this->db->where("hotel_id", $hotel_id);
		$this->db->order_by("date_from", "desc"); 	   
        $query = $this->db->get('hotel_renovation_schedule');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
function deleteHotalRenovationSchedule($arrayIds,$hid)
    {
	if(!empty($arrayIds) && count($arrayIds)>0)
	{
	$this->db->where('hotel_id', $hid);
	$this->db->where_not_in('rnv_id', $arrayIds);
	$this->db->delete('hotel_renovation_schedule');
	}
	return true;
	}	
function updateHotalRenovationSchedule($data,$id='')
    {	
	if($id!="")
	{
	$this->db->where('rnv_id',$id);
	$q = $this->db->get('hotel_renovation_schedule'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('rnv_id',$id); 
	$this->db->update('hotel_renovation_schedule',$data); 
	return $id;	
	}
	else { 
	$this->db->insert('hotel_renovation_schedule',$data);
	return $this->db->insert_id();	
	}	
	}
	else { 
	$this->db->insert('hotel_renovation_schedule',$data); 
	return $this->db->insert_id();
	}
	return ;
	}
/*  hotel Renovation schedule details section */

/*  hotel Distance  from section */
    
  function addHotalDistanceFrom($data)
    {
        if($this->db->insert_batch('hotel_distance', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalDistanceFrom($hotel_id,$dtype='')
    {
		$this->db->select('id,distance_from,distance,distance_type'); 
		$this->db->where("hotel_id", $hotel_id);
		if($dtype!='')
		{
			$this->db->where("distance_type", $dtype); /* dtype 1 for  normal, and 2 for airport*/
		}
        $query = $this->db->get('hotel_distance');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/*  hotel Distance section */

/* hotel Facility section */
    
  function addHotalFacilities($data)
    {
        if($this->db->insert_batch('hotel_facilities', $data))
        {
			return true;
		}
		return false;
    }
    
  function getHotalFacilities($hotel_idhotel_id)
    {
		$this->db->select('facilities.facility_id,facilities.facility_title'); 
		$this->db->from('facilities');
		$this->db->join('hotel_facilities','facilities.facility_id = hotel_facilities.facility_id');
		$this->db->where("hotel_facilities.hotel_id", $hotel_id);
		$query = $this->db->get();
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
/*  hotel Facility  section end */

/*  hotel pricings  section */
function addHotalRoomsPricingData($data)
    {
        if($this->db->insert('hotel_rooms_pricing', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
function addHotalRoomPricingComplimentary($data,$pricing_id=null)
	{
	if($pricing_id!="")
	{
	$this->db->where('pricing_id', $pricing_id);
	$this->db->delete('hotel_rooms_pricing_complimentary');	
	}
	 if($this->db->insert_batch('hotel_rooms_pricing_complimentary',$data))
        {
			return true;
		}
		return false;	
	}
	
function updateHotalRoomsPricingData($data,$pricing_id)
	{
	if($pricing_id!="")
	{
	$this->db->where('pricing_id',$pricing_id);
	$q = $this->db->get('hotel_rooms_pricing'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('pricing_id',$pricing_id); 
	$this->db->update('hotel_rooms_pricing',$data); 
	return $pricing_id;	
	}
	else { 
	$this->db->insert('hotel_rooms_pricing',$data);
	return $this->db->insert_id();	
	}	
	}
	else { 
	$this->db->insert('hotel_rooms_pricing',$data); 
	return $this->db->insert_id();
	}
	return ;
	}
	
function deleteHotalRoomsPricingData($arrayIds,$hid)
    {
	if(!empty($arrayIds) && count($arrayIds)>0)
	{
	$this->db->where('hotel_id', $hid);
	$this->db->where_not_in('pricing_id', $arrayIds);
	$this->db->delete('hotel_rooms_pricing');
	}
	return true;
	}	

function addHotelRroomsPricingDetails($data)
	{
	 if($this->db->insert_batch('hotel_rooms_pricing_details', $data))
        {
			return true;
		}
		return false;	
	}
	
function updateHotelRroomsPricingDetails($data,$pricingDetId,$pricing_id)
	{
	if($pricing_id!="" && $pricingDetId!='')
	{
	$this->db->where('pricing_id',$pricing_id);
	$this->db->where('id',$pricingDetId);
	$q = $this->db->get('hotel_rooms_pricing_details'); 
	if($q->num_rows() > 0 ) { 
	$this->db->where('pricing_id',$pricing_id); 
	$this->db->where('id',$pricingDetId);
	$this->db->update('hotel_rooms_pricing_details',$data); 
	return $pricing_id;	
	}
	else { 
	$this->db->insert('hotel_rooms_pricing_details',$data);
	return $this->db->insert_id();	
	}	
	}
	else { 
	$this->db->insert('hotel_rooms_pricing_details',$data); 
	return $this->db->insert_id();
	}		
	return ;
	}

function deleteHotalHotelRroomsPricingDetails($arrayIds,$pricing_id)
    {
	if(!empty($arrayIds) && count($arrayIds)>0)
	{
	$this->db->where('pricing_id', $pricing_id);
	$this->db->where_not_in('id', $arrayIds);
	$this->db->delete('hotel_rooms_pricing_details');
	}
	return true;
	}	
	
function getHotalRoomsPricingData($hotel_id)
    {
	$this->db->select('pricing_id,room_type,inclusions,
	curency_code,max_adult,max_child,inventory,period_from,period_to'); 
	$this->db->where("hotel_id", $hotel_id);
	$query = $this->db->get('hotel_rooms_pricing');
     if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
		return false;
		}
    }	
	
function getHotelRroomsPricingDetails($pricing_id)
	{
	$this->db->select('id,market_id,double_price,
	triple_price,quad_price,breakfast_price,half_board_price,
	all_incusive_adult_price,extra_adult_price,extra_child_price,extra_bed_price'); 
	$this->db->where("pricing_id", $pricing_id);
	$query = $this->db->get('hotel_rooms_pricing_details');
     if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
		return false;
		}	
	}
function getHotalRoomPricingComplimentary($pricing_id)
	{
	$this->db->select('cmpl_service_id'); 
	$this->db->where("pricing_id", $pricing_id);
	$query = $this->db->get('hotel_rooms_pricing_complimentary');
     if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
		return false;
		}		
	
	}	
	
/*  hotel pricings  section  end*/

function get_gallary()
	{
	$this->db->select('gallery_id,gallery_title'); 
	$this->db->order_by('gallery_id','ASC');
	$query = $this->db->get('gallery');
	return $result = $query->result();
	}
	
function insert_images($data)
	{
	$id = $this->db->insert('gallery_images', $data);
	return $id;
	}

	function update_images($identity,$data)
    {
	$this->db->where('identifier', $identity);
	$this->db->update('gallery_images', $data); 
	}
	
	function get_dbidentity($identity){
		$this->db->where('identifier', $identity);
		$query = $this->db->get('gallery_images');
		if ($query->num_rows() > 0)
		{
        return $query->row(); 
		}
		else{
			return false;
		}
	}
	
	function clear_gallery_image_table(){
		
		$this->db->query("DELETE FROM `gallery_images` where `upload_date` < DATE_SUB(NOW() , INTERVAL 24 HOUR) and hotel_id IS NULL");
	}
	
	function delete_images($imgname,$galid){
		
		$this->db->query("delete from gallery_images where image_name = '".$imgname."' and gallery_id = '".$galid."'");
	}
	
	function gallery_detail(){		
		$query = $this->db->query("select * from gallery");
		
		if ($query->num_rows() > 0)
		{
        return $query->result_array(); 
		}
		else{
			return false;
		}
	}
	
	/******************************************* Edit information queries start here *******************************************/
	function get_gallaryimg($hid)
	{
		$this->db->select('gi.gallery_id, gi.id, gi.image_name'); 
		$this->db->from('gallery_images gi');
		$this->db->where("gi.hotel_id", $hid);
		$query = $this->db->get();
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	
	function deletehotelgalleryimg($imgid,$hotelgalid,$hotelid){
			$this->db->where('gallery_id',$hotelgalid);
			$this->db->where('id',$imgid);
			$this->db->where('hotel_id',$hotelid);
			$this->db->delete('gallery_images');
			return $imgid;
	}
	
	function getHotelbyid($hid)
    {
		$this->db->select('hotels.hotel_id,
							hotels.hotel_name,
							hotels.hotel_address,
							hotels.hotel_code,
							countries.country_name,
							hotels.city,
							hotels.district,
							hotels.currency,
							hotels.post_code,
							hotels.dorak_rating,
							hotels.star_rating,
							hotels.child_age_group_id,
							hotel_purpose.id as purpose,
							hotels.last_updated,
							cn.chain_id as chain,
							hs.id as status,
							c.country_name as hotel_country,
							pt.id as property_types');
		$this->db->from('hotels');
		$this->db->join('hotel_status hs', 'hotels.status = hs.id');
		$this->db->join('countries', 'hotels.country_code = countries.country_code');
		$this->db->join('hotel_chain_list cn', 'cn.chain_id = hotels.chain_id','left');
		$this->db->join('hotel_purpose', 'hotel_purpose.id = hotels.purpose','left');
		$this->db->join('hotel_property_types  hpt', 'hotels.hotel_id = hpt.hotel_id','left');
		$this->db->join('countries  c', 'c.country_code = hotels.country_code','left');
		$this->db->join('property_types  pt', 'pt.id = hpt.property_type_id','left');
	
		$this->db->where('hotels.hotel_id', $hid);		
        $query = $this->db->get();
        $results = $query->row();
		if($results){ return $results; } return false;	
    }
	
	function deleteHotalpropertytype($hid){
		$this->db->where('hotel_id',$hid);
		$this->db->delete('hotel_property_types');
		return true;
	}
	
	function getpropertyid($hid){
		
		$this->db->select('hpt.property_type_id');
		$this->db->from('hotel_property_types hpt');
		$this->db->where('hpt.hotel_id', $hid);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
		$result = $query->result_array();
		foreach($result as $re)
		{
			return $ids = array_column($result, 'property_type_id');
		}
		}else
		{
			return false;
		}
	}

	/******************************************* Update hotel contact information *******************************************/
	function getcontactinfobyid($hid){
		$this->db->select('hcd.position as designation, hcd.name, hcd.email, hcd.phone, hcd.extension, hcd.contact_id');
		$this->db->from('hotel_contact_details hcd');
		$this->db->join('positions as p','p.title = hcd.position');
		$this->db->where('hcd.hotel_id', $hid);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $result = $query->result();
		}else{
			return false;
		}
		
	}
	
	function updateHotalContact($data,$id='') {
	if($id!="")	{
		$this->db->where('contact_id',$id);
		$q = $this->db->get('hotel_contact_details'); 
		if($q->num_rows() > 0 ) { 
			$this->db->where('contact_id',$id); 
			$this->db->update('hotel_contact_details',$data); 
			return $id;
		} else { 
			$this->db->insert('hotel_contact_details',$data);
			return $this->db->insert_id();
		}	
	} else { 
		$this->db->insert('hotel_contact_details',$data);
		return $this->db->insert_id();
	}
		return;
	}
	
	function deleteHotalContact($arrayIds,$hid)  {
		$this->db->where('hotel_id', $hid);
		$this->db->where_not_in('contact_id', $arrayIds);
		$this->db->delete('hotel_contact_details');
		return true;
	}
	/************************************ Update hotel contact information  close *********************************/
	
	/************************************ Update city information start here *********************************/
	function getdistanceForCity($hid){
		
		$this->db->select('hd.distance_from as city_name_normal, hd.distance as dis_city, hd.id');
		$this->db->from('hotel_distance hd');
		$this->db->where('hd.distance_type', 1);
		$this->db->where('hd.hotel_id', $hid);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $result = $query->result();
		}else{
			return false;
		}
	}
	
	function updateHotalcity($data,$id='') {
	if($id!="")	{
		$this->db->where('id',$id);
		$q = $this->db->get('hotel_distance'); 
		if($q->num_rows() > 0 ) { 
			$this->db->where('id',$id);
			$this->db->where('distance_type',1);
			$this->db->update('hotel_distance',$data); 
			return $id;
		} else { 
			$this->db->insert('hotel_distance',$data);
			return $this->db->insert_id();
		}	
	} else { 
		$this->db->insert('hotel_distance',$data);
		return $this->db->insert_id();
	}
		return;
	}
	
	function deleteHotalcity($arrayIds,$hid)  {
		$this->db->where('hotel_id', $hid);
		$this->db->where('distance_type', '1');
		$this->db->where_not_in('id', $arrayIds);
		$this->db->delete('hotel_distance');
		return true;
	}
	
	/************************************ Update city information close here *********************************/
	
	/************************************ Update airport information start here *********************************/
	function getdistanceForAirport($hid){
		
		$this->db->select('hd.distance_from as city_name_airport, hd.distance as dis_airport, hd.id');
		$this->db->from('hotel_distance hd');
		$this->db->where('hd.distance_type', 2);
		$this->db->where('hd.hotel_id', $hid);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $result = $query->result();
		}else{
			return false;
		}
	}
	
	
	function updatedistanceForAirport($data,$id='') {
	if($id!="")	{
		$this->db->where('id',$id);
		$q = $this->db->get('hotel_distance'); 
		if($q->num_rows() > 0 ) { 
			$this->db->where('id',$id); 
			$this->db->where('distance_type','2'); 
			$this->db->update('hotel_distance',$data); 
			return $id;
		} else { 
			$this->db->insert('hotel_distance',$data);
			return $this->db->insert_id();
		}	
	} else { 
		$this->db->insert('hotel_distance',$data);
		return $this->db->insert_id();
	}
		return;
	}
	
	function deletedistanceForAirport($arrayIds,$hid)  {
		$this->db->where('hotel_id', $hid);
		$this->db->where('distance_type','2');
		$this->db->where_not_in('id', $arrayIds);
		$this->db->delete('hotel_distance');
		return true;
	}
	
	/************************************ Update airport information close here *********************************/
	
	
	/************************************ hotal facilities information start here *********************************/
	 function hotelFacility($hid){
		
		$this->db->select('*');
		$this->db->from('hotel_facilities');
		$this->db->where('hotel_id', $hid);
		$query = $this->db->get();
		 if ($query->num_rows() > 0)
		{
		$result = $query->result_array();
		//echo $this->db->last_query(); die;
		foreach($result as $re)
		{
			return $ids = array_column($result, 'facility_id');
		}
		}else
		{
			return false;
		}
	} 
	
	
	function deletehotelFacility($hid)  {
		$this->db->where('hotel_id', $hid);
		$this->db->delete('hotel_facilities');
		return true;
	}
	
	/************************************ hotal facilities information close here *********************************/
	
	
	/************************************ room facilities information start here *********************************/
	function roomFacility($hid){
		
		$this->db->select('hcs.cmpl_service_id');
		$this->db->from('hotel_complimentary_services hcs');
		$this->db->where('hcs.hotel_id', $hid);
		$query = $this->db->get();
		 if ($query->num_rows() > 0)
		{
		$result = $query->result_array();
		foreach($result as $re)
		{
			return $ids = array_column($result, 'cmpl_service_id');
		}
		}else
		{
			return false;
		}
	} 
	
	function deleteroomFacility($hid)  {
		$this->db->where('hotel_id', $hid);
		$this->db->delete('hotel_complimentary_services');
		return true;
	}
	
	/************************************ room facilities information close here *********************************/
	
	/************************************** Update hotel bank accounts *************************************************/
	
	function hotelBankAccount($hid){
		$this->db->select('*');
		$this->db->from('hotel_bank_accounts hba');
		$this->db->where('hba.hotel_id', $hid);
		$query = $this->db->get();
		$result = $query->result();
		if($query->num_rows() > 0){
			return $result;
		}else{
			return false;
		}
	}
	
	function updateHotalbankaccounts($data,$id='') {
		
	if($id!="")	{		
		$this->db->where('id',$id);
		$q = $this->db->get('hotel_bank_accounts'); 
		if($q->num_rows() > 0 ) { 
			$this->db->where('id',$id); 
			$this->db->update('hotel_bank_accounts',$data); 
			return $id;
		} else { 
			$this->db->insert('hotel_bank_accounts',$data); 
			return $this->db->insert_id();
		}	
	} else { 
		$this->db->insert('hotel_bank_accounts',$data); 
		return $this->db->insert_id();
	}
		return ;
	}
	
	function deleteHotalbankaccounts($arrayIds,$hid)  {
		$this->db->where('hotel_id', $hid);
		$this->db->where_not_in('id', $arrayIds);
		$this->db->delete('hotel_bank_accounts');
		return true;
	}
	
	/******************************* Update hotel bank accounts information closed ****************************************/
	
	function updateHotelInformation($hid,$hotel_detail) {
		$this->db->where('hotel_id', $hid);
		$this->db->update('hotels', $hotel_detail); 
	}
}
