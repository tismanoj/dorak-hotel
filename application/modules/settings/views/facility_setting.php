<h2>Facility Detail</h2>
<div class="search-box">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add room facility</a>
<div id="toggle" class="toggle-block">
<?php echo form_open_multipart($addAction,array('class' => 'room_facilityadd', 'id' => 'room_facilityadd'));?>
<?php echo form_input($add_input_name)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>

<a class="add-hotel" href="javascript:void(0);" id="hfacilityid">Add hotel facility</a>
<div id="toggle-block" class="toggle-block">
<?php echo form_open_multipart($addHFAction,array('class' => 'hotel_facilityadd', 'id' => 'hotel_facilityadd'));?>
<?php echo form_input($add_input_hfacility_name)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>

</div>
<div class="setting-room-type">
<div class="mian-head setting-section">
<div class="facility-left">
<?php echo form_open($addAction,array('class' => 'section_edit', 'id' => 'section_edit'));?>

<table id="hotel-hfacility-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="50%">
<thead>
<tr>
<th>Hotel Level</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listHFData && count($listHFData)>0){
	foreach($listHFData as $hfData)
	{
	?>
	<tr><td><label id="label-hfblock-<?php echo $hfData->facility_id?>"><?php echo $hfData->facility_title;?></label><span class="edit-block" style="display:none;" id="edit-hfblock-<?php echo $hfData->facility_id?>"><input type="text" data-validation-length="min3" maxlength = "100" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  id="new-hftitle-<?php echo $hfData->facility_id?>" name="new_hftitle_<?php echo $hfData->facility_id?>" value="<?php echo $hfData->facility_title;?>">
	<a class="save_button" id="savelnkhf_<?php echo $hfData->facility_id?>" onclick="inlineHFsaveAction(<?php echo $hfData->facility_id;?>)">save</a>
	<a class="cancel_button" id="cancellnkhf_<?php echo $hfData->facility_id?>" onclick="cancelHFsaveAction(<?php echo $hfData->facility_id;?>)">cancel</a>
	
	</span></td>
	<td>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $hfData->facility_id?>" href="javascript:void(0);" onclick="toggleHFEdit(<?php echo $hfData->facility_id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delHFAction ?>/<?php echo $hfData->facility_id?>" onclick="return confirm('Are you sure?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
<?php   echo form_close();?>
</div>
<div class="facility-right">
<?php echo form_open($addAction,array('class' => 'section_edit2', 'id' => 'section_edit2'));?>

<table id="hotel-rfacility-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="50%">
<thead>
<tr>
<th>Room Level</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr><td><label id="label-block-<?php echo $sData->cmpl_service_id?>"><?php echo $sData->service_name;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->cmpl_service_id?>"><input type="text" maxlength = "100" data-validation-length="min3" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  id="new-title-<?php echo $sData->cmpl_service_id?>" name="new_title_<?php echo $sData->cmpl_service_id?>" value="<?php echo $sData->service_name;?>">
	<a class="save_button" id="savelnk_<?php echo $sData->cmpl_service_id?>" onclick="inlinesaveAction(<?php echo $sData->cmpl_service_id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->cmpl_service_id?>" onclick="cancelsaveAction(<?php echo $sData->cmpl_service_id;?>)">cancel</a>
	
	</span></td>
	<td>
	<span>
	<a title="Edit" rel="<?php echo $sData->cmpl_service_id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->cmpl_service_id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->cmpl_service_id?>" onclick="return confirm('Are you sure?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
<?php   echo form_close();?>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();  
	$('#toggle-block').hide(); 	
   $.validate({
	form : '#room_facilityadd'
	});
  });
  
  
   $(document).on('click','#hfacilityid',function(){
    var $this= $(this);
    $('#toggle-block').toggle();
	$('#toggle').hide();	
   $.validate({
	form : '#hotel_facilityadd'
	});
  });
  
  
  
  $(document).ready(function (){
	  
	 $.validate({
	form : '#section_edit'
	}); 
	
	$.validate({
	form : '#section_edit2'
	}); 
   var table = $('#hotel-rfacility-table').DataTable(
   {
    "order": [[ 0, "asc" ]],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "90%" },
    { "width": "10%" }
  ] ,
  "aoColumns": [
      null,
      { "bSortable": false }
    ]
  }
   );
   
   var tableh = $('#hotel-hfacility-table').DataTable(
   {
   "order": [[ 0, "asc" ]],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "90%" },
    { "width": "10%" }
  ],
  "aoColumns": [
      null,
      { "bSortable": false }
    ]
  }
   );
   
   
   
   
  });
  
  function inlinesaveAction(rowid)
  {
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	 { 
	 var newval= $('#new-title-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		data: {newname: newval,id: rowid},
		success: function(res) {		
		if (res=='exists')
		{
		alert(newval +' Already exists.');
		}
		else if(res==newval)
		{
		$('#new-title-'+rowid).val(res);	
		$('#label-block-'+rowid).html(res);
		}
		}
		});
		}
	  });
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	   
  }
  
  
  function inlineHFsaveAction(rowid)
  {
	$('#new-hftitle-'+rowid).validate(function(valid, elem) {
	if(valid)
	 {
	 var newval= $('#new-hftitle-'+rowid).val();
	 $('#edit-hfblock-'+rowid).hide();  
	 $('#label-hfblock-'+rowid).show(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editHFAction?>",
		data: {newname: newval,id: rowid},
		success: function(res) {		
		if (res=='exists')
		{
		alert(newval +' Already exists.');
		}
		else if(res==newval)
		{
		$('#new-hftitle-'+rowid).val(res);	
		$('#label-hfblock-'+rowid).html(res);
		}
		}
		});
		}
	  });
  }
  function toggleHFEdit(rowid)
  {
	   $('#edit-hfblock-'+rowid).toggle(); 
	   $('#label-hfblock-'+rowid).toggle(); 	   
  }
  
  function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show();   
  }
  
  function cancelHFsaveAction(rowid)
  {
	 $('#edit-hfblock-'+rowid).hide();  
	 $('#label-hfblock-'+rowid).show();  
  }
  
</script>