<?php
class Setting_model extends AdminModel {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
function addHotelChain($data)
    {
        if($this->db->insert('hotel_chain_list', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotelChain($title,$chain_id='')
    {
		$this->db->select('chain_id');
		$this->db->where("title", $title);
		if($chain_id!="")	
		$this->db->where("chain_id !=", $chain_id);			
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->chain_id;
		}
		else{
			return false;
		}
	}
	
function getHotelChainList()
    {
		$this->db->select('chain_id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	
/*  hotel pricings  section  end*/
}


function delHotelChain($chainId)
{
$this->db->where('chain_id',$chainId);
return $this->db->delete('hotel_chain_list');	
}

function updateHotelChain($chainId,$data)
    {
	$this->db->where('chain_id', $chainId);
	$this->db->update('hotel_chain_list', $data); 
	}


	#### purpose section ###
	
		
function addHotelPurpose($data)
    {
        if($this->db->insert('hotel_purpose', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotelPurpose($title,$eid='')
    {
		$this->db->select('id');
		$this->db->where("title",$title); 
		if($eid!="")	
		$this->db->where("id !=", $eid);	
        $query = $this->db->get('hotel_purpose');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getHotelPurposeList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('hotel_purpose');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	
}


function delHotelPurpose($chainId)
{
$this->db->where('id',$chainId);
return $this->db->delete('hotel_purpose');	
}

function updateHotelPurpose($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('hotel_purpose', $newdata); 
	}

	####purpose section end ####
	
	
#### PropertyType section ###
	
		
function addHotelPropertyType($data)
    {
        if($this->db->insert('property_types', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotelPropertyType($title,$eid='')
    {
		$this->db->select('id');
		$this->db->where("title",$title); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('property_types');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getHotelPropertyTypeList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('property_types');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	
}


function delHotelPropertyType($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('property_types');	
}

function updateHotelPropertyType($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('property_types', $newdata); 
	}

####Property Type section end ####
	


#### Facility section ###
	
		
function addHotelFacility($data)
    {
        if($this->db->insert('facilities', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getHotelFacility($title,$eid='')
    {
		$this->db->select('facility_id');
		$this->db->where("facility_title",$title); 
		if($eid!="")	
		$this->db->where("facility_id !=", $eid);
        $query = $this->db->get('facilities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->facility_id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getHotelFacilityList()
    {
		$this->db->select('facility_id,facility_title'); 
		$this->db->order_by('facility_title');
        $query = $this->db->get('facilities');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delHotelFacility($dId)
{
$this->db->where('facility_id',$dId);
return $this->db->delete('facilities');	
}

function updateHotelFacility($dbId,$newdata)
    {
	$this->db->where('facility_id', $dbId);
	$this->db->update('facilities', $newdata); 
	}

	
function addRoomFacility($data)
    {
        if($this->db->insert('complimentary_services', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getRoomFacility($title,$eid='')
    {
		$this->db->select('cmpl_service_id');
		$this->db->where("service_name",$title); 
		if($eid!="")	
		$this->db->where("cmpl_service_id !=", $eid);
        $query = $this->db->get('complimentary_services');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->cmpl_service_id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getRoomFacilityList()
    {
		$this->db->select('cmpl_service_id,service_name'); 
		$this->db->order_by('service_name');
        $query = $this->db->get('complimentary_services');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	
}


function delRoomFacility($dId)
{
$this->db->where('cmpl_service_id',$dId);
return $this->db->delete('complimentary_services');	
}

function updateRoomFacility($dbId,$newdata)
    {
	$this->db->where('cmpl_service_id', $dbId);
	$this->db->update('complimentary_services', $newdata); 
	}	
	

####Facility  section end ####


####Curency  section  ####



function addCurrency($data)
    {
        if($this->db->insert('currencies', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getCurrency($code,$usval='',$eid='')
    {
		$this->db->select('id');
		$this->db->where("code",$code); 
		if($usval!='')
		$this->db->where("currency_usd_value",$usval); 

		if($eid!="")	
		$this->db->where("id !=", $eid);
	
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getCurrencyList()
    {
		$this->db->select('id,code,currency_usd_value'); 
		$this->db->order_by('code');
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delCurrency($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('currencies');	
}

function deleteCurencyCodesData($delCodesArray)
{
$this->db->where_in('code', $delCodesArray);
return $this->db->delete('currencies');		
}

function updateCurrency($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('currencies', $newdata); 
	}	
	
 function importCurencyData($data) {
	 // clear old data
	 $this->db->truncate('currencies'); 
	 // import new data
    return $this->db->insert_batch('currencies',$data);
	}	
	
function addCurencyData($data) {
      return $this->db->insert_batch('currencies',$data);
	 }	
####Curency  section  end ####


####Country  section  ####

function addCountry($data)
    {
        if($this->db->insert('countries', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getCountry($cname)
    {
		$this->db->select('id');
		$this->db->where("country_name",$cname); 
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
	
function checkCountryExists($Id='',$name)
    {
		$this->db->select('id');
		if($Id!='')
		$this->db->where("id !=",$Id); 
		$this->db->where("country_name",$name);	
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	
function checkCountryCodeExists($Id='',$ccode)
    {
		$this->db->select('id');
		if($Id!='')
		$this->db->where("id !=",$Id); 
		$this->db->where("country_code",$ccode);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	
	
	
function getCountryCode($cid)
    {
		$this->db->select('country_code');
		$this->db->where("id",$cid); 
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->country_code;
		}
		else{
			return false;
		}
		return false;
	}
	
function getCountryName($cid)
    {
		$this->db->select('country_name');
		$this->db->where("id",$cid); 
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->country_name;
		}
		else{
			return false;
		}
		return false;
	}
	
	
function getCountriesList()
    {
		$this->db->select('id,country_code as code,country_name'); 
		$this->db->order_by('country_name','ASC');
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delCountry($dId)
{
$this->delCities($dId);// delete cities of the selected cuntry
$this->db->where('id',$dId);
return $this->db->delete('countries');	
}


function updateCountry($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('countries', $newdata); 
	}	

	
####Country  section   end####

####City  section  ####	

function addCity($data)
    {
        if($this->db->insert('country_cities', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getCity($cname)
    {
		$this->db->select('id');
		$this->db->where("city_name",$cname); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
	
function checkCityExists($name,$cid='',$cuntryId='')
    {
		$this->db->select('id');
		$this->db->where("city_name",$name); 
		if($cid!='')
		$this->db->where("id !=",$cid); 
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId);	
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	
	function checkCityCodeExists($ccode,$cid='',$cuntryId='')
    {
		$this->db->select('id');
		if($cid!='')
		$this->db->where("id !=",$cid); 
	
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId); 
	
		$this->db->where("city_code",$ccode); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	
	
function getCityByCode($ccode)
    {
		$this->db->select('id');
		$this->db->where("city_code",$ccode); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
	
function getCitiesList($cuntryId)
    {
		$this->db->select('id,city_name,city_code'); 
		$this->db->order_by('city_name','ASC');
		$this->db->where("country_id",$cuntryId); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delCity($dId)
{
$this->delCityDistricts($dId);
$this->db->where('id',$dId);
return $this->db->delete('country_cities');	
}

function delCities($cuntryId)//  by passing country id
{
$this->delCountryCityDistricts($cuntryId);
$this->db->where('country_id',$cuntryId);
return $this->db->delete('country_cities');	
}

function updateCity($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('country_cities', $newdata); 
	}

	
function getCityName($cId)
    {
		$this->db->select('city_name');
		$this->db->where("id",$cId); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->city_name;
		}
		else{
			return false;
		}
		return false;
	}
		
	
####City  section end  ####	
	
	
####District  section  ####	

function addDistrict($data)
    {
        if($this->db->insert('city_districts', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getDistrict($dname,$cid='')
    {
		$this->db->select('id');
		$this->db->where("district_name",$dname); 
		if($cid!='')
		$this->db->where("city_id",$cid); 
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
	
function checkDistrictExists($name,$cuntryId='',$cid='',$did='')
    {
		$this->db->select('id');
		$this->db->where("district_name",$name);
		if($cid!='')
		$this->db->where("city_id",$cid); 
		if($did!='')
		$this->db->where("id !=",$did); 
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId);	
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	

function getCityDistricts($cityId)
    {
		$this->db->select('id,district_name');		
		$this->db->where("city_id",$cityId); 
		$this->db->order_by('district_name','ASC');
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}	

	
function getDistrictList($cuntryId,$cityId)
    {
		$this->db->select('id,district_name');		
		$this->db->where("country_id",$cuntryId); 
		$this->db->where("city_id",$cityId); 
		$this->db->order_by('district_name','ASC');
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delDistrict($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('city_districts');	
}

function delCityDistricts($city_id)//  by passing country id
{
$this->db->where('city_id',$city_id);
return $this->db->delete('city_districts');	
}

function delCountryCityDistricts($country_id)
{
$this->db->where('country_id',$country_id);
return $this->db->delete('city_districts');	
}

function updateDistrict($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('city_districts', $newdata); 
	}	
	
####District  section end  ####		
	
	
	

####Market  section  ####

function addMarket($data)
    {
        if($this->db->insert('markets', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getMarket($cname,$eid='')
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('markets');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getMarketList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('markets');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delMarket($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('markets');	
}

function updateMarket($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('markets', $newdata); 
	}	
	
	
####Travel Type  section  ####

function addTravelType($data)
    {
        if($this->db->insert('travel_types', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getTravelType($cname,$eid="")
    {
		$this->db->select('travel_type_id');
		$this->db->where("travel_type_title",$cname); 
		if($eid!="")	
		$this->db->where("travel_type_id !=", $eid);
        $query = $this->db->get('travel_types');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->travel_type_id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getTravelTypeList()
    {
		$this->db->select('travel_type_id as id,travel_type_title as title'); 
		$this->db->order_by('travel_type_title');
        $query = $this->db->get('travel_types');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delTravelType($dId)
{
$this->db->where('travel_type_id',$dId);
return $this->db->delete('travel_types');	
}

function updateTravelType($dbId,$newdata)
    {
	$this->db->where('travel_type_id', $dbId);
	$this->db->update('travel_types', $newdata); 
	}	
	
	
####Room type  section  ####

function addRoomType($data)
    {
        if($this->db->insert('hotel_room_types', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getRoomType($cname,$eid="")
    {
		$this->db->select('type_id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("type_id !=", $eid);
        $query = $this->db->get('hotel_room_types');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->type_id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getRoomTypesList()
    {
		$this->db->select('type_id as id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('hotel_room_types');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delRoomType($dId)
{
$this->db->where('type_id',$dId);
return $this->db->delete('hotel_room_types');	
}

function updateRoomType($dbId,$newdata)
    {
	$this->db->where('type_id', $dbId);
	$this->db->update('hotel_room_types', $newdata); 
	}	
	
	
####Room type section  ####	
	

####Meal plan section  ####

function addMealPlan($data)
    {
        if($this->db->insert('meal_plans', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getMealPlan($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('meal_plans');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getMealPlansList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('meal_plans');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delMealPlan($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('meal_plans');	
}

function updateMealPlan($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('meal_plans', $newdata); 
	}	
	
####Meal plan section  ####	


####Role section  ####

function addRole($data)
    {
        if($this->db->insert('role', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getRole($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('role');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getRolesList()
    {
		$this->db->select('id,title,access_level_id'); 
		$this->db->order_by('title');
        $query = $this->db->get('role');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delRole($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('role');	
}

function updateRole($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('role', $newdata); 
	}	
	
####Role section end  ####	



####Position section  ####

function addPosition($data)
    {
        if($this->db->insert('positions', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getPosition($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('positions');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getPositionsList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('positions');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delPosition($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('positions');	
}

function updatePosition($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('positions', $newdata); 
	}	
	
####Position section end  ####	
	
####star_rating section  ####

function addStarRating($data)
    {
        if($this->db->insert('star_ratings', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getStarRating($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('star_ratings');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getStarRatingList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('star_ratings');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delStarRating($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('star_ratings');	
}

function updateStarRating($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('star_ratings', $newdata); 
	}	
	
####star_rating section end  ####	
	
	
####dorak_rating section  ####

function addDorakRating($data)
    {
        if($this->db->insert('dorak_ratings', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getDorakRating($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('dorak_ratings');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getDorakRatingList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('dorak_ratings');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delDorakRating($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('dorak_ratings');	
}

function updateDorakRating($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('dorak_ratings', $newdata); 
	}	
	
####dorak_rating section end  ####	


####status section  ####

function addStatus($data)
    {
        if($this->db->insert('hotel_status', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getStatus($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('hotel_status');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getStatusList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('hotel_status');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delStatus($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('hotel_status');	
}

function updateStatus($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('hotel_status', $newdata); 
	}	
	
####status section end  ####		


####Department section  ####

function addDepartment($data)
    {
        if($this->db->insert('departments', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getDepartment($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('departments');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getDepartmentsList()
    {
		$this->db->select('id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('departments');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delDepartment($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('departments');	
}

function updateDepartment($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('departments', $newdata); 
	}	
	
####status section end  ####		
	
	
	
#### CancellationDuration section  ####

function addCancellationDuration($data)
    {
        if($this->db->insert('cancel_time_option', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getCancellationDuration($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("time_period",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('cancel_time_option');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getCancellationDurationList()
    {
		$this->db->select('id,time_period as title'); 
		$this->db->order_by('time_period');
        $query = $this->db->get('cancel_time_option');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delCancellationDuration($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('cancel_time_option');	
}

function updateCancellationDuration($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('cancel_time_option', $newdata); 
	}	
	
####CancellationDuration section end  ####	


#### Payment Duration section  ####

function addPaymentDuration($data)
    {
        if($this->db->insert('payment_options', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getPaymentDuration($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("option_title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('payment_options');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getPaymentDurationList()
    {
		$this->db->select('id,option_title as title'); 
		$this->db->order_by('option_title');
        $query = $this->db->get('payment_options');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delPaymentDuration($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('payment_options');	
}

function updatePaymentDuration($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('payment_options', $newdata); 
	}	
	
####Payment Duration section end  ####	

####Age group section end  ####	


function addAgeGroup($data)
    {
        if($this->db->insert('child_age_ranges', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
    
  function getAgeGroup($cname,$eid="")
    {
		$this->db->select('id');
		$this->db->where("age_range",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('child_age_ranges');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	

function getAgeGroupList()
    {
		$this->db->select('id,age_range as title'); 
		$this->db->order_by('age_range');
        $query = $this->db->get('child_age_ranges');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delAgeGroup($dId)
{
$this->db->where('id',$dId);
return $this->db->delete('child_age_ranges');	
}

function updateAgeGroup($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	$this->db->update('child_age_ranges', $newdata); 
	}	
	
####Payment Age group section end  ####	

####Company section   ####	


function addCompany($data)
    {
        if($this->db->insert('companies', $data))
        {
			return $this->db->insert_id();
		}
		return false;
    }
	
function addCompanyMarkets($data)
    {
       if($this->db->insert_batch('company_markets', $data))
       {
		return true;
	  }
	return false;
    }	
	
function getCompanyMarkets($cId)
    {
		$this->db->select('market_id');
		$this->db->where("company_id",$cId); 
        $query = $this->db->get('company_markets');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
    
  function getCompany($cname)
    {
		$this->db->select('id');
		$this->db->where("company_name",$cname); 
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
function getCompanyDetails($cid)
    {
		$this->db->select('id,company_name,address,country_code,city,head,company_logo,primary_company_id,status');
		$this->db->where("id",$cid); 
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row;
		}
		else{
			return false;
		}
		return false;
	}
	
function getCompanyLogo($cid)
    {
		$this->db->select('company_logo');
		$this->db->where("id",$cid); 
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->company_logo;
		}
		else{
			return false;
		}
		return false;
	}
	

function getCompanyList()
    {
		$this->db->select('id,company_name as title'); 
		$this->db->order_by('company_name');
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}

function delCompany($dId)
{
$this->delCompanyMarkets($dId);
$this->db->where('id',$dId);
return $this->db->delete('companies');	
}

function delCompanyMarkets($cmpId)
{
$this->db->where('company_id',$cmpId);
return $this->db->delete('company_markets');	
}

function updateCompany($dbId,$newdata)
    {
	$this->db->where('id', $dbId);
	return $this->db->update('companies', $newdata); 
	}	
	
####Company section end  ####	

}