<?php 
/**
  * @author tis
  * @author M
  * @description this controller  is for  settings hotel  related fields  information add, edit delete options to field
  */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends AdminController {
	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

private function _init()
	{
		$this->load->helper(array('form','html','form_field_values','acl'));
		$this->load->library('form_validation');	
		$this->output->set_meta('description','Dorak Hotel Settings');
		$this->output->set_meta('title','Dorak Hotel Settings');
		$this->output->set_title('Dorak Hotel Settings');
		$this->output->set_template('settings');
		$this->load->model('settings/setting_model');
		$this->tmpFileDir=$this->config->item('upload_temp_dir');
		
	}
public function index()
    {
       redirect('settings/chain');
    }
	
public function chain($action='list',$cid='')
    {
	switch ($action) {
    case 'del':
		if($cid!="")
		{
		  $chid=(int)$cid;
		 if($this->setting_model->delHotelChain($chid))
		 {
		  $sdata['message'] = 'Selected chain deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/chain');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$chain_id=trim($this->input->post('id'));
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getHotelChain($new_title,$chain_id))
		{
		if(!$this->setting_model->updateHotelChain($chain_id,$upData))
		{
        echo $new_title;
		}
		}
		else
		{
		echo 'exists';	
		}
		exit();
        break;
    case 'add':    
		$chname=removeExtraspace($this->input->post('chain_name'));
		if($chname!="")
		{
		//check if already in database
		if(!$this->setting_model->getHotelChain($chname))
		{
		$insData=array('title'=>$chname);
		
		if($this->setting_model->addHotelChain($insData))
		 {
		  $sdata['message'] = 'Chain '.$chname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Chain '.$chname.' not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Chain '.$chname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/chain');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/chain/add'), 
			'editAction' => site_url('settings/chain/editajax'), 
			'delAction' => site_url('settings/chain/del'), 			
            'chain_name'=>array('id' => 'chain_name', 														
								'name' => 'chain_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"3-100",													
								'placeholder'=>'Chain name',				
								'maxlength'     => '100',
								'size'          => '100',
								'value'=>$this->input->post('chain_name'))
					
			);			
		$data['hotel_chains']=$this->setting_model->getHotelChainList();		
        $this->load->view('chain_setting', $data);
    }
	
public function purpose($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delHotelPurpose($did))
		 {
		  $sdata['message'] = 'Selected purpose deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/purpose');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getHotelPurpose($new_title,$dbId))
		{
		if(!$this->setting_model->updateHotelPurpose($dbId,$upData))
		{
        echo $new_title;
		}
		}
		else
		{
		echo 'exists';	
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('purpose_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getHotelPurpose($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addHotelPurpose($insData))
		 {
		  $sdata['message'] = 'Purpose '. $newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Purpose '. $newname.' not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Purpose '.$newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/purpose');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/purpose/add'), 
			'editAction' => site_url('settings/purpose/editajax'), 
			'delAction' => site_url('settings/purpose/del'), 			
            'purpose_name'=>array('id' => 'purpose_name', 														
								'name' => 'purpose_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"3-100",													
								'placeholder'=>'Purpose',				
								'maxlength'     => '100',
								'size'          => '100',
								'value'=>$this->input->post('purpose_name'))
					
			);			
		$data['listData']=$this->setting_model->getHotelPurposeList();		
        $this->load->view('purpose_setting', $data);
    }
	
	
	## property type setting ####
	
	
public function property_type($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delHotelPropertyType($did))
		 {
		  $sdata['message'] = 'Selected property type deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/property_type');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getHotelPropertyType($new_title,$dbId))
		{
		if(!$this->setting_model->updateHotelPropertyType($dbId,$upData))
		{
        echo $new_title;
		}
		}
		else
		{
		echo 'exists';	
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('property_type_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getHotelPropertyType($newname))
		{
		$insData=array('title'=>$newname,'status'=>1);
		if($this->setting_model->addHotelPropertyType($insData))
		 {
		  $sdata['message'] = 'Property type '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Property type '.$newname.' not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Property type '.$newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/property_type');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/property_type/add'), 
			'editAction' => site_url('settings/property_type/editajax'), 
			'delAction' => site_url('settings/property_type/del'), 			
            'add_input_name'=>array('id' => 'property_type_name', 														
								'name' => 'property_type_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z&\-\s]*$",											
								'data-validation-length'=>"3-100",											
								'placeholder'=>'Property type',				
								'maxlength'     => '100',
								'size'          => '100',
								'value'=>$this->input->post('property_type_name'))
					
			);			
		$data['listData']=$this->setting_model->getHotelPropertyTypeList();		
        $this->load->view('property_type_setting', $data);
    }
	
## property type setting end ####
			
			
## facility  setting ####
	
	
	
public function facility($action='list',$did='')
    {
	switch ($action) {
		case 'del':
			if($did!="")
			{
			$did=(int)$did;
			if($this->setting_model->delRoomFacility($did))
			{
			$sdata['message'] = 'Selected facility deleted!';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
			}			
			}
			redirect('settings/facility');		
			break;
		case 'editajax':
			$new_title=removeExtraspace($this->input->post('newname'));
			$dbId=$this->input->post('id');
			$upData = array('service_name'=>$new_title);
			if($new_title!="" && !$this->setting_model->getRoomFacility($new_title,$dbId))
			{
			if(!$this->setting_model->updateRoomFacility($dbId,$upData))
			{
			echo $new_title;
			}
			}
			else
			{
			echo 'exists';	
			}
			exit();
        break;
    case 'add':    
			$newname=removeExtraspace($this->input->post('rfacility_name'));
			if($newname!="")
			{
			//check if already in database
			if(!$this->setting_model->getRoomFacility($newname))
			{
			$insData=array('service_name'=>$newname);
			if($this->setting_model->addRoomFacility($insData))
			 {
			  $sdata['message'] = 'Facility '.$newname.' added';
			  $flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
				$this->session->set_userdata($flashdata);
			 }
			else{
			$sdata['message'] = 'Facility '.$newname.'  not added!';
			  $flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
				$this->session->set_userdata($flashdata);
				}		 
			}		
			else{
				$sdata['message'] = 'Facility '.$newname.' already exists';
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
				$this->session->set_userdata($flashdata);
				}	
			}
			redirect('settings/facility');
			break;
			
			## hotel label ##
			
		case 'delhotelfacility':
			if($did!="")
			{
			$did=(int)$did;
			if($this->setting_model->delHotelFacility($did))
			{
			$sdata['message'] = 'Selected hotel level facility deleted!';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
			}			
			}
			redirect('settings/facility');		
			break;
		case 'edithotelfacilityajax':
			$new_title=removeExtraspace($this->input->post('newname'));
			$dbId=$this->input->post('id');
			$upData = array('facility_title'=>$new_title);
			if($new_title!="" && !$this->setting_model->getHotelFacility($new_title,$dbId))
			{
			if(!$this->setting_model->updateHotelFacility($dbId,$upData))
			{
			echo $new_title;
			}
			}
			else
			{
			echo 'exists';	
			}
			exit();
        break;
    case 'addhotelfacility':    
			$newname=removeExtraspace($this->input->post('hfacility_name'));
			if($newname!="")
			{
			//check if already in database
			if(!$this->setting_model->getHotelFacility($newname))
			{
			$insData=array('facility_title'=>$newname);
			if($this->setting_model->addHotelFacility($insData))
			 {
			  $sdata['message'] = 'Facility '.$newname.' added';
			  $flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);				
				$this->session->set_userdata($flashdata);
			 }
			else{
			$sdata['message'] = 'Facility '.$newname.'  not added!';
			  $flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
				$this->session->set_userdata($flashdata);
				}		 
			}		
			else{
				$sdata['message'] = 'Facility '.$newname.' already exists';
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);				
				$this->session->set_userdata($flashdata);
				}	
			}
			redirect('settings/facility');
			break;
			default:  
		}
        $data = array(
            'addAction' => site_url('settings/facility/add'), 
			'editAction' => site_url('settings/facility/editajax'), 
			'delAction' => site_url('settings/facility/del'), 			
            'add_input_name'=>array('id' => 'rfacility_name', 														
								'name' => 'rfacility_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min3",
								'placeholder'=>'Room Facility',				
								'maxlength'     => '100',
								'size'          => '100',
								'value'=>$this->input->post('rfacility_name')),
								
								
								
			'addHFAction' => site_url('settings/facility/addhotelfacility'), 
			'editHFAction' => site_url('settings/facility/edithotelfacilityajax'), 
			'delHFAction' => site_url('settings/facility/delhotelfacility'), 			
            'add_input_hfacility_name'=>array('id' => 'hfacility_name', 														
								'name' => 'hfacility_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min3",
								'placeholder'=>'Hotel Facility',				
								'maxlength'     => '100',
								'size'          => '100',
								'value'=>$this->input->post('hfacility_name'))
					
					
			);			
		$data['listData']=$this->setting_model->getRoomFacilityList();	
		$data['listHFData']=$this->setting_model->getHotelFacilityList();			
        $this->load->view('facility_setting', $data);
    }
	
## facility setting end ####
		
			
## curency  setting ####
	
public function currency($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delCurrency($did))
		 {
		  $sdata['message'] = 'Selected currency deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/currency');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$currency_usd_value=removeExtraspace($this->input->post('newusdval'));
		$dbId=$this->input->post('id');
		$upData = array('code'=>$new_title,'name'=>$new_title,'currency_usd_value'=>$currency_usd_value);
		if($new_title!="" && $currency_usd_value!="")
		{
		if(!$this->setting_model->getCurrency($new_title,$currency_usd_value,$dbId))
		{
		if(!$this->setting_model->updateCurrency($dbId,$upData))
		{
			$return=array('error'=>false,'curency'=>$new_title,'usdval'=>$currency_usd_value);
			echo  json_encode($return);
       // echo $new_title;
		}
		}
		else{
			$return=array('error'=>true,'curency'=>$new_title,'usdval'=>$currency_usd_value);
			echo  json_encode($return);
		}
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('currency_name'));
		$usd_value=removeExtraspace($this->input->post('currency_usd_val'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getCurrency($newname))
		{
		$insData=array('code'=>$newname,'name'=>$newname,'currency_usd_value'=>$usd_value);
		if($this->setting_model->addCurrency($insData))
		 {
		  $sdata['message'] = 'Currency '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Currency '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Currency '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/currency');
        break;
	default:  
	}
        $data = array(		
			'importAction' => site_url('settings/process_curency_rates'),
            'addAction' => site_url('settings/currency/add'), 
			'editAction' => site_url('settings/currency/editajax'), 
			'delAction' => site_url('settings/currency/del'), 			
            'add_input_name'=>array('id' => 'currency_name', 														
								'name' => 'currency_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",											
								'placeholder'=>'Currency',				
								'maxlength'     => '30',
								'size'          => '50',
								'value'=>$this->input->post('currency_name')),
								'add_input_value'=>array('id' => 'currency_usd_val', 														
								'name' => 'currency_usd_val',
								'class'=>'required',
								'data-validation'=>'required,price',
								'data-validation-allowing'=>'float',
								'placeholder'=>'Currency USD Value',				
								'maxlength'     => '10',
								'size'          => '50',
								'value'=>$this->input->post('currency_usd_val')),
							   'clear_old_data' => array(
								'name'        => 'clear_old_data',
								'id'          => 'clear_old_data',
								'value'       => 'yclear',
								'data-label'=>"Clear old data",
								'checked'     => False,
								
								)								
					
			);			
		$data['listData']=$this->setting_model->getCurrencyList();		
        $this->load->view('currency_setting', $data);
    }
	
## property type setting end ####		
		

   public function process_curency_rates()
    { 
           if ($this->form_validation->run($this) != FALSE) {	
            $sdata['message'] ='Please select excel file to import';
			$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
			$this->session->set_userdata($flashdata); 
            $this->currency();
        } else {
			$arr_data=array();
			
			$clear_old_data = $this->input->post('clear_old_data');
            // config upload
            $config['upload_path'] = $this->tmpFileDir;
            $config['allowed_types'] = 'xlsx|csv|xls|xls';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('curency_file')) {
				
				$sdata['message'] = $this->upload->display_errors();
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
				$this->session->set_userdata($flashdata); 
                 $this->currency();
            } else {
                $upload_data = $this->upload->data();
				$curency_file = $upload_data['full_path'];
				chmod($curency_file,0777);	
                

			$this->load->library('excel');
			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($curency_file);
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			//extract to a PHP readable array format
			$delArray=array();// prepare curency code to delete
			foreach ($cell_collection as $cell) {
				$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
				//header will/should be in row 1 only. of course this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				} 
				else {
					$arr_data[$row][$header[1][$column]] = $data_value;
				  array_push($delArray,$arr_data[$row][$header[1]['A']]);
				}
			}
			$dCurencyCode=array_unique($delArray);
			if(count($dCurencyCode)>0)
			{
				$this->setting_model->deleteCurencyCodesData($dCurencyCode);
			}
			if(in_array('code',$header[1]) && in_array('name',$header[1]) && in_array('currency_usd_value',$header[1]))
			{
			if($clear_old_data && $clear_old_data=='yclear')
			$clearPrevData=true;
			else
			$clearPrevData=false;
			$isimportedData=false;
			if($arr_data && count($arr_data)>0)
				{				
				if($clearPrevData)
				{
				// empty the datble data and add new data
				if($this->setting_model->importCurencyData($arr_data))
					{
						$isimportedData=true;
					}
				}
				else{
					// add new data to the  table with old data
					if($this->setting_model->addCurencyData($arr_data))
					{
						$isimportedData=true;
					}
				 }
				if($isimportedData)
				{
				$sdata['message'] = 'Currency details imported!';
				$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'sucess'
							);	
				$this->session->set_userdata($flashdata);				
				if(file_exists($curency_file))
						{
						unlink($curency_file);	
						}			
				}
				else{
					$sdata['message'] = 'Currency details not imported!';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);	
					$this->session->set_userdata($flashdata); 
					if(file_exists($curency_file))
						{
						unlink($curency_file);	
						}
					
				}
				}
				}
				else{
					$sdata['message'] = 'The file is not in proper format!Please check the sample file';
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'error'
							);	
					$this->session->set_userdata($flashdata); 					
				}
				if(file_exists($curency_file))
						{
						unlink($curency_file);	
						}
				}
			}
			redirect('settings/currency');
}

## country  cities setting  ####
	
public function cities($cuntryId='',$action='list',$cid='')
{
	if($cuntryId!="")
	{
	switch ($action) {
    case 'del':
		if($cid!="")
		{
		 $did=(int)$cid;
		 // check if  cities have districts
		if(!$this->setting_model->getCityDistricts($did))
		{
		 if($this->setting_model->delCity($did))
		 {
		  $sdata['message'] = 'Selected city deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }	
		}
		else{
			 $sdata['message'] = 'Selected city not deleted! Please delete its districts first';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			
		}		
		}
		redirect('settings/cities/'.$cuntryId);		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('cityname'));
		$city_code=removeExtraspace($this->input->post('citycode'));
		$dbId=$this->input->post('ctyid');
		$upData = array('city_name'=>$new_title,'city_code'=>$city_code);
		if($new_title!="" || $city_code!="")
		{
		if($this->setting_model->checkCityExists($new_title,$dbId,$cuntryId))
		{
		$return=array('error'=>true,'city_name'=>$new_title,'city_code'=>$city_code,'msg'=>'City Name Already Exists');
		echo  json_encode($return);
		exit();
		}
		if($city_code!="" && $this->setting_model->checkCityCodeExists($city_code,$dbId,$cuntryId))
		{	
			$return=array('error'=>true,'city_name'=>$new_title,'city_code'=>$city_code,'msg'=>'City Code Already Exists');
			echo  json_encode($return);
			exit();
		}
		if(!$this->setting_model->updateCity($dbId,$upData))
			{
				$return=array('error'=>false,'city_name'=>$new_title,'city_code'=>$city_code);
				echo  json_encode($return);
      
			}
		
		}		
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('city_name'));
		$city_code=removeExtraspace($this->input->post('city_code'));
		$country_code=$this->setting_model->getCountryCode($cuntryId);
		if($newname!="")
		{
		//check if already in database		
		$isCityExists=false;
		if($this->setting_model->checkCityExists($newname,'',$cuntryId))
		{
			$isCityExists=true;
			$sdata['message'] = "City name ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);			
		}	
		
		if($this->setting_model->checkCityCodeExists($city_code,'',$cuntryId))
		{
			$isCityExists=true;
			$sdata['message'] = "City code ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
			
		}	
	if(!$isCityExists)
		{
		$insData=array('country_id'=>$cuntryId,'country_code'=>$country_code,'city_name'=>$newname,'city_code'=>$city_code);
		if($this->setting_model->addCity($insData))
		 {
		  $sdata['message'] = "City ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "City ".$newname.'  not ndded!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
			
		}
		redirect('settings/cities/'.$cuntryId);	
        break;
	default:  
	}
	$country_name=$this->setting_model->getCountryName($cuntryId);
        $data = array(	
			'country_name' => $country_name,		
            'addAction' => site_url('settings/cities/'.$cuntryId.'/add'), 
			'editAction' => site_url('settings/cities/'.$cuntryId.'/editajax'), 
			'delAction' => site_url('settings/cities/'.$cuntryId.'/del'),
			'addDistrictUrl' => site_url('settings/district/'.$cuntryId), 			
            'add_input_name'=>array('id' => 'city_name', 														
								'name' => 'city_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min3",											
								'placeholder'=>'City Name',				
								'maxlength'     => '30',
								'size'          => '50',
								'value'=>$this->input->post('city_name')),
			'add_input_fld2'=>array('id' => 'city_code', 														
								'name' => 'city_code',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",
								'placeholder'=>'City Code',				
								'maxlength'     => '8',
								'size'          => '10',
								'value'=>$this->input->post('city_code')),			
																
					
			);			
		$data['listData']=$this->setting_model->getCitiesList($cuntryId);		
        $this->load->view('country_cities_setting', $data);
	}
	else{
		redirect('settings/country');	
	}
	
}
## country  cities setting end ####


##   cities  district setting  ####
	
public function district($cuntryId='',$cityId='',$action='list',$did='')
{
	if($cuntryId!="" && $cityId!="")
	{
	$city_name=$this->setting_model->getCityName($cityId);
	switch ($action) {
    case 'del':
		if($did!="")
		{
		 $cdid=(int)$did;
		 if($this->setting_model->delDistrict($cdid))
		 {
		  $sdata['message'] = 'Selected district deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/district/'.$cuntryId.'/'.$cityId);		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('district_name'));
		$dbId=$this->input->post('district_id');
		$upData = array('district_name'=>$new_title);
		if($new_title!="")
		{
		if($this->setting_model->checkDistrictExists($new_title,$cuntryId,$cityId,$dbId))
		{
		$return=array('error'=>true,'district_name'=>$new_title,'msg'=>'District Name Already Exists');
		echo  json_encode($return);
		exit();
		}

		if(!$this->setting_model->updateDistrict($dbId,$upData))
			{
				$return=array('error'=>false,'district_name'=>$new_title);
				echo  json_encode($return);      
			}
		
		}		
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('district_name'));
		if($newname!="")
		{
		//check if already in database
		
		$isDistrictExists=false;
		if($this->setting_model->checkDistrictExists($newname,$cuntryId,$cityId,''))
		{
			$isDistrictExists=true;
			$sdata['message'] = "District name ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
			
		}	
		
		
	if(!$isDistrictExists)
		{
		$insData=array('country_id'=>$cuntryId,'city_id'=>$cityId,'city_name'=>$city_name,'district_name'=>$newname);
		if($this->setting_model->addDistrict($insData))
		 {
		  $sdata['message'] = "District ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "District ".$newname.'  not ndded!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
			
		}
		redirect('settings/district/'.$cuntryId.'/'.$cityId);		
        break;
	default:  
	}
        $data = array(	
			'city_name' => $city_name,	
			'country_id' =>$cuntryId,			
            'addAction' => site_url('settings/district/'.$cuntryId.'/'.$cityId.'/add'), 
			'editAction' => site_url('settings/district/'.$cuntryId.'/'.$cityId.'/editajax'), 
			'delAction' => site_url('settings/district/'.$cuntryId.'/'.$cityId.'/del'),
			
            'add_input_name'=>array('id' => 'district_name', 														
								'name' => 'district_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min3",											
								'placeholder'=>'District Name',				
								'maxlength'     => '50',
								'size'          => '50',
								'value'=>$this->input->post('district_name')),
				);
		$data['listData']=$this->setting_model->getDistrictList($cuntryId,$cityId);		
        $this->load->view('city_district_setting', $data);
	}
	else{
		redirect('settings/country');	
	}
	
}
##   cities district end ####

## country  setting ####
	
public function country($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		  #check if country having  cities , if not then delete
	 if(!$this->setting_model->getCitiesList($did))
		{
		 if($this->setting_model->delCountry($did))
		 {
		  $sdata['message'] = 'Selected country deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }	
		}
		else{
			 $sdata['message'] = 'Selected country could not deleted! Please delete its cities first';
			 $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
		}		
		}
		redirect('settings/country');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$country_code=removeExtraspace($this->input->post('code'));
		$dbId=$this->input->post('id');
		$upData = array('country_name'=>$new_title,'country_code'=>$country_code);
		if($new_title!="" && $country_code!="")
		{
		if(!$this->setting_model->checkCountryExists($dbId,$new_title))
		{
			if(!$this->setting_model->checkCountryCodeExists($dbId,$country_code))
			{	
				if(!$this->setting_model->updateCountry($dbId,$upData))
				{
					$return=array('error'=>false,'country'=>$new_title,'country_code'=>$country_code);
					echo  json_encode($return);
      
				}
		
			}
			else
			{
				$return=array('error'=>true,'country'=>$new_title,'country_code'=>$country_code,'msg'=>'Country Code Already Exists');
				echo  json_encode($return);
			}
		}
		else
		{
			$return=array('error'=>true,'country'=>$new_title,'country_code'=>$country_code,'msg'=>'Country Name Already Exists');
		   echo  json_encode($return);
		}
		}
		
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('country_name'));
		$cuntry_code=removeExtraspace($this->input->post('country_code'));
		if($newname!="")
		{
		//check if already in database
		$isexists=false;
		if($this->setting_model->checkCountryExists('',$newname))
		{
			$isexists=true;
			$sdata['message'] = "Country name ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
			
		}	
		
		if($this->setting_model->checkCountryCodeExists('',$cuntry_code))
		{
			$isexists=true;
			$sdata['message'] = "Country Code ". $cuntry_code.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
			
		}	
		if(!$isexists)
		{
		$insData=array('country_name'=>$newname,'country_code'=>$cuntry_code);
		if($this->setting_model->addCountry($insData))
		 {
		  $sdata['message'] = "Country ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "Country ".$newname.'  not ndded!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		}
		redirect('settings/country');
        break;
	default:  
	}
        $data = array(		
            'addAction' => site_url('settings/country/add'), 
			'editAction' => site_url('settings/country/editajax'), 
			'delAction' => site_url('settings/country/del'), 
			'addCitiesUrl' => site_url('settings/cities'),			
            'add_input_name'=>array('id' => 'country_name', 														
								'name' => 'country_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",											
								'placeholder'=>'Country Name',				
								'maxlength'     => '30',
								'size'          => '50',
								'value'=>$this->input->post('country_name')),
			'add_input_fld2'=>array('id' => 'country_code', 														
								'name' => 'country_code',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",
								'placeholder'=>'Country Code',				
								'maxlength'     => '8',
								'size'          => '10',
								'value'=>$this->input->post('country_code')),			
																
					
			);			
		$data['listData']=$this->setting_model->getCountriesList();		
        $this->load->view('country_setting', $data);
    }
	
## Market  setting ####
	
	
public function market($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delMarket($did))
		 {
		  $sdata['message'] = 'Selected market deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/market');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getMarket($new_title,$dbId))
		{
		if(!$this->setting_model->updateMarket($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Market : '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('market_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getMarket($newname))
		{
		$insData=array('title'=>$newname,'status'=>1);
		if($this->setting_model->addMarket($insData))
		 {
		  $sdata['message'] = "Market ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "Market ".$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = "Market: ".$newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/market');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/market/add'), 
			'editAction' => site_url('settings/market/editajax'), 
			'delAction' => site_url('settings/market/del'), 			
            'add_input_name'=>array('id' => 'market_name', 														
								'name' => 'market_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s .,;]*$",											
								'data-validation-length'=>"min2",																			
								'maxlength'     => '200',				
								'placeholder'=>'Market',
								'value'=>$this->input->post('market_name'))
					
			);			
		$data['listData']=$this->setting_model->getMarketList();		
        $this->load->view('market_setting', $data);
    }
	
## market setting end ####
	
## Travel  type  setting ####
	
	
public function travel_type($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delTravelType($did))
		 {
		  $sdata['message'] = 'Selected travel type deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/travel_type');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('travel_type_title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getTravelType($new_title,$dbId))
		{
		if(!$this->setting_model->updateTravelType($dbId,$upData))
		{
        echo $new_title;
		}
		}
		else
		{
		echo 'exists';	
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('travel_type_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getTravelType($newname))
		{
		$insData=array('travel_type_title'=>$newname,'status'=>1);
		if($this->setting_model->addTravelType($insData))
		 {
		  $sdata['message'] = 'Travel type '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = $newname.' travel type not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/travel_type');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/travel_type/add'), 
			'editAction' => site_url('settings/travel_type/editajax'), 
			'delAction' => site_url('settings/travel_type/del'), 			
            'add_input_name'=>array('id' => 'travel_type_name', 														
								'name' => 'travel_type_name',
								'class'=>'required',
								'data-validation'=>'required,alphanumeric,length',																			
								'data-validation-allowing'=>'-_ ',
								'data-validation-length'=>"min3",	
								'maxlength'     => '50',								
								'placeholder'=>'Travel Type',
								'value'=>$this->input->post('travel_type_name'))
					
			);			
		$data['listData']=$this->setting_model->getTravelTypeList();		
        $this->load->view('travel_type_setting', $data);
    }
	
## travel type end ####
	

## room type  setting ####
	
	
public function room_type($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delRoomType($did))
		 {
		  $sdata['message'] = 'Selected room type deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/room_type');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getRoomType($new_title,$dbId))
		{
		if(!$this->setting_model->updateRoomType($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Room type : '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('room_type_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getRoomType($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addRoomType($insData))
		 {
		  $sdata['message'] = 'Room type '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Room type '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Room type '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/room_type');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/room_type/add'), 
			'editAction' => site_url('settings/room_type/editajax'), 
			'delAction' => site_url('settings/room_type/del'), 			
            'add_input_name'=>array('id' => 'room_type_name', 														
								'name' => 'room_type_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s ]*$",											
								'data-validation-length'=>"min2",									
								'maxlength'     => '50',				
								'placeholder'=>'Room Type',
								'value'=>$this->input->post('room_type_name'))
					
			);			
		$data['listData']=$this->setting_model->getRoomTypesList();		
        $this->load->view('room_type_setting', $data);
    }
	
## room type end ####	
	
	
## meal plan setting ####
	
	
public function meal_plan($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delMealPlan($did))
		 {
		  $sdata['message'] = 'Selected meal plan deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/meal_plan');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getMealPlan($new_title,$dbId))
		{
		if(!$this->setting_model->updateMealPlan($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Meal plan: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('meal_plan_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getMealPlan($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addMealPlan($insData))
		 {
		  $sdata['message'] = 'Meal plan '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Meal plan '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Meal plan '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/meal_plan');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/meal_plan/add'), 
			'editAction' => site_url('settings/meal_plan/editajax'), 
			'delAction' => site_url('settings/meal_plan/del'), 			
            'add_input_name'=>array('id' => 'meal_plan_name', 														
								'name' => 'meal_plan_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s ]*$",											
								'data-validation-length'=>"min2",
								'maxlength'     => '10',								
								'placeholder'=>'Meal Plan',
								'value'=>$this->input->post('meal_plan_name'))
					
			);			
		$data['listData']=$this->setting_model->getMealPlansList();		
        $this->load->view('meal_plan_setting', $data);
    }
	
## meal plan end ####		

## role setting ####
	
	
public function role($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delRole($did))
		 {
		  $sdata['message'] = 'Selected role deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/role');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$access_level_id=removeExtraspace($this->input->post('access_id'));		
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title,'access_level_id'=>$access_level_id);
		if($new_title!="" && !$this->setting_model->getRole($new_title,$dbId))
		{
		if(!$this->setting_model->updateRole($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title,'level_id'=>getAccessLevelTitle($access_level_id));
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Role: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('role_name'));
		$access_level_id=removeExtraspace($this->input->post('access_level_id'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getRole($newname))
		{
		$insData=array('title'=>$newname,'access_level_id'=>$access_level_id);
		if($this->setting_model->addRole($insData))
		 {
		  $sdata['message'] = 'Role '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Role '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Role '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/role');
        break;
	default:  
	}
	$accessLevels=getAccessLevelOptions(); /* using helper*/
        $data = array(
            'addAction' => site_url('settings/role/add'), 
			'editAction' => site_url('settings/role/editajax'), 
			'delAction' => site_url('settings/role/del'), 			
            'add_input_name'=>array('id' => 'role_name', 														
								'name' => 'role_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s ]*$",											
								'data-validation-length'=>"min2",
								'maxlength'     => '50',								
								'placeholder'=>'Role',
								'value'=>$this->input->post('role_name')),
				'access_levels'=>	$accessLevels
					
			);			
		$data['listData']=$this->setting_model->getRolesList();		
        $this->load->view('role_setting', $data);
    }
	
## role section end ####	


## position setting ####
	
	
public function position($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delPosition($did))
		 {
		  $sdata['message'] = 'Selected position deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/position');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getPosition($new_title,$dbId))
		{
		if(!$this->setting_model->updatePosition($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Position: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('position_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getPosition($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addPosition($insData))
		 {
		  $sdata['message'] = 'Position '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Position '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Position '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/position');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/position/add'), 
			'editAction' => site_url('settings/position/editajax'), 
			'delAction' => site_url('settings/position/del'), 			
            'add_input_name'=>array('id' => 'position_name', 														
								'name' => 'position_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s ]*$",											
								'data-validation-length'=>"min2",
								'maxlength'     => '50',								
								'placeholder'=>'Position',
								'value'=>$this->input->post('position_name'))
					
			);			
		$data['listData']=$this->setting_model->getPositionsList();		
        $this->load->view('position_setting', $data);
    }
	
## position section end ####		
	
	
## star rating setting ####
	
	
public function star_rating($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delStarRating($did))
		 {
		  $sdata['message'] = 'Selected star rating deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/star_rating');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getStarRating($new_title,$dbId))
		{
		if(!$this->setting_model->updateStarRating($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Star rating: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('star_rating_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getStarRating($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addStarRating($insData))
		 {
		  $sdata['message'] = 'Star rating '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Star rating '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Star rating '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/star_rating');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/star_rating/add'), 
			'editAction' => site_url('settings/star_rating/editajax'), 
			'delAction' => site_url('settings/star_rating/del'), 			
            'add_input_name'=>array('id' => 'star_rating_name', 														
								'name' => 'star_rating_name',									
								'data-validation'=>'required,number,length',
								'data-validation-allowing'=>"range[1;7]",							
								'data-validation-length'=>"min1",
								'data-validation-error-msg'=>"Star Rating 1 to 7",								
								'maxlength'     => '3',								
								'placeholder'=>'Star Rating',
								'value'=>$this->input->post('star_rating_name'))
					
			);			
		$data['listData']=$this->setting_model->getStarRatingList();		
        $this->load->view('star_rating_setting', $data);
    }
	
## star rating section end ####	

## Dorak rating section ####	

public function dorak_rating($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delDorakRating($did))
		 {
		  $sdata['message'] = 'Selected dorak rating deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/dorak_rating');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getDorakRating($new_title,$dbId))
		{
		if(!$this->setting_model->updateDorakRating($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Dorak rating: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('dorak_rating_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getDorakRating($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addDorakRating($insData))
		 {
		  $sdata['message'] = 'Dorak rating '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Dorak rating '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Dorak rating '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/dorak_rating');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/dorak_rating/add'), 
			'editAction' => site_url('settings/dorak_rating/editajax'), 
			'delAction' => site_url('settings/dorak_rating/del'), 			
            'add_input_name'=>array('id' => 'dorak_rating_name', 														
								'name' => 'dorak_rating_name',
								'data-validation'=>'required,number,length',
								'data-validation-allowing'=>"range[1;7]",							
								'data-validation-length'=>"min1",
								'data-validation-error-msg'=>"Dorak Rating 1 to 7",							
								'maxlength'     => '3',															
								'placeholder'=>'Dorak Rating',
								'value'=>$this->input->post('dorak_rating_name'))
					
			);			
		$data['listData']=$this->setting_model->getDorakRatingList();		
        $this->load->view('dorak_rating_setting', $data);
    }
	
## Dorak rating section end ####	


## status setting ####
	
	
public function status($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delStatus($did))
		 {
		  $sdata['message'] = 'Selected status deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/status');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getStatus($new_title,$dbId))
		{
		if(!$this->setting_model->updateStatus($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Status: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('status_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getStatus($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addStatus($insData))
		 {
		  $sdata['message'] = 'Status '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Status '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Status '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/status');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/status/add'), 
			'editAction' => site_url('settings/status/editajax'), 
			'delAction' => site_url('settings/status/del'), 			
            'add_input_name'=>array('id' => 'status_name', 														
								'name' => 'status_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s ]*$",											
								'data-validation-length'=>"min3",
								'maxlength'     => '50',								
								'placeholder'=>'Status',
								'value'=>$this->input->post('status_name'))
					
			);			
		$data['listData']=$this->setting_model->getStatusList();		
        $this->load->view('status_setting', $data);
    }
	
## status section end ####	

## Department setting ####
	
	
public function departments($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delDepartment($did))
		 {
		  $sdata['message'] = 'Selected department deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/departments');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getDepartment($new_title,$dbId))
		{
		if(!$this->setting_model->updateDepartment($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Department: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('department_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getDepartment($newname))
		{
		$insData=array('title'=>$newname);
		if($this->setting_model->addDepartment($insData))
		 {
		  $sdata['message'] = 'Department '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Department '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Department '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/departments');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/departments/add'), 
			'editAction' => site_url('settings/departments/editajax'), 
			'delAction' => site_url('settings/departments/del'), 			
            'add_input_name'=>array('id' => 'department_name', 														
								'name' => 'department_name',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s ]*$",											
								'data-validation-length'=>"min2",
								'maxlength'     => '50',								
								'placeholder'=>'Department',
								'value'=>$this->input->post('department_name'))
					
			);			
		$data['listData']=$this->setting_model->getDepartmentsList();		
        $this->load->view('departments_setting', $data);
    }
	
## Department section end ####	

## cancellation_duration setting ####
	
	
public function cancellation_duration($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delCancellationDuration($did))
		 {
		  $sdata['message'] = 'Selected duration deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/cancellation_duration');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('time_period'=>$new_title);
		if($new_title!="" && !$this->setting_model->getCancellationDuration($new_title,$dbId))
		{
		if(!$this->setting_model->updateCancellationDuration($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Duration: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('duration_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getCancellationDuration($newname))
		{
		$insData=array('time_period'=>$newname);
		if($this->setting_model->addCancellationDuration($insData))
		 {
		  $sdata['message'] = 'Duration '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Duration '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Duration '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/cancellation_duration');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/cancellation_duration/add'), 
			'editAction' => site_url('settings/cancellation_duration/editajax'), 
			'delAction' => site_url('settings/cancellation_duration/del'), 			
            'add_input_name'=>array('id' => 'duration_name', 														
								'name' => 'duration_name',
								'data-validation'=>'required,alphanumeric,length',
								'data-validation-allowing'=>"- ><.",
								'data-validation-length'=>"min2",
								'maxlength'     => '50',
								'placeholder'=>'Duration',
								'value'=>$this->input->post('duration_name'))
					
			);			
		$data['listData']=$this->setting_model->getCancellationDurationList();		
        $this->load->view('cancellation_duration_setting', $data);
    }
	
## cancellation duration section end ####	


## cancellation_duration setting ####
	
	
public function payment_duration($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delPaymentDuration($did))
		 {
		  $sdata['message'] = 'Selected duration deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/payment_duration');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('option_title'=>$new_title);
		if($new_title!="" && !$this->setting_model->getPaymentDuration($new_title,$dbId))
		{
		if(!$this->setting_model->updatePaymentDuration($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Duration: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('duration_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getPaymentDuration($newname))
		{
		$insData=array('option_title'=>$newname);
		if($this->setting_model->addPaymentDuration($insData))
		 {
		  $sdata['message'] = 'Duration '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Duration '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Duration '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/payment_duration');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/payment_duration/add'), 
			'editAction' => site_url('settings/payment_duration/editajax'), 
			'delAction' => site_url('settings/payment_duration/del'), 			
            'add_input_name'=>array('id' => 'duration_name', 														
								'name' => 'duration_name',
								'data-validation'=>'required,alphanumeric,length',
								'data-validation-allowing'=>"- ><.",
								'data-validation-length'=>"min2",
								'maxlength'     => '50',								
								'placeholder'=>'Duration',
								'value'=>$this->input->post('duration_name'))
					
			);			
		$data['listData']=$this->setting_model->getPaymentDurationList();		
        $this->load->view('payment_duration_setting', $data);
    }
	
	
	
## cancellation_duration setting end ####
	
## age group setting end ####

public function age_group($action='list',$did='')
    {
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		 if($this->setting_model->delAgeGroup($did))
		 {
		  $sdata['message'] = 'Age group duration deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/age_group');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$dbId=$this->input->post('id');
		$upData = array('age_range'=>$new_title);
		if($new_title!="" && !$this->setting_model->getAgeGroup($new_title,$dbId))
		{
		if(!$this->setting_model->updateAgeGroup($dbId,$upData))
		{
       	$return=array('error'=>false,'newname'=>$new_title);
		echo  json_encode($return);
      
		}
		}
		else
		{
			$return=array('error'=>'Age group: '.$new_title.' already exists','newname'=>$new_title);
			echo  json_encode($return);
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('age_group_name'));
		if($newname!="")
		{
		//check if already in database
		if(!$this->setting_model->getAgeGroup($newname))
		{
		$insData=array('age_range'=>$newname);
		if($this->setting_model->addAgeGroup($insData))
		 {
		  $sdata['message'] = 'Age group '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = 'Age group '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Age group '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}	
		}
		redirect('settings/age_group');
        break;
	default:  
	}
        $data = array(
            'addAction' => site_url('settings/age_group/add'), 
			'editAction' => site_url('settings/age_group/editajax'), 
			'delAction' => site_url('settings/age_group/del'), 			
            'add_input_name'=>array('id' => 'age_group_name', 														
								'name' => 'age_group_name',
								'data-validation'=>'required,alphanumeric,length',
								'data-validation-allowing'=>"- ><. +",
								'data-validation-length'=>"min2",
								'maxlength'     => '50',								
								'placeholder'=>'Age group',
								'value'=>$this->input->post('age_group_name'))
					
			);			
		$data['listData']=$this->setting_model->getAgeGroupList();		
        $this->load->view('age_group_setting', $data);
    }	
	
	## age_group_setting section  end ####
	
## Company/sub company section  ####	
public function edit_company($cid='')
    {
	$this->load->library('upload');
	$this->load->helper('path');
	$this->logoFileDir=$this->config->item('company_logo_dir');	
	$getcmpMarkets=$this->setting_model->getCompanyMarkets($cid);
	$cmpMarkets=array();
	if($getcmpMarkets)
	{
	foreach($getcmpMarkets as $marketID)
	{
	$cmpMarkets[$marketID->market_id]=$marketID->market_id;	
	}
	}
	
	if($cid!="")
	{	
	if (!empty($_POST))
		{	
		$sdata=array('message'=>'');
		# validation rules 
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('company_name', 'Company name', 'required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules('company_address', 'Address', 'required|min_length[10]|max_length[150]');
		$this->form_validation->set_rules('city', 'City', 'required');	
		$this->form_validation->set_rules('country_code', 'Country', 'required');				
		if ($this->form_validation->run($this) != FALSE) {
		$logo_name='';			
		if (isset($_FILES['logo_file']) && is_uploaded_file($_FILES['logo_file']['tmp_name'])) 
			{ 
				//get old logo and delete that one
				
					if($lgfile=$this->setting_model->getCompanyLogo($cid))
					{
					  if($lgfile!="")
					  {
						$logofile= set_realpath($this->logoFileDir).$lgfile;
						if(file_exists($logofile))
						{
						unlink($logofile);	
						}
					  }
					}
				// upload new logo
				
					$config['upload_path'] = $this->logoFileDir;
					$config['allowed_types'] = 'gif|jpg|jpeg|png|svg';
					$config['max_size']	= '0';//2 mb , 0 for unlimited
					$config['max_width']  = '0';// unlimited
					$config['max_height']  = '0';
					$this->upload->initialize($config);
						if ( ! $this->upload->do_upload('logo_file'))
						{
							$error =  $this->upload->display_errors();
							$sdata['message'] .= ', Notice.logo file not uploaded because '.$error.' , please upload it again!';

						}
						else
						{
							$uploadedFileDetail =  $this->upload->data();
							$logo_name=$uploadedFileDetail['file_name'];
							
						}
				}
		if($this->input->post('primary_company_id')!="" && $this->input->post('primary_company_id') > 0)
			{			
			$isPcompany=0;
			}
			else{
				$isPcompany=1;
				}
		$upData = array(				
				'company_name' => $this->input->post('company_name'),
				'address' => $this->input->post('company_address'),				
				'city' => $this->input->post('city'),
				'country_code' => $this->input->post('country_code'),
				'head' => $this->input->post('company_head'),
				//'market_id' => $this->input->post('market_id'),				
				'is_primary_company' => $isPcompany,
				'primary_company_id' => $this->input->post('primary_company_id'),
				'status' => $this->input->post('company_status')				
				);
		if($logo_name!="")
		$upData['company_logo'] = $logo_name;
			
		if($this->setting_model->updateCompany($cid,$upData))
		{
			/* update Company Markets start*/
			// check  if  markets are  changed then  update
			$companyMarkets = $this->input->post('market_id');
			$resultMatch=array_intersect($cmpMarkets,$companyMarkets); // return the matched values
			if(count($cmpMarkets) != count($resultMatch) || count($cmpMarkets)<1)
			{
			// delete old  markets 
			$this->setting_model->delCompanyMarkets($cid);		
			if($companyMarkets!='' && count($companyMarkets)>0)		
					{										
					$addCompanyMarketsData=array();							
						foreach($companyMarkets as $companyMarket)
						{
						 if($companyMarket!='')
							{				
								$addCompanyMarketsData[] = array(
											'company_id' => $cid,
											'market_id' => $companyMarket
										);
								}		
						}
				if(count($addCompanyMarketsData)>0)
					{				
						$this->setting_model->addCompanyMarkets($addCompanyMarketsData);
					}
				}	
			}					
			/* update Company Markets end*/	
			
		  $sdata['message'] .= 'Company: '.$this->input->post('company_name').' details updated';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
			$sdata['message'] = 'Company:'.$this->input->post('company_name').' Detail not updated!';
				$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}
			
		redirect('settings/company');	
		}
		}
	
	$countriesOptions=getCountriesOptions(); /* using helper*/
	$primaryCompanies=getPrimaryCompanies();
	$primaryCompaniesOptions=array('0'=>'None');
	if($primaryCompanies && count($primaryCompanies)>0)									
		{			
		foreach($primaryCompanies as $primaryCompany)
		{
			$primaryCompaniesOptions[$primaryCompany->id]=$primaryCompany->company_name;
		}
		}
		
	$dc=$this->config->item('default_country_code');
	
	$pcdoce=$this->input->post('country_code');
	if($pcdoce=="")
	{
	$pcdoce=$dc;	
	}
	$citiesOptions =getCountyCitiesOptions($pcdoce);	/* using helper*/
	$marketsList=getMarkets();
	$marketOptions=array();
	if($marketsList && count($marketsList)>0)									
		{			
		foreach($marketsList as $market)
		{
			$marketOptions[$market->id]=$market->title;
		}
		}		
	$positions=getPositions();
	$positionOptions =array(''  => 'Select');	
	if($positions && count($positions)>0)
	{
	foreach($positions as $position)
	{
		$positionOptions[$position->title]=$position->title;
	}
	}
	$comp_det=$this->setting_model->getCompanyDetails($cid);	
        $data = array(            
			'logo_directory' => $this->logoFileDir, 
			'editAction' => site_url('settings/edit_company/'.$cid), 
						
            'edit_company_name'=>array('id' => 'company_name', 														
								'name' => 'company_name',
								'autofocus'=>'autofocus',
								'tabindex'=>1,
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",						
								'data-validation-length'=>"min3",
								'maxlength'     => '100',								
								'placeholder'=>'Name',
								'value'=>$comp_det->company_name),
			'edit_company_address'=>array('id' => 'company_address', 
												'tabindex'=>4,
												'data-validation'=>'required,alphanumeric,length',	
												'data-validation-allowing'=>"'\'-_@#:,./ ()\n\r",												
												'data-validation-length'=>"15-150",													
												'name' => 'company_address',												
												'cols'     => '43',
												'tabindex'=>4,
												'rows'          => '2'),					
								'country_id_options' => $countriesOptions,				
							    'city_options' => $citiesOptions,
								'markets_options'=>$marketOptions,	
			'edit_company_head'=>array('id' => 'company_head', 														
								'name' => 'company_head',
								'tabindex'=>7,
								'data-validation'=>'custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",
								'data-validation-optional'=>'true',								
								'data-validation-length'=>"min3",
								'maxlength'     => '50',								
								'placeholder'=>'Head',
								'value'=>$comp_det->head),	
					'statusOptions'=>array(
								'1'=>'Enable',
								'0'=>'Disable',				
								),	
				'position_options' => $positionOptions,
				'primary_companies_options' =>$primaryCompaniesOptions,				
				'companyDetail'=>$comp_det,
				'companyMarketIds'=>$cmpMarkets,
   
			);	
			
		 $this->load->view('company_edit_setting', $data);
			
		}
	else{
			redirect('settings/company');	
		}
		
	}


public function company($action='list',$did='')
    {		
	$this->load->library('upload');
	$this->load->helper('path');
	$this->logoFileDir=$this->config->item('company_logo_dir');		
					
	switch ($action) {
    case 'del':
		if($did!="")
		{
		  $did=(int)$did;
		  ##check if company have logi uploaded  then delete that file
		  
		  if($lgfile=$this->setting_model->getCompanyLogo($did))
		  {
			  if($lgfile!="")
			  {
				$logofile= set_realpath($this->logoFileDir).$lgfile;
				if(file_exists($logofile))
				{
				unlink($logofile);	
				}
			  }
		  }
		 if($this->setting_model->delCompany($did))
		 {
		  $sdata['message'] = 'Selected Company deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }			
		}
		redirect('settings/company');		
        break;
 
    case 'add': 	
		if (!empty($_POST))
		{	
		$sdata=array('message'=>'');
		# validation rules 
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('company_name', 'Company name', 'required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules('company_address', 'Address', 'required|min_length[10]|max_length[150]');
		$this->form_validation->set_rules('city', 'City', 'required');	
		$this->form_validation->set_rules('country_code', 'Country', 'required');				
		if ($this->form_validation->run($this) != FALSE) {
		$newname=removeExtraspace($this->input->post('company_name'));
		//check if already in database
		if(!$this->setting_model->getCompany($newname))
		{	
		$logo_name='';			
		if (isset($_FILES['logo_file']) && is_uploaded_file($_FILES['logo_file']['tmp_name'])) 
					{ 
					$config['upload_path'] = $this->logoFileDir;
					$config['allowed_types'] = 'gif|jpg|jpeg|png|svg';
					$config['max_size']	= '0';//2 mb , 0 for unlimited
					$config['max_width']  = '0';// unlimited
					$config['max_height']  = '0';
					$this->upload->initialize($config);
						if ( ! $this->upload->do_upload('logo_file'))
						{
							$error =  $this->upload->display_errors();
							$sdata['message'] .= ', Notice.logo file not uploaded because '.$error.' , please upload it again!';

						}
						else
						{
							$uploadedFileDetail =  $this->upload->data();
							$logo_name=$uploadedFileDetail['file_name'];
							
						}
					}
				if($this->input->post('primary_company_id')!="" && $this->input->post('primary_company_id') > 0)
				{			
					$isPcompany=0;
				}
				else{
					$isPcompany=1;
				}
			$insData = array(				
				'company_name' => removeExtraspace($this->input->post('company_name')),
				'address' => $this->input->post('company_address'),				
				'city' => $this->input->post('city'),
				'country_code' => $this->input->post('country_code'),
				'head' => $this->input->post('company_head'),
				'company_logo' => $logo_name,
				'is_primary_company' => $isPcompany,
				'primary_company_id' => $this->input->post('primary_company_id'),
				'status' => $this->input->post('company_status')				
				);
				
		
		if($cmpID=$this->setting_model->addCompany($insData))
		 {
		  $sdata['message'] .= 'Company '.$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
								
			/* Add Company Markets start*/
			$companyMarkets = $this->input->post('market_id');				
			if($companyMarkets!='' && count($companyMarkets)>0)		
					{										
					$addCompanyMarketsData=array();							
						foreach($companyMarkets as $companyMarket)
						{
						 if($companyMarket!='')
							{				
								$addCompanyMarketsData[] = array(
											'company_id' => $cmpID,
											'market_id' => $companyMarket
										);
								}		
						}
				if(count($addCompanyMarketsData)>0)
					{				
						$this->setting_model->addCompanyMarkets($addCompanyMarketsData);
					}
				}	
									
					/* Add Company Markets end*/
			
		 }
		else{
		$sdata['message'] = 'Company '.$newname.'  not added!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		
		else{
			$sdata['message'] = 'Company '. $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}
			redirect('settings/company');
		}			
		}
		else{
			$sdata['message'] = 'Empty data';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}
			redirect('settings/company');
		
        break;
	default:  
	}
	
	
	$countriesOptions=getCountriesOptions(); /* using helper*/
	$primaryCompanies=getPrimaryCompanies();
	$primaryCompaniesOptions=array('0'=>'None');
	if($primaryCompanies && count($primaryCompanies)>0)									
		{
			
		foreach($primaryCompanies as $primaryCompany)
		{
			$primaryCompaniesOptions[$primaryCompany->id]=$primaryCompany->company_name;
		}
		}
		
	$dc=$this->config->item('default_country_code');
	
	$pcdoce=$this->input->post('country_code');
	if($pcdoce=="")
	{
	$pcdoce=$dc;	
	}
	$citiesOptions =getCountyCitiesOptions($pcdoce);	/* using helper*/
	$marketsList=getMarkets();
	$marketOptions=array();
	if($marketsList && count($marketsList)>0)									
		{			
		foreach($marketsList as $market)
		{
			$marketOptions[$market->id]=$market->title;
		}
		}
		
	$positions=getPositions();
	$positionOptions =array(''  => 'Select');	
	if($positions && count($positions)>0)
	{
	foreach($positions as $position)
	{
		$positionOptions[$position->title]=$position->title;
	}
	}
	
        $data = array(
            'addAction' => site_url('settings/company/add'), 
			'editAction' => site_url('settings/edit_company/'), 
			'delAction' => site_url('settings/company/del'), 			
            'company_name'=>array('id' => 'company_name', 														
								'name' => 'company_name',
								'tabindex'=>1,
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",						
								'data-validation-length'=>"min3",							
								'placeholder'=>'Name',
								'value'=>$this->input->post('name')),
			'company_address'=>array('id' => 'company_address', 
												'tabindex'=>4,
												'data-validation'=>'required,alphanumeric,length',	
												'data-validation-allowing'=>"'\'-_@#:,./ ()\n\r",												
												'data-validation-length'=>"15-150",														
												'name' => 'company_address',												
												'cols'     => '40',
												'tabindex'=>4,
												'rows'          => '2'),					
								'country_id_options' => $countriesOptions,				
							    'city_options' => $citiesOptions,
								'markets_options'=>$marketOptions,	
			'company_head'=>array('id' => 'company_head', 														
								'name' => 'company_head',
								'tabindex'=>7,
								'data-validation'=>'custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",
								'data-validation-optional'=>'true',								
								'data-validation-length'=>"min3",							
								'placeholder'=>'Head',
								'value'=>$this->input->post('company_head')),	
				'statusOptions'=>array(
								'1'=>'Enable',
								'0'=>'Disable',				
								),

					'member_name'=>array(												
										'name' => 'member[1][name]',
										'id' => 'member_1_name',
										'tabindex'=>10,												
										'data-validation'=>'custom,length',
										'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
										'data-validation-length'=>"min3",
										'data-validation-optional'=>'true',
										'placeholder'=>'Name',
										'value'=>set_value('member[1][name]')),
					'member_email'=>array(												
										'name' => 'member[1][email]',
										'tabindex'=>11,
										'id' => 'member_1_email',
										'data-validation'=>'email',
										'data-validation-optional'=>'true',												
										'placeholder'=>'Email',
										'value'=>set_value('member[1][email]')),
				'position_options' => $positionOptions,
				'primary_companies_options' =>$primaryCompaniesOptions				
					
			);	
		$defMarket=$this->config->item('default_market');
		$defCcode = array($dc);							
		$data['posted_country_code']=	$this->input->post('country_code')!='' ? $this->input->post('country_code'):$defCcode;	
		$data['posted_city']=	$this->input->post('city')!='' ? $this->input->post('city'):'';
		$data['posted_address']=$this->input->post('company_address');							
		$data['posted_market_id']=	$this->input->post('market_id')!='' ? $this->input->post('market_id]'):'';	
		$data['posted_company_status']=	$this->input->post('company_status')!='' ? $this->input->post('company_status]'):'1';	
		$data['posted_position']=	$this->input->post('member[1][position]')!='' ? $this->input->post('member[1][position]]'):'';				
		$data['posted_primary_company_id']=	$this->input->post('primary_company_id')!='' ? $this->input->post('primary_company_id'):'0';	
		$data['listData']=$this->setting_model->getCompanyList();		
        $this->load->view('company_setting', $data);
    }
	
## Company/sub company section end ####
	
}
