<?php 

function getAccessLevelOptions()
{
	$CI =& get_instance();	
	$tables  = $CI->config->item('tables', 'ion_auth');
	$aclevel_options =array('0'  => 'Select');	
	$CI->db->select('id,name,description'); 
	$CI->db->order_by('name','asc'); 
    $query = $CI->db->get($tables['groups']); 
        if ($query->num_rows() > 0)
		{
        $levels=$query->result(); 
			foreach($levels as $aclevel)
			{
			$aclevel_options[$aclevel->id]=$aclevel->name;
			}
		}
	return $aclevel_options;
}


function getAccessLevelTitle($id)
{
	$CI =& get_instance();	
	$tables  = $CI->config->item('tables', 'ion_auth');
	$CI->db->select('name,description'); 
	$CI->db->where('id',$id); 
    $query = $CI->db->get($tables['groups']); 
	if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->name;
		}
	return false;       
	
}

function get_status_lnk($status,$uid)
{
if($uid==1)
{
return '<span class="status-icon"><a href="javascript:void(0);"  title="Active" class="active"> &nbsp</a></span>';
}
$html='<span class="status-icon">';
$html.=($status) ? anchor("users/deactivate/".$uid,'&nbsp',' title="Active" class="active" onclick="return confirm(\'Are you sure to deactivate this user?\')"') : anchor("users/activate/". $uid,'&nbsp',' title="Inactive" class="inactive" onclick="return confirm(\'Are you sure to activate this user?\')"');
$html.='</span>';
return $html;
}


function get_action_lnk($uid,$accessLabelId)
{
if($uid==1)
{
return "";
}
$html='';
if(checkAccess($accessLabelId,'users','edit')){
	$html.='<a class="edit_rec" href="users/edit_user/'.$uid.'">Edit</a>';
}
if(checkAccess($accessLabelId,'users','delete')){
	$html.='<a class="delete_rec" href="'.base_url().'users/delete_user/'.$uid.'" onclick="return confirm(\'Are you sure to delete this user?\')">Delete</a>';
}
if($html=='')
{
$html='NA';	
}
 
return $html;
}


 function acl_controller_methods($cId=NULL,$method_name = NULL)
	{
		$CI =& get_instance();	
		$tables  = $CI->config->item('tables', 'ion_auth');
		
		$CI->db->select('id,class_method_name,title');
		                
		if (isset($method_name))
		{
			$CI->db->where($tables['acl_controller_methods'].'.class_method_name', $method_name);
		}
		
		if (isset($cId))
		{
			$CI->db->where($tables['acl_controller_methods'].'.acl_controller_id', $cId);
		}		
		
		$CI->db->order_by('class_method_name', 'asc');
		
		$query =  $CI->db->get($tables['acl_controller_methods']);
		if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}

	}
	

function checkInAssignedACLResource($idval,$assignedACLResource,$accessType='controller')
{
	if($assignedACLResource && count($assignedACLResource)>0)
	{
	foreach($assignedACLResource as $res)
	{
		if($accessType=='method')
		{
		if($idval==$res->acl_sys_method_id)
		{
			return true;
		}	
		}
		else{
		if($idval==$res->acl_sys_controller_id)
		{
			return true;
		}
		}
	}
	}
	return false;
}
function checkAccess($accessLabelId,$curentControler,$controlerMethod)
{
$CI =& get_instance();
//echo 	$accessLabelId;
//echo 	'/'.$curentControler;
//echo 	'/'.$controlerMethod;
return $CI->ion_auth->check_authentication($accessLabelId,$curentControler,$controlerMethod);
}