<?php
class AdminController extends MX_Controller {
  	protected $paginateConfig = array();
  	protected $languages = array();
  	protected $breadcrum;
	protected $userId;
	protected $lang_id;
	public $curentControler;
	public $controlerMethod;
	public $accessDenidMessage;
	public $accessLabelId;
    public function __construct()
    {
		$this->load->library(array('ion_auth','user_agent'));
        $this->load->helper(array('url','acl_helper'));
		$this->accessDenidMessage=$this->config->item('access_not_allowed_message');
		
        parent::__construct();
			$this->output->set_meta('description','Dorak information');
		$this->output->set_meta('title','Dorak information');
		$this->output->set_title('Dorak');
		$this->output->set_template('default');
      // if not logged in - go to home page
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        } 
		
	if ($this->ion_auth->logged_in()){
		$this->curentControler=$this->router->fetch_class(); // current class
		$this->controlerMethod=$this->router->fetch_method(); // curent class method
		$user = $this->ion_auth->user()->row();	
		$this->accessLabelId = $this->ion_auth->get_users_groups($user->id)->row()->id;	
		if(!$this->ion_auth->is_admin() && ($this->curentControler!='users' && $this->controlerMethod!='index'))// check user access for class/methods 
		{
		$ac=checkAccess($this->accessLabelId,$this->curentControler,'view');// if user have permession to see
		if(!$ac)
		{
		
			//$sdata['message'] ='You are  not allowed  to access : '.str_replace('_',' ',$this->curentControler).'->'.str_replace('_',' ',$this->controlerMethod);
					$sdata['message'] ='Permession required to access this page';					
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
					$this->session->set_userdata($flashdata);
					//echo $this->agent->referrer();
					
					//echo site_url('auth/login');
					//exit();
			if ($this->agent->is_referral() && $this->agent->is_referral()!=site_url('auth/login'))	
			{
			redirect($this->agent->referrer());
			}
			else
			{
			 redirect('users', 'refresh');	
			}
		}
		}
		
		}
 
    }

    //--------------------------------------------------------------------


}
