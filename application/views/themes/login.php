<html lang="en">
<head>
	<title>Dorak Holding</title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="all, index, follow"/>
		<meta name="googlebot" content="all, index, follow" />
<!-- Le styles -->
<link href="<?php echo base_url(); ?>assets/themes/default/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-checkbox.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-select.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/calendar.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300,900' rel='stylesheet' type='text/css'>

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/default/images/favicon.png" type="image/x-icon"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.pulse.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-checkbox.js"></script>

<!-- Latest compiled and minified JavaScript -->

</head>
<body>
<div class="login-outer">
	<div class="lofin-part">
	
	<?php 
$uriseg=$this->uri->segment_array();
/* folowing are the custome code  to display alert message  after different  action
 * this added because the HMVC  module not support the  session flashdata of codinighter
 * */
if($this->session->userdata('message_type')=='sucess' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alertmssage" class=""><?php echo $this->session->userdata('flashdata');?> </div>
<?php }
elseif($this->session->userdata('message_type')=='notice' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alertnotice" class=""><?php echo $this->session->userdata('flashdata');?></div>
<?php }
elseif($this->session->userdata('message_type')=='error' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alerterror" class=""><?php echo $this->session->userdata('flashdata');?></div>
<?php 
}
if($this->session->userdata('message_type')!='')
{
$this->session->unset_userdata('message_type');
}
if($this->session->userdata('flashdata')!='')
{
$this->session->unset_userdata('flashdata');
}

/* alert messahe section end */
?>

 <?php echo $output;?>
    

    </div>
</div>

<script>
  	$('input[type="checkbox"]').checkbox({
    checkedClass: 'icon-check',
    uncheckedClass: 'icon-check-empty'
});
  </script>

</body>
</html>