<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends \Myth\Controllers\ThemedController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 /* Session variable for vechicle=== vechicleDetailsess for  */
	 function __Construct() {
		parent::__Construct ();
		$this->load->library('session');
		$this->addStyle('parsley.css');
		$this->addScript('parsley.min.js');
		$this->addScript('custom.js');
		$this->load->model('Dealer_model');	
		$this->load->helper('extra');
		$stepDetail = $this->session->userdata('StepDetails');
		$this->setVar('steps', $stepDetail);
	
	}
	
	public function index()
	{	//$this->addScript('custom.js');
		$this->output->enable_profiler( FALSE );
		$this->session->sess_destroy();
		$this->render();
	}
	
	
	public function step1()
	{
		//$this->output->enable_profiler( FALSE );
		//$indexNumber = base64_decode($this->input->get('rec'));
		$vin_num = $this->input->get('vin');
		$vRecord = $this->searchInCsv($vin_num);
		$vechicleDetail = $vRecord;
		$this->session->set_userdata('vechicleDetailsess',$vRecord);
		$vin['vin'] = $vRecord['A'];
		$this->session->set_userdata('sess_vin',$vin['vin']);
		if($vin['vin']!=$vin_num){
			$this->setMessage('Record Not found', 'warning');
			$this->session->unset_userdata('sess_vin');
			redirect('home');
		}
		$this->setVar('title', 'Vehicle Details');
		$this->setVar('vechicleDetail', $vechicleDetail);
		$this->render();
		
	}
	
	/**
	* 
	* @function  for step 2 dealer search 
	* 
	* @return
	*/
	public function step2(){
		$vin_n = $this->input->get('vin');
		if($vin_n!=($this->session->userdata('sess_vin'))){
			redirect('home');
		}
		if($vin_n){
			//$DetailOnStep2 = array();
			$vechicleDetail = $this->searchInCsv($vin_n);
			$DetailOnStep[$vin_n]['current_step'] = '2';
			$DetailOnStep[$vin_n]['last_step'] = '1';
			$DetailOnStep[$vin_n]['vechile_detail'] = $vechicleDetail;
			$DetailOnStep[$vin_n]['dealer_detail'] = '';
			$DetailOnStep[$vin_n]['app_date'] = '';
			$DetailOnStep[$vin_n]['app_slot'] = '';
			$DetailOnStep[$vin_n]['user_info'] = '';
			$this->session->set_userdata('StepDetails',$DetailOnStep);
			if($this->input->post()){
			$this->load->model('dealer_model');
			$p_code = trim($this->input->post('p_code'));
			//print_r($p_code);
			$dealers = $this->dealer_model->where('postal_code', $p_code)
						  ->limit(25)
						  ->find_all();
			
			if(empty($dealers)){
				$this->setVar('msg', 'Please Enter a valid Pincode');
			}
			
			foreach($dealers as $dealer){
				$lat = $dealer['location_x'];
				$lng = $dealer['location_y'];
				
				$statrement = "*,
				((ACOS(SIN($lat * PI() / 180) *
				SIN(location_x * PI() / 180) + COS($lat * PI() / 180) *
				COS(location_x * PI() / 180) * COS(($lng - location_y) *
				PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance
				";
				
				$dealerList = $this->dealer_model->select($statrement, TRUE)
							->join('dealer_timing', 'dealer_timing.dealer_id=dealers.id')
							->having('distance < ', 300)
							->order_by('distance', 'asc')
							->find_all();
				
			}	
			}
				
			
			//*###Get Excel motor details###**************
		//	$this->output->enable_profiler( FALSE );
			$vin_num = $this->input->get('vin');
			$vechicleDetail = $this->searchInCsv($vin_num);
			$this->setVar('vechicleDetails',$vechicleDetail);
			$this->checkVinstatus($vin_n); /* check vin status for thankyou message */
			$this->setVar('title', 'Dealer Search');
			$this->setVar('vin_n', isset($vin_n) ? $vin_n : NULL);
			$this->setVar('dealerList', isset($dealerList) ? $dealerList : NULL);
			$this->render();
		}
	}
	
	public function SearchDealerByPin(){
		$stepDetail = $this->session->userdata('StepDetails');
		$dealerId = $this->input->post('DealerId');
		
		
		
		$vin = $this->input->post('vin');
		$vechicleDetail = $this->searchInCsv($vin);
		if(isset($stepDetail[$vin]) && $stepDetail[$vin]['current_step']==2){
			$dealers = $this->Dealer_model->select('name,address,town,postal_code')->find($dealerId[0]);
			$stepDetail[$vin]['current_step'] = '2';
			$stepDetail[$vin]['last_step'] = '1';
			$stepDetail[$vin]['dealer_detail'] = $dealerId;
			$stepDetail[$vin]['dealer_info'] = $dealers;
			$data = $this->session->set_userdata('StepDetails',$stepDetail);
			$result = array('status'=>'yes', 'msg'=>'Dealer Successfully Selected' , 'nextUrl' =>base_url("home/booking?vin=$vin"));
			$this->renderJson($result);
		}
		else{
			$result = array('status'=>'no', 'msg'=>'Something went to wrong' , 'nextUrl' =>base_url());
			$this->renderJson($result);
		}
	}
	
	/**
	* 
	* @function  for thankyou page
	* 
	* @return
	*/
	
	public function thankyou_us(){
		$ref = $this->input->get('ref');
		$sess_vin = $this->session->userdata('sess_vin');
		$sess_date = $this->session->userdata('sess_date');
		$this->setVar('session_vin',$sess_vin);
		$this->setVar('session_date',$sess_date);
		$this->render();
	}
	
	public function thankyou_yes(){
		$ref = $this->input->get('ref');
		$vin_no = $this->input->get('vin');
		$this->setVar('vin_no',$vin_no);
		$this->render();
	}
	
	
	
	public function printR(){
		$ref = $this->input->get('ref');
		$ref = $this->input->get('vin');
		$this->setVar('vin_no',$ref);
		$this->render();
	}
	
	/**
	* 
	* @function  for New Registration if data does not exist in csv file and VIN number not match
	* 
	* @return
	*/
	public function step4(){
		$this->output->enable_profiler( FALSE );
		$this->addStyle('parsley.css');
		$this->addScript('parsley.min.js');
		$this->addScript('custom.js');
		$this->setVar('title', 'Registration');
		$this->load->library('form_validation');
		$this->load->model('users_detail_model');
		$this->load->model('booking_model');
		$vin = $this->input->get('vin');
		$currentdate = date('m-d-Y');
		$this->session->set_userdata('sess_vin', $vin);
		$this->session->set_userdata('sess_date', $currentdate);
		$sess_vin = $this->session->userdata('sess_vin');
		$sess_date = $this->session->userdata('sess_date');
		$this->setVar('session_vin',$sess_vin);
		$this->setVar('session_date',$sess_date);		 
		//$vinStatus = $this->checkVinstatus($sess_vin);
		//$booking_type = $this->input->post('booking_type');
		//$this->session->set_userdata('booking_type', $booking_type);
		//if($vinStatus=='yes'){	
		//	redirect('home/step1?vin='.$sess_vin); 
		//}
		$secret = '6Lf0xhcTAAAAAKiYW2hIadKgBPacnBhQXvN-X_nP';
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
			//var_dump($vinStatus);
		if($this->input->post()){	
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
				if ($resp->isSuccess()) {
					$data = $this->input->post();
					$userId = $this->users_detail_model->insert($data, TRUE);
					$data['user_detail'] = $userId;
					$check = $this->booking_model->find_by('vin',$data['vin']);
					if(!empty($check)){
						$this->booking_model->update_by('vin',$data['vin'],$data);
					}
					else{
						$sess_vin = $this->session->userdata('sess_vin');
						$sess_booking_type = $this->session->userdata('booking_type');
						$data = array('user_detail' => $userId, 'vin' => $sess_vin, 'booking_type' => $sess_booking_type);
						$refid = $this->booking_model->insert($data);
					}
					
					
					//**###########Mail to User****##//
					$data = $this->input->post();
					$message='';
$message .='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title></title><meta name="" content=""></head><body><table width="700" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #cccccc"><table width="100%%" border="0" cellspacing="3" cellpadding="3"><tr><td width="87%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/logo.png" alt="" width="276" height="85" /></td><td width="13%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/volkswagon.png" alt="" width="92" height="92"/></td></tr></table></td></tr>';
  
 $message .='<tr><td height="300" valign="top"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px;">&nbsp;</td></tr>';
      $message .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;">Dear '.$data['first_name'].',</td></tr><tr><td>&nbsp;</td></tr>';
     $message .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;">Thank you for your booking request.  One of our advisors will be in touch shortly to confirm details of your appointment.';
        
          $message .='<table width="100%" border="0" cellspacing="1" cellpadding="5"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;" ><table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744; border-top:1px solid #ccc; border-left:1px solid #ccc; font-weight:normal;">';

              
        
 
            $message .='</table></td></tr></table></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr>';
      
      $message .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;"><strong>Jardine Motor Group</strong><br /><a href="http://jardine.digitalagencyuk.co.uk">www.jardinemotors.co.uk</a></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr></table></td></tr><tr><td height="100" style="background:#eee"><table width="80%" border="0" align="center" cellpadding="3" cellspacing="3"><tr><td width="7%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/footer-logo.png" alt="" width="50" height="66" /></td><td width="93%" style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:35px;"><a href="#">Terms</a> | <a href="#">Privacy</a> | <a href="#">Unsubscribe</a><br />2016 Jardine Motors Group </td></tr></table></td></tr></table></body></html>';
				$to = $data['email'];
				$data['email']='admin@jardine.com';
				$subject = "Jardine Motors: Thank you for your booking request";
				//$message ="this is message";
				$headers = "From: " . $data['email'] . "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				 if (mail($to, $subject, $message, $headers)) {
               $mess = 'Your message has been sent.';
               $status = 'ok';
             	} else {
               $mess = 'There was a problem sending the email.';
               $status = 'no';
             	}
				
			//*######*Email Template to Admin****#####
					$message2='';
$message2 .='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title></title><meta name="" content=""></head><body><table width="700" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #cccccc"><table width="100%%" border="0" cellspacing="3" cellpadding="3"><tr><td width="87%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/logo.png" alt="" width="276" height="85" /></td><td width="13%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/volkswagon.png" alt="" width="92" height="92"/></td></tr></table></td></tr>';
  
 $message2 .='<tr><td height="300" valign="top"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px;">&nbsp;</td></tr>';
      $message2 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;">Dear Admin,</td></tr><tr><td>&nbsp;</td></tr>';
     $message2 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;">Below is the detail of new booking request:';
        
          $message2 .='<table width="100%" border="0" cellspacing="1" cellpadding="5"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;" ><table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744; border-top:1px solid #ccc; border-left:1px solid #ccc; font-weight:normal;">';
              
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Vehicle Reg No.</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['date'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Name</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['title'].' '.$data['first_name'].' '.$data['last_name'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Email ID</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['email'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Phone Number</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['contact_number'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Postal Code</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['postal_code'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Country</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['country'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">User Address Type</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['address_type'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">User Address</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$data['address'].'</td></tr>';	
            $message2 .='</table></td></tr></table></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr>';
      
      $message2 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;"><strong>Jardine Motor Group</strong><br /><a href="http://jardine.digitalagencyuk.co.uk">www.jardinemotors.co.uk</a></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr></table></td></tr><tr><td height="100" style="background:#eee"><table width="80%" border="0" align="center" cellpadding="3" cellspacing="3"><tr><td width="7%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/footer-logo.png" alt="" width="50" height="66" /></td><td width="93%" style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:35px;"><a href="#">Terms</a> | <a href="#">Privacy</a> | <a href="#">Unsubscribe</a><br />2016 Jardine Motors Group </td></tr></table></td></tr></table></body></html>';
				$to2 = 'anjali@inspirationinc.co.uk';
				$data['email']='admin@jardine.com';
				$subject2 = "Jardine Motors: Thank you for your booking request(To Admin)";
				//$message ="this is message";
				$headers2 = "From: " . $data['email'] . "\r\n";
				$headers2 .= "MIME-Version: 1.0\r\n";
				$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				 if (mail($to2, $subject2, $message2, $headers2)) {
               $mess = 'Your message has been sent.';
               $status = 'ok';
             	} else {
               $mess = 'There was a problem sending the email.';
               $status = 'no';
             	}
				
					
					
					

					redirect(base_url('home/thankyou_us?vin='.$sess_vin));
				} 
				else{
				    //$errors = $resp->getErrorCodes();
				   // $this->setMessage('Please Verify Your Captcha','Warning');
				    $this->setVar('cap','Please Verify Your Captcha');
				}
		}
		$this->setVar('vin', $vin);
		$this->render();
	}
	
	/**
	* 
	* @function  for booking recall step3
	* 
	* @return
	*/
	
	public function booking(){
		$this->addStyle('jquery.datetimepicker.css');
		$this->addScript('jquery.datetimepicker.full.js');
		$this->addStyle('parsley.css');
		$this->addScript('parsley.min.js');
		$this->addScript('custom.js');
		$this->load->library('session');
		
		$vin_no = $this->input->get('vin');
	//	$vin_num = $this->session->userdata('sess_vin');
	
		/*if($this->input->post()){
		//print_r( $this->input->post()); die;
			 $selectedDealer = $this->input->post('selectedDealer');
			 $this->session->set_userdata('sess_dealerid', $selectedDealer);
			 $sess_dealerid = $this->session->userdata('sess_dealerid');
			 //die('P');
			 $dealers = $this->Dealer_model->find_by('id',$sess_dealerid);
			 //print $this->db->last_query();
		} */
	//$dealers = $this->dealer_model->where('id', $selectedDealer)->find_all();
//$this->setVar('dealer_info',$dealers);
		//$this->setVar('selectedDealer', $sess_dealerid);
		$this->setVar('vin_no', $vin_no);
		$this->render();
	}
	
	public function bookingSlot(){     
		$step3Detail = $this->session->userdata('StepDetails');
		$app_date = $this->input->post('app_date');
		$vin = $this->input->post('vin');
		$time_slot = $this->input->post('time_slot');
		if(isset($step3Detail[$vin]) && $step3Detail[$vin]['current_step']==2){
			$step3Detail[$vin]['current_step'] = '3';
			$step3Detail[$vin]['last_step'] = '2';
			$step3Detail[$vin]['app_date'] = $app_date;
			$step3Detail[$vin]['app_slot'] = $time_slot;
			$data = $this->session->set_userdata('StepDetails',$step3Detail);
			$result = array('status'=>'yes', 'msg'=>'Dealer Successfully Selected' , 'nextUrl' =>base_url("home/user_detail?vin=$vin"));
			$this->renderJson($result);
		}
		else{
			$result = array('status'=>'no', 'msg'=>'Something went to wrong' , 'nextUrl' =>base_url());
			$this->renderJson($result);
		}
	}
	
	
	public function lastStepThankyou(){
		$this->load->model('users_detail_model');
		$this->load->model('booking_model');
		$this->load->model('dealer_model');
			$step4Detail = $this->session->userdata('StepDetails');
			$vin = $this->input->post('vin_num');
			$user_infos = $this->input->post();
			$secret = '6Lf0xhcTAAAAAKiYW2hIadKgBPacnBhQXvN-X_nP';
			$recaptcha = new \ReCaptcha\ReCaptcha($secret);
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
			if ($resp->isSuccess()){
			if(isset($step4Detail[$vin]) && $step4Detail[$vin]['current_step']==3){
			$user_detail['first_name'] = $this->input->post('first_name');
			$user_detail['title'] = $this->input->post('title');
			$user_detail['last_name'] = $this->input->post('last_name');
			$user_detail['email'] = $this->input->post('email');
			$user_detail['contact_number'] = $this->input->post('contact_number');
			$user_detail['postal_code'] = $this->input->post('postal_code');
			$user_detail['country'] = $this->input->post('country');
			$user_detail['address_type'] = $this->input->post('address_type');
			$user_detail['address'] = $this->input->post('address'); 
			$user_detail['address1'] = $this->input->post('address1');  
			$session_data = $this->session->userdata('StepDetails');
			$booking_type = $session_data[$vin]['vechile_detail']['J'];
			if(strtolower(trim($booking_type))=='yes'){
				$booking_ty='Y';	
			}
			if(strtolower(trim($booking_type))=='us'){	
				$booking_ty='US';
			}
			
			$bookin_detail['appointment_date'] = $session_data[$vin]['app_date'];
			$bookin_detail['time_slot'] = $session_data[$vin]['app_slot'];
			$bookin_detail['dealer_id'] = $session_data[$vin]['dealer_detail'][0];
			$bookin_detail['vin'] = $vin;
			$bookin_detail['booking_type'] = $booking_ty;
			$user_detailid = $this->users_detail_model->insert($user_detail);
				$bookin_detail['user_detail'] = $user_detailid;
			$check = $this->booking_model->find_by('vin',$bookin_detail['vin']);
			if(!empty($check)){
				$lastid = $this->booking_model->update_by('vin',$bookin_detail['vin'],$bookin_detail);			
				}
			else{
				$lastid = $this->booking_model->insert($bookin_detail);
				}
				$step4Detail[$vin]['current_step'] = '4';
				$step4Detail[$vin]['last_step'] = '3';
				$step4Detail[$vin]['user_info'] = $user_infos;
				$this->session->set_userdata('StepDetails',$step4Detail);
				$result = array('status'=>'yes', 'msg'=>'User ASuccessfully' , 'nextUrl' =>base_url("home/thankyou_yes/?ref=$lastid&vin=$vin"));
				
				//##**Email Message Template To User**##//
				if($bookin_detail['time_slot']=='E'){
					$slot='PM';
				}
				if($bookin_detail['time_slot']=='M'){
					$slot='AM';
				}
				$dealer_details = $this->session->userdata('StepDetails');
				$dname = $dealer_details[$vin]['dealer_info']['name'];
				$daddress = $dealer_details[$vin]['dealer_info']['address'];
				$message='';
$message .='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title></title><meta name="" content=""></head><body><table width="700" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #cccccc"><table width="100%%" border="0" cellspacing="3" cellpadding="3"><tr><td width="87%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/logo.png" alt="" width="276" height="85" /></td><td width="13%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/volkswagon.png" alt="" width="92" height="92"/></td></tr></table></td></tr>';
  
 $message .='<tr><td height="300" valign="top"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px;">&nbsp;</td></tr>';
      $message .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;">Dear '.$user_detail['first_name'].',</td></tr><tr><td>&nbsp;</td></tr>';
     $message .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;">Thank you for your booking request.  One of our advisors will be in touch shortly to confirm details of your appointment.';
        
          $message .='<table width="100%" border="0" cellspacing="1" cellpadding="5"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;" ><table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744; border-top:1px solid #ccc; border-left:1px solid #ccc; font-weight:normal;">';
              
              $message .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Vehicle Reg No.</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$vin.'</td></tr>';
              
              $message .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Dealer Name</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$dname.'</td></tr>';
              
              $message .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Address</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$daddress.'</td></tr>';
              
             $message .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Appointment Date / Slot</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$bookin_detail['appointment_date'].' / '.$slot.'</td></tr>';
 
            $message .='</table></td></tr></table></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr>';
      
      $message .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;"><strong>Jardine Motor Group</strong><br /><a href="http://jardine.digitalagencyuk.co.uk">www.jardinemotors.co.uk</a></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr></table></td></tr><tr><td height="100" style="background:#eee"><table width="80%" border="0" align="center" cellpadding="3" cellspacing="3"><tr><td width="7%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/footer-logo.png" alt="" width="50" height="66" /></td><td width="93%" style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:35px;"><a href="#">Terms</a> | <a href="#">Privacy</a> | <a href="#">Unsubscribe</a><br />2016 Jardine Motors Group </td></tr></table></td></tr></table></body></html>';
				$to = $user_detail['email'];
				$data['email']='admin@jardine.com';
				$subject = "Jardine Motors: Thank you for your booking request";
				//$message ="this is message";
				$headers = "From: " . $data['email'] . "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				 if (mail($to, $subject, $message, $headers)) {
               $mess = 'Your message has been sent.';
               $status = 'ok';
             	} else {
               $mess = 'There was a problem sending the email.';
               $status = 'no';
             	}
				
				//##**Email Message Template To Admin**##//
				
				$message2='';
$message2 .='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title></title><meta name="" content=""></head><body><table width="700" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #cccccc"><table width="100%%" border="0" cellspacing="3" cellpadding="3"><tr><td width="87%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/logo.png" alt="" width="276" height="85" /></td><td width="13%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/volkswagon.png" alt="" width="92" height="92"/></td></tr></table></td></tr>';
  
 $message2 .='<tr><td height="300" valign="top"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px;">&nbsp;</td></tr>';
      $message2 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;">Dear Admin,</td></tr><tr><td>&nbsp;</td></tr>';
     $message2 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;">Below is the detail of new booking request:';
        
          $message2 .='<table width="100%" border="0" cellspacing="1" cellpadding="5"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;" ><table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744; border-top:1px solid #ccc; border-left:1px solid #ccc; font-weight:normal;">';
              
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Vehicle Reg No.</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$vin.'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Name</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['title'].' '.$user_detail['first_name'].' '.$user_detail['last_name'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Email ID</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['email'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Phone Number</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['contact_number'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Postal Code</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['postal_code'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Country</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['country'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">User Address Type</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['address_type'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">User Address</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['address'].'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Dealer Name</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$dname.'</td></tr>';
              $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Dealer Address</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$daddress.'</td></tr>';
             $message2 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Appointment Date / Slot</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$bookin_detail['appointment_date'].' / '.$slot.'</td></tr>';	
            $message2 .='</table></td></tr></table></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr>';
      
      $message2 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;"><strong>Jardine Motor Group</strong><br /><a href="http://jardine.digitalagencyuk.co.uk">www.jardinemotors.co.uk</a></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr></table></td></tr><tr><td height="100" style="background:#eee"><table width="80%" border="0" align="center" cellpadding="3" cellspacing="3"><tr><td width="7%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/footer-logo.png" alt="" width="50" height="66" /></td><td width="93%" style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:35px;"><a href="#">Terms</a> | <a href="#">Privacy</a> | <a href="#">Unsubscribe</a><br />2016 Jardine Motors Group </td></tr></table></td></tr></table></body></html>';
				$to2 = 'anjali@inspirationinc.co.uk';
				$data['email']='admin@jardine.com';
				$subject2 = "Jardine Motors: Thank you for your booking request(To Admin)";
				//$message ="this is message";
				$headers2 = "From: " . $data['email'] . "\r\n";
				$headers2 .= "MIME-Version: 1.0\r\n";
				$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				 if (mail($to2, $subject2, $message2, $headers2)) {
               $mess = 'Your message has been sent.';
               $status = 'ok';
             	} else {
               $mess = 'There was a problem sending the email.';
               $status = 'no';
             	}
				
				
				
				//##**Email Message Template To Dealers**##//
				
				$message3='';
$message3 .='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title></title><meta name="" content=""></head><body><table width="700" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #cccccc"><table width="100%%" border="0" cellspacing="3" cellpadding="3"><tr><td width="87%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/logo.png" alt="" width="276" height="85" /></td><td width="13%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/volkswagon.png" alt="" width="92" height="92"/></td></tr></table></td></tr>';
  
 $message3 .='<tr><td height="300" valign="top"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px;">&nbsp;</td></tr>';
      $message3 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;">Dear '.$dname.',</td></tr><tr><td>&nbsp;</td></tr>';
     $message3 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;">Below is the detail of new booking request:'; 
        
          $message3 .='<table width="100%" border="0" cellspacing="1" cellpadding="5"><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; line-height:30px; color:#2c3744;" ><table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744; border-top:1px solid #ccc; border-left:1px solid #ccc; font-weight:normal;">';
              
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Vehicle Reg No.</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$vin.'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Name</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['title'].' '.$user_detail['first_name'].' '.$user_detail['last_name'].'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Email ID</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['email'].'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Phone Number</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['contact_number'].'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Postal Code</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['postal_code'].'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Country</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['country'].'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">User Address Type</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['address_type'].'</td></tr>';
              $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">User Address</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$user_detail['address'].'</td></tr>';
              
              
             $message3 .='<tr><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">Appointment Date / Slot</td><td style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;">'.$bookin_detail['appointment_date'].' / '.$slot.'</td></tr>';	
            $message3 .='</table></td></tr></table></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr>';
      
      $message3 .='<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px; color:#2c3744;"><strong>Jardine Motor Group</strong><br /><a href="http://jardine.digitalagencyuk.co.uk">www.jardinemotors.co.uk</a></td></tr><tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:26px;">&nbsp;</td></tr></table></td></tr><tr><td height="100" style="background:#eee"><table width="80%" border="0" align="center" cellpadding="3" cellspacing="3"><tr><td width="7%"><img src="http://jardine.digitalagencyuk.co.uk/assets/images/footer-logo.png" alt="" width="50" height="66" /></td><td width="93%" style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:35px;"><a href="#">Terms</a> | <a href="#">Privacy</a> | <a href="#">Unsubscribe</a><br />2016 Jardine Motors Group </td></tr></table></td></tr></table></body></html>';
				$to3 = 'anjali@inspirationinc.co.uk';
				$data['email']='admin@jardine.com';
				$subject3 = "Jardine Motors: Thank you for your booking request(To Dealer)";
				//$message ="this is message";
				$headers3 = "From: " . $data['email'] . "\r\n";
				$headers3 .= "MIME-Version: 1.0\r\n";
				$headers3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				 if (mail($to3, $subject3, $message3, $headers3)) {
               $mess = 'Your message has been sent.';
               $status = 'ok';
             	} else {
               $mess = 'There was a problem sending the email.';
               $status = 'no';
             	}
				
				
				$this->renderJson($result);
			}
			else{
			$result = array('status'=>'no', 'msg'=>'Something went to wrong' , 'nextUrl' =>base_url());
			
			$this->renderJson($result);
			}
			}
			else{
				    $errors = $resp->getErrorCodes();
				    //print_r($errors);
					$vin = $this->session->userdata('sess_vin');
					$this->setMessage('Please select captcha', 'warning');
					//redirect('home/user_detail?vin='.$vin);
				}
			
		}
	
	
	
	
	public function user_detail(){
		$this->load->model('users_detail_model');
		$this->load->model('booking_model');
		$this->load->model('dealer_model');
		$this->load->library('session');
		if($this->input->post()){
			$appoinmentDate = $this->input->post('appoinmentDate');
			$time_slot = $this->input->post('time_slot');
			$this->session->set_userdata('appoinment_date',$appoinmentDate); 
			$this->session->set_userdata('time_slot',$time_slot); 
			$appoinmentDate = $this->session->userdata('appoinment_date');
			$time_slot = $this->session->userdata('time_slot');
		}
		$vin_no = $this->input->get('vin');
		$this->setVar('vin_no', $vin_no);
		$this->setVar('appoinmentDate', isset($appoinmentDate) ? $appoinmentDate : NULL);
		$this->setVar('time_slot', isset($time_slot) ? $time_slot : NULL);
		$this->render();
	}
	
	
	
	
	public function bookRecall(){
		
		$this->load->model('users_detail_model');
		$this->load->model('booking_model');
		$this->load->model('dealer_model');
		$this->load->library('session');
		$this->addScript('custom.js');
		if($this->input->post())
		{
			
			$data =	$this->input->post();
			$secret = '6Lf0xhcTAAAAAKiYW2hIadKgBPacnBhQXvN-X_nP';
			$recaptcha = new \ReCaptcha\ReCaptcha($secret);
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
			
				if ($resp->isSuccess()) 
				{
					$user_detail['first_name'] = $this->input->post('first_name');
					$user_detail['title'] = $this->input->post('title');
					$user_detail['last_name'] = $this->input->post('last_name');
					$user_detail['email'] = $this->input->post('email');
					$user_detail['contact_number'] = $this->input->post('contact_number');
					$user_detail['postal_code'] = $this->input->post('postal_code');
					//$user_detail['floor_unit_number'] = $this->input->post('floor_unit_number');
					//$user_detail['building_number'] = $this->input->post('building_number');
					//$user_detail['street'] = $this->input->post('street');
					//$user_detail['locality'] = $this->input->post('locality');
					//$user_detail['town'] = $this->input->post('town');
					$user_detail['country'] = $this->input->post('country');
					$user_detail['address_type'] = $this->input->post('address_type');
					$user_detail['address'] = $this->input->post('address'); 
					$user_detail['address1'] = $this->input->post('address1'); 
					$bookin_detail['appointment_date'] = $this->input->post('appointment_date');
					$bookin_detail['time_slot'] = $this->input->post('time_slot');
					$bookin_detail['dealer_id'] = $this->session->userdata('sess_dealerid');
					$bookin_detail['vin'] = $this->session->userdata('sess_vin');
					$bookin_detail['booking_type'] = $this->input->post('booking_type');
					
						$user_detailid = $this->users_detail_model->insert($user_detail);
						//echo validation_errors(); die;
						$bookin_detail['user_detail'] = $user_detailid;
						$check = $this->booking_model->find_by('vin',$bookin_detail['vin']);
						if(!empty($check))
						{
							$lastid = $this->booking_model->update_by('vin',$bookin_detail['vin'],$bookin_detail);
							
								
								redirect('home/thankyou_yes/?ref='.$lastid );
							
						}
						else
						{
							$lastid = $this->booking_model->insert($bookin_detail);
							
								/* $this->session->unset_userdata('sess_dealerid');
								$this->session->unset_userdata('booking_type');
								$this->session->unset_userdata('appoinment_date');
								$this->session->unset_userdata('sess_vin');
								$this->session->unset_userdata('vechicleDetailsess'); */
								redirect('home/thankyou_yes/?ref='.$lastid);
						}
				}
				else 
				{
				    $errors = $resp->getErrorCodes();
				    //print_r($errors);
					$vin = $this->session->userdata('sess_vin');
					$this->setMessage('Please select captcha', 'warning');
					redirect('home/user_detail?vin='.$vin);
				}
		}
		
	}
	
	
	
	/**
	* 
	* @function  for check VIN number status exist or not 
	* 
	* @return
	*/
	
	private function checkVinstatus($vin=NULL){
		$vRecord = $this->searchInCsv($vin);
		$this->output->enable_profiler( FALSE );
		return $status = trim(strtolower($vRecord['J']));
		//print_r($status);
	}
	
	/**
	* 
	* @function  for check VIN number 
	* 
	* @return
	*/
	public function checkVin(){
		$this->load->model('message_model');
		//print_r($result);
		$vin_num = trim($this->input->post('vin_num'));
		
		$vRecord = $this->searchInCsv($vin_num);
		$this->output->enable_profiler( FALSE );
		$status = trim(strtolower($vRecord['J']));
		 if(!empty($status))
		 {
			
			switch($status)  {
				case 'yes':
				
					$result	= $this->message_model->find_by('message_id', 'Y');
					$indexId = $vRecord['id'];
					$vRecord['message'] = $result['message'];
					//$vRecord['nextURL'] = base_url('home/step1?rec='.base64_encode($indexId));
					$vRecord['nextURL'] = base_url('home/step1?vin='.$vin_num);
				break;
				case 'no':
					$result	= $this->message_model->find_by('message_id', 'N');
					$vRecord['message'] = $result['message'];
					$vRecord['nextURL'] =  base_url('checkregistration');
				break;
				default:
					$result	= $this->message_model->find_by('message_id', 'US');
					$vRecord['message'] = $result['message'];
					$vRecord['us_vin_num'] = $vin_num;
					$vRecord['nextURL'] = base_url('home/step4?vin='.$vin_num);
				break;
			}
			
		}else{
				$result	= $this->message_model->find_by('message_id', 'US');
				$vRecord['message'] = $result['message'];
				$vRecord['nextURL'] = base_url('home/step4');
		}
		$this->renderJson($vRecord);
	}

	/**
	* 
	* @param string $keyword
	* 
	* @return
	*/
	private function searchInCsv($keyword=NULL){
		$vinfile = FCPATH.'restircted/vin_details.xlsx';
		$inputFileType = PHPExcel_IOFactory::identify ( $vinfile ); 
		$objReader = PHPExcel_IOFactory::createReader ( $inputFileType );
		$objPHPExcel = $objReader->load ( $vinfile );
		$sheetData = $objPHPExcel->getActiveSheet ()->toArray ( null, true, true, true );
		
		$vin = array_search($keyword, array_column($sheetData, 'A'));
		/// adding $vin  + 1 to excludeheading
		$vin += 1;
		$sheetData[$vin]['id'] = $vin;
		//print($vin);
		
		return (isset($sheetData[$vin])) ? $sheetData[$vin] : NULL;
		
	}
	
	public function page($id=0){
		if(!empty($id)){
			$this->load->model('Page_model');
			$page = $this->Page_model->select('pages.*')->find_by('pages.id', $id);
			if(!empty($page)){
				$this->setVar('metatitle',$page['metatitle']);
				$this->setVar('metadescription',$page['metadescription']);
				$this->setVar('metakeyword',$page['metakeyword']);
				$this->setVar('description',$page['description']);
				$exjs = explode(PHP_EOL,$page['external_js']);
				if(!empty($exjs)){
					foreach($exjs as $js){
						$this->addScript($js);
					}
				}
				
				///$this->addInScript($page['internal_js']);
				$excss = explode(PHP_EOL,$page['external_css']);
				if(!empty($excss)){
					foreach($excss as $css){
						$this->addStyle($css);
					}
				}
			}else{
				show_404();
			}
		}else{
			show_404();
		}
		$this->render();
	}
	
	
}
